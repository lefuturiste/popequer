#!/usr/bin/bash

export RUST_BACKTRACE=1
export POPEQUER_NOTEBOOK=/mnt/extramedia3/mbess/workspace/popequer/sandbox_notebook_1

cargo run --bin popequer -- "$@"
