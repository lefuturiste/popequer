@Entry {
    @label(fr) {
        "Château de Gaillon"
        "château français de la Renaissance situé à Gaillon, Normandie"
        {"résidence d'été des évêques de Rouen", "Le château olalala"}
    }
    @label(en) {
        "Gaillon Castle"
        "French Renaissance castle located in Gaillon, Normandy"
    }
    @label(es) {
        "Castillo de Gaillon"
    }

    is: [building/castle]
    image: "Chateau-de-Gaillon-27.jpg"
    inception: 1453
    territory: [Gaillon]
    owner: { // Syntaxic sugar to prevent duplication of owner
        [Royaume de France] {
            start_date: 1192
            end_date: 1262
        }
        [Archevèque de Rouen] {
            start_date: 1262
            end_date: 1789
        }
        [France] {
            start_date: 1804
        }
    }
// could also be done like this:
//   owner: [France] {
//      start_date: 1804
//   }
    address: `
        Allée du Château
        27600 Gaillon
    `
    website: "http://www.arc-gaillon.fr/index.php"
    phone: +33 2 32 51 48 50

    heritage_designation: {
        [monument historique inscrit] {
            start_date: 1862
            end_date: 1996
        }
        [monument historique classé] {
            start_date: 1996
        }
    }

    builders: {
        [John Mc Cain]
        [Viollet le Duc]
    }
}

