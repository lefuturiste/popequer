
parse_json() {
    cat $1.$2 | jq . > _$1.$2
    rm $1.$2
    mv _$1.$2 $1.$2
}

rm -rv $2
mkdir $2
cd $2

wget "https://www.wikidata.org/wiki/Special:EntityData/$1.json"
parse_json $1 "json"

wget "https://www.wikidata.org/wiki/Special:EntityData/$1.jsonld"
parse_json $1 "jsonld"

wget "https://www.wikidata.org/wiki/Special:EntityData/$1.ttl"

wget "https://www.wikidata.org/wiki/Special:EntityData/$1.rdf"
wget "https://www.wikidata.org/wiki/Special:EntityData/$1.n3"
