# TODO

- [x] most basic database
    - write: encode popequer bunch of objects into binary file
    - read: decode binaries and have a way to view them
    - only a index on ID field to quickly find later
    - no SQL
- [x] basic query CLI feature
    - search function that take O(n) on pure entries with basic filter language using claim property
- [ ] implement auto_labels function for Human, Events and things...
- [x] add macro or shortcut
- [x] handle one language quick labels as (name:, description:, alias:)
- [x] !labels shortcut expansion
- [x] function to take a ParsedEntry with details and turn it to a Entry struct removing every intermediate ParseOutput
- [ ] handle recursive entries definition.
- [ ] handle entry header shortcut like @Human, or @Event, or @Account, @Attendance, used to skip the use of `is:` claim
- [ ] fix macro cursor offset, but how?
- [ ] parse_extractor: add more errors
- [ ] parse_extractor: add test for Geo function
- [ ] indexing: add entry mutation (to add new ID) on new entry (have the indexer add a ID after first discovery of the items)
    - the requirement for that was to have the whole syntaxical tree available in the parse output of an entry
    - we now have that, we can now safely add *in text* the claim: `id: E000001`, that way we can identify each item later for update
- [ ] adapter: add vcard contacts exporter
- [ ] adapter: add ical calendar events exporter
- [ ] wikidata interface: add the upload action
- [ ] wikidata: have offline wikidata copy to quickly refer to items
    - wikidata compressed JSON dump is 80G
    - wikidata smaller dump
    - https://arxiv.org/pdf/2211.05416.pdf
    - https://github.com/phucty/wikidb

- advanced db features
    - [ ] construct custom indexs (eg. index for geocoords)

## 2023-08-10

### placement de l'ids, les labels

reflexions sur le placement de l'id, du nom et de la description

dans un notebook classique, on va avoir classiquement seulement qu'un seul language
donc c'est un peu superflut d'avoir le 

il faut un moyen de ne pas avoir a preciser {en: , fr:} etc

pour l'instant on fait sans, car en vrai avec un snippet ça devrais le faire

### moyen pour passer d'un objet `ParsedEntry` a `Entry` tout cours sans les wrapper de parsing

soit une macro qui reconstruit un Entry depuis un ParseEntry avec des references

### interactions avec git

notre indexer va devoir demander a git les dates de modification de tel ou tel partie de texte corespondant a tel ou tel partie de texte a partir des dates de commits.

## 2023-11-13

So okay, now with the !labels macro we have name, description, alias properties which are specials
So we can in theory delete all the special labels function that parsed the claims

## 2023-11-20

Okay now we have a way to take a Parsed Entry with complicated cursor attributes to a "Pure Entry" that only care about the abstractions.

Now another issue with macro expansion

if you have


```
{
    shit: !somemacro_expand_long(length: 500),
    another_code: 10
}
```

then it expand into
```
{
    shit: (
        50000000000000...0000000000000
    ),
    another_code: 10
}
```

the claim `another_code: 10` is offset by 2 lines

possible fixes and workaround:

- fix with dummy whitespaces
- have some way to parse the macro at the same time as the others components

so for example, parse into claims

other workaround:
- being able to have an offset table

ExpansionsList:
- input_range: [start, end]
  output_range: [start, end]

This mean we will have to ship ther expansion list and bring it with us at anytime.
So each time we have to do a manipulation we will need to first apply the expansions offsets before using the cursors positions.

## 2023-11-27: next milestone: a Vcard directory exporter

Export Human entries to a RFC 6350 compatible list.

I will use the [vcard lib](https://lib.rs/crates/vcard).

- disaventage using this lib: no support for vcard 3, but will be retrocompatible

add the vcard export in ppq probably behind a feature flag.

for that we need :
- stable ids generated for each entries to use in the vcard
    - offset annulation after shortcut expansion working
- a way to filter by [Human]
    - a way to recognize commons wikidata items like Q5
    - for now, hardcoded inside a .yaml
- for some fields a kind of value enhancement, like for phone number, need to have a formatted phone number after inputing
- the vcard exporter module
- handle VCard v3

later:
- validation
- need to handle ADDRESSBOOKSERVER, which is a special contacts entries with a list of contact id assigned to this group.

https://github.com/magiclen/vcard

## 2024-01-17

goal: vcf card contact exporter

for that, we need:
- filter by instance
- filter by property value
- reference resolver, to resolve [Human] to the right iterm
- name database with efficient indexing, probably some kind of token tree, but for now we will match against every name with fuzzy match

## 2024-01-25

goal: calendar listing

being able to list upcoming events to have a dashboard show up on every new day
have a dashboard on my mobile

for that we need:
- auto add uuid with id property on new entries
- indexer: property resolver
    - we need to look into the schema of defined properties and check if the property slug/name is valid
    - for a good Pure Entry, we must map a property slug (String) to a defined size (like u32)
    - u32 will still be bigger that most &str, but more efficient to process than String ??
    - go from Property::Custom to Property::Defined ?? no
    - just have a `struct Property { id: u32 }`
- indexer: reference resolver
    - scan first to get pure entries
    - index every pure entry labels into a HashMap or fuzzing index ?? that associate a string (name or alias) to the Entry pointer
    - then scan every entry for unresolved reference
        - try to resolve, try to match a SoftLocation in the label index
        - it's not bad if we cannot find a entry via SoftLocation, it will stay SoftLocation
        - otherwise, convert to a ResolvedReference (via UUID) the entries we found
        - if the unresolved location is a hard reference well, we just check that the uuid is correct, if not we throw an error, a HardLocation MUST resolve
    - we save the fuzzing index into the Notebook database for reuse later
    - we will finally get a list of Entry with the maximum of ResolvedReferences with hardlocation
- so now we will be able to filter things by instance, but with an item uuid
    - `let events = notebook.entries.filter_by_instance(notebook.entries.fuzzy_find("Event") + {additional_params: Not final} )`
    - NOTE: if an item have a property "instance_of" then it's a final class, otherwise it has a "subclass_of" property and it's not final
- next we define a CalendarView that list all upcoming events.

in neovim we will need something that will hide the ids

## 2024-01-26

- work on: auto add uuid with id property
    - [x] fix the application of add_id_on_entry_code when there is shortcut expansions
    - [x] fix the application of this multiple times, same thing we need to apply shortcut expansions
- work on: resolve links and use ids to create links to resolve references
    - ok resolve links is great

- so : goal : having calendar works
    - so we need to have a CalDAV sync target
    - so we need to find a rust server
    - so we will have to create a server that will sync popequer notebook and serve CalDAV via http
    - or we can also have the popequer mobile app use their sync version and create the CalDAV events locally
        - may be using " Calendar Provider API "
        - before that, we just need to filter the calendar events and list them

## 2024-01-31

- add tests for the resolver
- work on property resolution
- separate wikidata into a separate package
- work on a calendar interface (up coming events)

## 2024-02-02

- have a way to have stable ref to wikidata
- have a way to easily import a limited numbers of wikidata

so we can have a local wikidata db, and we fetch some wikidata items from wikidata http api and convert into ppq format

## 2024-02-03

- [x] now we have the list of upcoming events, we need to implement a better way to get all the items that are either instance of of instance of by sub.

## 2024-02-07

- add support for @Human, @Event etc
- add a cli checker, report warning

## 2024-02-17

- test: add support for virtual fs

- use Rope in the future to better handle chars, https://docs.rs/ropey/latest/ropey/

## 2024-02-19

- better preprocessing of inline comnments

## 2024-02-20

- handle the expansions offsets and apply them
- TODO: need to apply them on `parse_wrapper`
    - for now, the parse_markdown function will return, along the result the expansion list.
    - or we can do the preprocess in the parse_file before passing the code to parse markdown
    - then we need to have parse_wrapper accept an expansion list as optional parameter

## 2024-02-22

Okay so the expansions are kind of working correctly now, all the tests are green. That's good!

First attempt at test coverage.

- add more tests for indexing

- Move all tests in a new `tests` directory in each module so that the coverage tool ignore them

- fix(parser): detect shortcut
    - fix the fact that sometimes ! at an end of a string is wrongly considered a shortcut.
    - the shortcut pattern must be : !<lowercase with _ word> with opening parenthese

- support for description in a second opening bracket couple {}
- @Entry {/* claims */}{/* free markdown description with recursive entries */}

TODO: fix the bug when parsing markdown (multiple entries), when multiples entries are next to the next one (are tightly packed together without whitespace)

Target for v0.1 : being able to fully parse my personal main notebook.
for that we need to :
- [ ] refactor the code
- [ ] fix all the little parsing bugs
- [ ] being able to skip the entries that have errors
- [ ] have better reporting of errors, easier to understand and to spot
- [x] support for custom entry intro
- [ ] support for nested entries in the description and in the claim value
- [ ] better support for shortcuts: `!date` `!time` with raw date or time inside the shortcuts.

for v0.2:
- have basic LSP that can be used to jump to definition of reference.
- add local entry reference (to not always depend on ids when defining relationship locally)

## 2024-02-24 - implémentation de la logique des embedded entries

comment faire???

