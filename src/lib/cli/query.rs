use crate::cli::read_notebook_db;
use crate::cli::CliErr;
use crate::fs_notebook::NotebookContext;
use crate::database::search::search_entry_by_label;
use crate::database::search::search_complex;

use super::FeedbackChannel;

pub fn query_by_label(mut output: impl FeedbackChannel, context: &NotebookContext, query: &str)
    -> Result<(), CliErr> {
    let notebook = read_notebook_db(context)?;

    let res = search_entry_by_label(&notebook, &query);
    for (score, entry) in res.iter().take(5) {
        output.push_answer(&format!("{} - {} \n", score, entry.pure_entry.name()))
    }

    Ok(())
}

pub fn query_by_claims_filter(mut output: impl FeedbackChannel, context: &NotebookContext, complex_query: &str)
    -> Result<(), CliErr> {
    let notebook = read_notebook_db(context)?;

    let _ = search_complex(&notebook, &complex_query)
        .map_err(|err| 
            CliErr {
                message: format!("Failed to interpret complex query: {}", err),
                exit_code: 1
            }
        ).map(|ecs| {
            for entry_container in ecs.iter() {
                output.push_answer(&format!("{}", entry_container.pure_entry.name()))
            }
        });

    Ok(())
}
