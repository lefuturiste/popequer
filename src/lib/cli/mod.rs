pub mod status;
pub mod index;
pub mod list;
pub mod query;
pub mod export;
pub mod views;

use fully_pub::fully_pub;
use crate::database::read::read_db;
use crate::database::models::Notebook;
use crate::fs_notebook::NotebookContext;

#[fully_pub]
#[derive(Debug)]
struct CliErr {
    message: String,
    exit_code: i32
}

fn read_notebook_db(context: &NotebookContext) -> Result<Notebook, CliErr> {
    Ok(read_db(&context).map_err(
        |err| {
            CliErr {
                message: format!("Failed to read db: {:?}", err),
                exit_code: 2
            }
        })?)
}


/// Abstraction of stdout and stderr
pub trait FeedbackChannel {
    /// used to push the actual answer that can be processed by the next programms (stdout)
    fn push_answer(&mut self, log: &str) -> ();

    /// used to push errors, warnings or other logs to inform progress (stderr)
    fn push_log(&mut self, log: &str) -> ();
}

/// pushed to system stdout and stderr
pub struct SystemFeedback;
impl FeedbackChannel for SystemFeedback {
    fn push_answer(&mut self, log: &str) {
        println!("{}", log);
    }
    fn push_log(&mut self, log: &str) {
        eprintln!("{}", log);
    }
}

/// Ignore feedback
/// eg. used in quiet cases
#[derive(Debug)]
#[fully_pub]
struct BufferedFeedback {
    answer_lines: Vec<String>,
    log_lines: Vec<String>
}
impl FeedbackChannel for BufferedFeedback {
    fn push_answer(&mut self, log: &str) {
        self.answer_lines.push(log.to_string());
    }
    fn push_log(&mut self, log: &str) {
        self.log_lines.push(log.to_string());
    }
}

#[derive(Debug)]
pub struct NullFeedback;
impl FeedbackChannel for NullFeedback {
    fn push_answer(&mut self, _log: &str) {}
    fn push_log(&mut self, _log: &str) {}
}

