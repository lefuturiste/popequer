use crate::unwrap_or_return;
use crate::cli::CliErr;
use crate::fs_notebook::NotebookContext;
use crate::database::read::read_db;
use crate::exporters::vcard_exporter::export_vcard_collection;

pub fn export_vcard(context: &NotebookContext, vcard_collection_path: String) -> Result<String, CliErr> {
    let notebook = unwrap_or_return!(
        read_db(&context),
        |err| {
            CliErr {
                message: format!("Failed to read db: {:?}", err),
                exit_code: 2
            }
        }
    );

    let _ = export_vcard_collection(&notebook, &vcard_collection_path);

    Ok(
        "Exported vcard collection".to_string()
    )
}
