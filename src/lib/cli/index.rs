use crate::{fs_notebook::NotebookContext, indexer};

use super::{CliErr, FeedbackChannel};

pub fn index_notebook_once(mut output: impl FeedbackChannel, context: &NotebookContext) -> Result<(), CliErr> {
    output.push_log("Start indexing…");

    // do a normal full indexing
    let notebook = indexer::index_and_save(&context)
        .map_err(|err| {
            CliErr {
                exit_code: 2,
                message: format!("Failed to index notebook.\n {:?}", err)
            }
        })?;

    output.push_log(
        &format!("Successfully indexed a notebook with {} entries", notebook.entries.len())
    );
    Ok(())
}

// if *watch {
//     // TODO: Implement proper watch daemon
//     match indexer::index_watch_daemon(&notebook_context) {
//         Ok(()) => {
//             return
//         },
//         Err(_err) => {
//         }
//     }
// }
