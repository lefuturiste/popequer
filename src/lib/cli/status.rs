use crate::{database::models::Notebook, fs_notebook::NotebookContext};

use super::{read_notebook_db, CliErr, FeedbackChannel};

pub fn get_notebook_status(mut output: impl FeedbackChannel, context: &NotebookContext) -> Result<(), CliErr> {
    let notebook: Notebook = read_notebook_db(context)?;
    output.push_answer(&format!("Notebook has {} entries", notebook.entries.len()));

    Ok(())
}

