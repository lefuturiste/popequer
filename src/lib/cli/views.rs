use crate::cli::{read_notebook_db, CliErr};
use crate::fs_notebook::NotebookContext;

use super::FeedbackChannel;

pub fn calendar_view(mut output: impl FeedbackChannel, context: &NotebookContext) -> Result<(), CliErr> {
    let notebook = read_notebook_db(context)?;

    let events = crate::views::calendar::get_upcoming_events(&notebook)
        .map_err(|err| CliErr {
            exit_code: 2,
            message: format!("Cannot get upcoming events {:?}", err)
        })?;

    for event in events {
        output.push_answer(&format!("name: {:?}; from: {}; to: {:?}\n", event.name, event.start_time, event.end_time));
    }

    Ok(())
}
