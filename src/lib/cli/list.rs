use crate::database::ids::Id;
use crate::cli::CliErr;
use crate::fs_notebook::NotebookContext;

use super::{read_notebook_db, FeedbackChannel};

pub fn get_list(mut output: impl FeedbackChannel, context: &NotebookContext) -> Result<(), CliErr> {
    let notebook = read_notebook_db(context)?;

    // output the number of entries in the db
    output.push_log(&format!(
        "Got {} entries in {} sources files\n",
        notebook.entries.len(), notebook.files.len()
    ).to_string());

    // output a summary of what inside the notebook
    for entry_container in &notebook.entries {
        let rel_path = &entry_container.source_file(&notebook).path;
        output.push_answer(
            &format!("{:?} - {:?} - {:?}",
                rel_path,
                &entry_container.pure_entry.id,
                &entry_container.pure_entry.name())
        );
    }

    // TODO: Json output
    Ok(())
}

pub fn get_list_debug(mut output: impl FeedbackChannel, context: &NotebookContext) -> Result<(), CliErr> {
    let notebook = read_notebook_db(context)?;

    for entry_container in &notebook.entries {
        output.push_answer(&format!("{:#?}\n\n", entry_container.pure_entry));
    }

    Ok(())
}

pub fn get_one_debug(mut output: impl FeedbackChannel, context: &NotebookContext, id_repr: &str) -> Result<(), CliErr> {
    let notebook = read_notebook_db(context)?;

    let entry_id = Id::from_repr(&id_repr).map_err(|err| {
        CliErr {
            message: format!("Failed to parse id: {:?}", err),
            exit_code: 2
        }
    })?;

    // FIXME: use iter().find()
    for entry_container in &notebook.entries {
        if entry_container.pure_entry.id != entry_id {
            continue
        }
        output.push_answer(&format!("{:#?}", entry_container.pure_entry));
    }

    Ok(())
}

