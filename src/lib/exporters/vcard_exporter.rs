use anyhow::{Result, anyhow};
use crate::database::models::Notebook;
use crate::database::read::read_db;

pub fn export_vcard_collection(
    notebook: &Notebook,
    vcard_collections_path: &str
) -> Result<()> {
    
    // 1. filter all humans
    // 2. use external lib to generate
    for entry_container in &notebook.entries {
        dbg!(&entry_container.pure_entry);
    }

    Ok(())
}
