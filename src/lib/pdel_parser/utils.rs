use crate::pdel_parser::ParseError;

pub fn handle_choice_errors(parent_name: &str, errors: &Vec<ParseError>, choices: &Vec<&str>) -> ParseError {
    let mut highest_index: usize = 0;
    let mut highest_cursor: usize = 0;
    for (i, err) in errors.iter().enumerate() {
        if err.cursor > highest_cursor {
            highest_cursor = err.cursor;
            highest_index = i;
        }
    }

    let mut same_cursor: bool = true;
    for i in 0..errors.len() {
        if i == 0 { continue }
        if errors.get(i-1).unwrap().cursor != errors.get(i).unwrap().cursor {
            same_cursor = false;
        }
    }
    // all the cursor have advanced the same amount
    if same_cursor {
        return ParseError {
            msg: format!("{}: Expected one of {:?}", parent_name, choices),
            cursor: highest_cursor,
            ..Default::default()
        };
    }

    errors[highest_index].clone()
}


