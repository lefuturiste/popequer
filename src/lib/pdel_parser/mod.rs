use crate::utils::Substring;

pub mod values;
mod claim;
mod collection;
mod utils;
pub mod preprocess;
mod labels;
pub mod markdown;

#[cfg(test)]
mod test_claim;

#[cfg(test)]
mod test_collection;

#[cfg(test)]
mod test_parse;

#[cfg(test)]
mod test_markdown;

#[cfg(test)]
mod test_preprocess;

use fully_pub::fully_pub;
use serde::{Serialize, Deserialize};

use crate::get_symbol_or_final;

#[derive(Debug)]
enum ParserState {
    EntryHeader,
    EntryBody,
}

/// A unresolved reference to another internal entry or external entry
/// Still need checked and resolve the soft location
// TODO: parse and verify the syntax of the hard location
#[fully_pub]
#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
enum UnresolvedReference {
    SoftLocation(String), // inner: a query string, we don't know where it's going
    HardLocation(String) // inner: a URI-like object or a wikidata alias
}

#[fully_pub]
#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
struct PEntry {
    /// the name of the instance.
    /// eg. Human, Presentation, Event or SportSession
    intro: ParseOutput<String>,
    /// the list of claims
    claims: ParseOutput<Vec<ParseOutput<PEntryClaim>>>,
    /// a free text description which can be also used to define sub-claims later
    description: Option<ParseOutput<String>>
}

/// Argument in a Function call
#[fully_pub]
#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
enum PFunctionArgument {
    Named { name: String, value: PEntryValue },
    Positional(PEntryValue)
}

/// A parsed Function call, with parsed arguments
#[fully_pub]
#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
struct PFunction {
    name: ParseOutput<String>,
    arguments: Vec<ParseOutput<PFunctionArgument>>
}

/// A parsed list of value container
/// used to reuse a single property and define multiple claims
/// not meant to be in a final value
pub type EntryValueList = Vec<ParseOutput<PEntryValueContainer>>;

#[fully_pub]
#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
enum PEntryValue {
    Integer(i64),
    Float(f64),
    String(String),
    Reference(UnresolvedReference),
    List(EntryValueList),
    Dict(Vec<ParseOutput<PEntryClaim>>),
    Function(PFunction),
    /// recursive embeded Entry
    Entry(PEntry)
}

impl TryFrom<&PEntryValue> for f64 {
    type Error = ();

    fn try_from(value: &PEntryValue) -> Result<Self, Self::Error> {
        match value {
            PEntryValue::Float(value) => Ok(*value),
            _ => Err(())
        }
    }
}

impl TryFrom<&PEntryValue> for String {
    type Error = ();

    fn try_from(value: &PEntryValue) -> Result<Self, Self::Error> {
        match value {
            PEntryValue::String(value) => Ok(value.to_string()),
            _ => Err(())
        }
    }
}


/// this is a temp struct to contains the value and the qualifiers
/// this type is not meant to be in the final parsed value
#[fully_pub]
#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
struct PEntryValueContainer {
    value: ParseOutput<PEntryValue>,
    qualifiers: Vec<ParseOutput<PEntryClaim>>
}

#[fully_pub]
#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
struct PEntryClaim {
    property: ParseOutput<String>,
    value_container: ParseOutput<PEntryValueContainer>,
}

/// The failure mode of a parser
/// a location kind is useful when a parsing fail and we need to get not just the cursor position
/// but the semantic meaning associated with this particular position.
/// **currently used** to replace a ahead check in the parent parser of a component
/// *Example: we want to know if the parser failed to aquire the first characteristical tokens of 
/// a component (the Before position)*
#[derive(Debug, Clone, PartialEq)]
pub enum ParseLocationKind {
    Before,
    Header,
    Inside,
    After,
    Unknown
}

impl Default for ParseLocationKind {
    fn default() -> Self { ParseLocationKind::Unknown }
}

#[derive(Debug)]
#[derive(Clone)]
pub struct ParseError {
    msg: String,
    location_kind: ParseLocationKind,
    cursor: usize,
    /// A range where the error is located
    range: Option<(usize, usize)>
}

impl ParseError {
    pub fn from_parsed_output<T>(msg: &str, parse_output: &ParseOutput<T>) -> ParseError {
        ParseError {
            msg: msg.to_string(),
            location_kind: ParseLocationKind::Unknown,
            cursor: 0,
            range: Some((parse_output.start_loc, parse_output.end_loc))
        }
    }
}

impl Default for ParseError {
    fn default() -> Self {
        Self {
            msg: "Parse Error".to_string(),
            location_kind: ParseLocationKind::default(),
            cursor: 0,
            range: Some((0, 0))
        }
    }
}

#[fully_pub]
#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
struct ParseOutput<T> {
    /// the cursor position when the source object start
    start_loc: usize,
    
    /// the position when the source object end (replaces cursor)
    end_loc: usize,

    /// the parsed object tree
    p: T
}

pub const WHITESPACES_INLINE: [char; 2] = [' ', '\t'];
pub const WHITESPACES: [char; 3] = [' ', '\t', '\n'];

// /// represent a parsed 
// #[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
// pub struct ParsedEntryContainer {
//     tree: ParseOutput<PEntry>,
//     expansions: Vec<Expansion>
// }

#[fully_pub]
#[derive(Debug, Clone)]
struct TextPosition {
    line: usize,
    column: usize
}

#[fully_pub]
#[derive(Debug, Clone)]
struct AugmentedParseError {
    original_err: ParseError,
    human_text: String,
    range: Option<(TextPosition, TextPosition)>
}

/// a helper to help you parse something
/// will preprocess the code
/// then call the parser
/// if there is a parse error,
///     will process the parse error and try to give a context
///     will take care of the cursors offsets introduced by shortcut expansions
///     and show where the error occured in the subject
pub fn parse_wrapper<T>(
    parser: fn (&str, usize) -> Result<ParseOutput<T>, ParseError>,
    initial_subject: &str
) -> Result<ParseOutput<T>, AugmentedParseError>
{
    fn get_text_position_from_cursor(lines: &Vec<&str>, cursor: usize) -> TextPosition {
        let mut line_index = 0;
        let mut offset = cursor;
        let mut column_index = 0;
        for (i, line) in lines.iter().enumerate() {
            if line.len() >= offset {
                line_index = i;
                column_index = offset+1;
                break
            } else {
                offset -= line.len()+1;
            }
        }
        TextPosition {
            line: line_index,
            column: column_index
        }
    }

    fn process_after<T>(initial_subject: &str, inp: Result<T, ParseError>) -> Result<T, AugmentedParseError> {
        match inp {
            Ok(res) => { Ok(res) },
            Err(err) => {
                dbg!(&err);
                let mut human_text = String::new();

                let lines: Vec<&str> = initial_subject.split('\n').collect();
                let text_pos = get_text_position_from_cursor(&lines, err.cursor);

                let line_context = lines.iter().nth(text_pos.line).unwrap().to_string();
                let mut line_pointer = String::new();
                for i in 0..(line_context.len()+1) {
                    if i == text_pos.column-1 {
                        line_pointer.push('^');
                    } else {
                        line_pointer.push(' ');
                    }
                }
                human_text.push_str(&format!("in Line: {:?}", line_context));
                human_text.push_str(&format!("ptr       {}", line_pointer));
                
                Err(AugmentedParseError {
                    human_text,
                    range: err.range.map(|(s, e)|
                        (get_text_position_from_cursor(&lines, s), get_text_position_from_cursor(&lines, e))
                    ),
                    original_err: err,
                })
            }
        }
    }

    let preprocess_output = process_after(&initial_subject, preprocess::preprocess_code(initial_subject))?;
    let subject = preprocess_output.code_output;
    
    process_after(&subject, parser(&subject, 0))
}


/// we will look for Entry Intro which is one uppercase letter + CamelCase
fn parse_entry_intro(subject: &str, initial_cursor: usize) -> Result<ParseOutput<String>, ParseError> {
    let mut cursor = initial_cursor;
    
    #[derive(Debug, PartialEq)]
    enum State {
        Out,
        FirstLetter,
        InIntroName,
        Final
    }

    let mut state: State = State::Out;
    let mut intro_name = String::new();
    let mut section_start: usize = 0;
    
    loop {
        // debug_state_machine!("parse_entry_intro", subject, state, cursor);
        match state {
            State::Out => {
                let symbol = get_symbol_or_final!(subject, state, cursor);
                if symbol == '@' {
                    section_start = cursor;
                    state = State::FirstLetter;
                    cursor += 1;
                    continue;
                }
                if !WHITESPACES.contains(&symbol) {
                    return Err(
                        ParseError {
                            msg: "Unexpected non-whitespace char before EntryIntro @".into(),
                            location_kind: ParseLocationKind::Before,
                            cursor,
                            ..Default::default()
                        }
                    )
                }
            },
            State::FirstLetter => {
                let symbol = get_symbol_or_final!(subject, state, cursor);
                intro_name.push(symbol);
                if !symbol.is_uppercase() {
                    return Err(
                        ParseError {
                            msg: "Unexpected non-uppercase char in first char".into(),
                            location_kind: ParseLocationKind::Header,
                            cursor,
                            ..Default::default()
                        }
                    );
                }
                state = State::InIntroName;
            },
            State::InIntroName => {
                let symbol = get_symbol_or_final!(subject, state, cursor);
                if WHITESPACES.contains(&symbol) {
                    state = State::Final;
                    cursor -= 1;
                    continue;
                }
                if !symbol.is_alphabetic() {
                    return Err(
                        ParseError {
                            msg: "Unexpected non-alphabetic char".into(),
                            location_kind: ParseLocationKind::Inside,
                            cursor,
                            ..Default::default()
                        }
                    );
                }
                intro_name.push(symbol);
            },
            State::Final => {
                if intro_name.len() == 0 {
                    return Err(ParseError {
                        msg: "No intro name was found".into(),
                        location_kind: ParseLocationKind::Header,
                        cursor,
                        ..Default::default()
                    })
                }
                return Ok(ParseOutput {
                    start_loc: section_start,
                    end_loc: cursor,
                    p: intro_name
                })
            }
        }
        cursor += 1;
    }
}

fn parse_free_description(subject: &str, initial_cursor: usize) -> Result<ParseOutput<String>, ParseError> {
    #[derive(Debug, PartialEq)]
    enum State {
        Out,
        InFreeText,
        Final
    }
    let mut state = State::Out;
    let mut cursor = initial_cursor;
    let mut free_text = String::new();
    let mut brace_count = 0;

    loop {
        match state {
            State::Out => {
                let symbol = get_symbol_or_final!(subject, state, cursor);
                if symbol == '{' {
                    state = State::InFreeText;
                    continue;
                }
                if !WHITESPACES.contains(&symbol) {
                    return Err(ParseError {
                        msg: format!("Unexpected symbol {:?} before start of description", symbol),
                        location_kind: ParseLocationKind::Before, 
                        cursor,
                        ..Default::default()
                    })
                }
            },
            State::InFreeText => {
                let symbol = get_symbol_or_final!(subject, state, cursor);
                if symbol == '{' {
                    brace_count += 1;
                    cursor += 1;
                    continue;
                }
                if symbol == '}' {
                    brace_count -= 1;
                }
                if brace_count > 0  {
                    free_text.push(symbol);
                }
                if brace_count == 0 {
                    state = State::Final;
                    continue;
                }
            },
            State::Final => {
                if cursor == initial_cursor {
                    return Err(ParseError {
                        msg: "Unexpected end of input, no symbols parsed".into(),
                        location_kind: ParseLocationKind::Before, 
                        cursor,
                        ..Default::default()
                    })
                }
                return Ok(ParseOutput {
                    p: free_text.trim().to_owned(),
                    start_loc: initial_cursor,
                    end_loc: cursor
                })
            }
        }
        cursor += 1;
    }
}

pub const ENTRY_INTRO: &str = "@Entry";

/// Parse whole Entry
/// pattern: `@Entry { CLAIMS }`
/// in the case of a parse success
/// should return a tree of ParseOutput object that indicate the start and end of cursor for each
/// subobject
/// TODO: handle extra free markdown description beteween { } ; remove implementation of labels in
/// a specific dict { } 
pub fn parse_entry(subject: &str, initial_cursor: usize) -> Result<ParseOutput<PEntry>, ParseError> {

    #[derive(Debug, PartialEq)]
    enum State {
        Out,
        InClaimsDict,
        InFreeDescription,
        Final
    }
    let mut state = State::Out;
    let mut cursor = initial_cursor;

    let mut intro_opt: Option<ParseOutput<String>> = None;
    let mut claims_opt:
        Option<ParseOutput<Vec<ParseOutput<PEntryClaim>>>> = None;
    let mut description_opt: Option<ParseOutput<String>> = None;

    loop {
        // debug_state_machine!("parse_entry", subject, state, cursor);
        match state {
            State::Out => {
                // we will look for Entry Intro which is one uppercase letter + camelcase
                let intro = parse_entry_intro(subject, cursor)?;
                cursor = intro.end_loc;
                state = State::InClaimsDict; 
                intro_opt = Some(intro);
            },
            State::InClaimsDict => {
                match collection::parse_dict("Dict", &subject, cursor) {
                    Err(err) => {
                        // we failed to get the start delimiter, probably it's the end
                        // we don't change the cursor
                        if err.location_kind == ParseLocationKind::Before {
                            state = State::Final;
                            cursor -= 1;
                            continue;
                        }
                        return Err(err);
                    },
                    Ok(pout) => {
                        cursor = pout.end_loc;
                        claims_opt = Some(pout);
                        state = State::InFreeDescription;
                    }
                }
            },
            State::InFreeDescription => {
                match parse_free_description(&subject, cursor) {
                    Err(err) => {
                        if err.location_kind != ParseLocationKind::Before {
                            return Err(ParseError {
                                msg: format!("Failed to parse free description {}", err.msg),
                                ..err
                            })
                        }
                        cursor -= 1;
                    },
                    Ok(d) => {
                        cursor = d.end_loc;
                        description_opt = Some(d);
                    }
                };
                state = State::Final;
                continue;
            },
            State::Final => {
                let intro = match intro_opt {
                    None => {
                        return Err(ParseError {
                            msg: "Entry intro was not found".into(),
                            cursor,
                            ..Default::default()
                        })
                    },
                    Some(i) => i
                };
                return Ok(ParseOutput {
                    start_loc: intro.start_loc,
                    p: PEntry {
                        claims: match claims_opt {
                            Some(c) => c,
                            None => {
                                return Err(ParseError {
                                    msg: "Did not find a claims dict".into(),
                                    cursor,
                                    ..Default::default()
                                })
                            }
                        },
                        intro,
                        description: description_opt
                    },
                    end_loc: cursor
                })
            }
        }

        cursor += 1;
    }
}

pub fn parse_claims_query(initial_subject: &str)
    -> Result<ParseOutput<Vec<ParseOutput<PEntryClaim>>>, ParseError> {
    collection::parse_dict("Dict", &initial_subject, 0)
}

/// Utility function to check if a pattern is seen ahead
/// I had to create this function because using .get() on string gave really weird behaviour, seems
/// like the .get and the chars() arraw are offset (by 2 chars) and don't get the same char
fn look_ahead(subj: &str, initial_cursor: usize, pattern: &str) -> Option<()> {
    for (i, pattern_symbol) in pattern.chars().enumerate() {
        match subj.chars().nth(initial_cursor+i) {
            None => {
                return None;
            },
            Some(subj_symbol) if subj_symbol == pattern_symbol => {
                continue;
            },
            _ => return None
        }
    }
    Some(())
}

/// Utility function to get the content line of the current cursor
/// the output cursor is the position of the newline
fn extract_current_line(subj: &str, initial_cursor: usize) -> (usize, String) {
    let mut current_line = String::from("");
    let mut cursor = initial_cursor;
    loop {
        let symbol = match subj.chars().nth(cursor) {
            Some(s) => s,
            None => {
                return (cursor-1, current_line);
            }
        };
        if symbol == '\n' {
            return (cursor, current_line);
        }
        current_line.push(symbol);
        cursor += 1;
    }
}


/// bunch of utilities function to extract different types from EntryValue

pub fn get_string_entry_value(subj: &PEntryValue) -> Option<String> {
    match subj {
        PEntryValue::String(inner) => Some(inner.to_string()),
        _ => None
    }
}

pub fn get_list_entry_value(subj: &PEntryValue) -> Option<&EntryValueList> {
    match subj {
        PEntryValue::List(inner) => Some(inner),
        _ => None
    }
}

pub fn get_string_from_entry_value_container(subj: &PEntryValueContainer) -> Option<String> {
    get_string_entry_value(&subj.value.p)
}

pub fn get_string_list_from_entry_value(subj: &PEntryValue) -> Option<Vec<String>> {
    let inp_list = get_list_entry_value(&subj)?;
    let mut out: Vec<String> = vec![];
    for x in inp_list.iter() {
       out.push(get_string_entry_value(&x.p.value.p)?);
    }
    return Some(out);
}

pub fn get_string_list_from_entry_value_container(subj: &PEntryValueContainer) -> Option<Vec<String>> {
    return get_string_list_from_entry_value(&subj.value.p);
}

pub fn string_list_to_pdel_list(subject: Vec<String>) -> String {
    format!("({})", subject.iter().map(|s| format!("{:?}", s)).collect::<Vec<String>>().join(", "))
}
