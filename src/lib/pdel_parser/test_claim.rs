use crate::pdel_parser::claim::*;
use crate::pdel_parser::PEntryValue;

#[test]
fn test_parse_entry_claim() {
    let subj = "foo: 43".to_string();
    let res = parse_entry_claim(&subj, 0);
    assert!(res.is_ok(), "{:?}", res);
    let pout = res.unwrap();
    assert_eq!(pout.end_loc, 6);
    assert_eq!(pout.p.property.p, "foo");
    assert_eq!(pout.p.value_container.p.value.p, PEntryValue::Integer(43));

    // check for over-consumption of cursor
    let subj = "{ foo: 43    yes: 4".to_string();
    let res = parse_entry_claim(&subj, 2);
    assert!(res.is_ok(), "{:?}", res);
    let pout = res.unwrap();
    assert_eq!(pout.end_loc, 8);

    let subj = "foo_bar:43".to_string();
    let res = parse_entry_claim(&subj, 0);
    assert!(res.is_ok(), "{:?}", res);
    let pout = res.unwrap();
    assert_eq!(pout.p.property.p, "foo_bar");
    assert_eq!(pout.p.value_container.p.value.p, PEntryValue::Integer(43));

    let subj = "foo_bar: \"some string\"".to_string();
    let res = parse_entry_claim(&subj, 0);
    assert!(res.is_ok(), "{:?}", res);
    let pout = res.unwrap();
    assert_eq!(pout.p.property.p, "foo_bar");
    assert_eq!(pout.p.value_container.p.value.p, PEntryValue::String("some string".to_string()));
}

#[test]
fn test_parse_entry_claim_with_qualifiers() {
    let subj = "foo: 43 => { q1: 32 }".to_string();
    let res = parse_entry_claim(&subj, 0);
    assert!(res.is_ok(), "{:?}", res);
    let pout = res.unwrap();
    assert_eq!(pout.p.property.p, "foo");
    assert_eq!(pout.p.value_container.p.value.p, PEntryValue::Integer(43));

    let first_qualifier = &pout.p.value_container.p.qualifiers[0];
    assert_eq!(first_qualifier.p.property.p, "q1");
    assert_eq!(first_qualifier.p.value_container.p.value.p, PEntryValue::Integer(32));
    
    let subj = "foo: 10 => { a: 0 }".to_string();
    let res = parse_entry_claim(&subj, 0);
    assert!(res.is_ok(), "{:?}", res);
    let pout = res.unwrap();
    assert_eq!(pout.end_loc, 18);
}

