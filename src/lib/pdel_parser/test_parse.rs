use std::assert_matches::assert_matches;

use super::*;
use crate::pdel_parser::PEntryValue;
use crate::pdel_parser::values::container::parse_entry_value_container;
use crate::pdel_parser::parse_wrapper;
use crate::database::parse_extractor::{get_claim_values, get_claim_value};
use indoc::indoc;

#[test]
fn test_parse_simple_entry() {
    let subj = " @Entry {}".to_string();
    let res = parse_wrapper(parse_entry, &subj);
    assert!(res.is_ok(), "{:?}", res);
    let pout = res.unwrap();
    let claims = pout.p.claims.p;
    assert_eq!(claims.len(), 0);

    let subj = "@Entry { nbr: 43 str: \"a string\" }".to_string();
    let res = parse_wrapper(parse_entry, &subj);
    assert!(res.is_ok(), "{:?}", res);
    let pout = res.unwrap();

    let claims = pout.p.claims.p;
    assert_eq!(claims[0].p.property.p, "nbr");
    assert_eq!(claims[0].p.value_container.p.value.p, PEntryValue::Integer(43));
    assert_eq!(claims[1].p.property.p, "str");
    assert_eq!(claims[1].p.value_container.p.value.p, PEntryValue::String("a string".to_string()));


    let subj = "@Entry {some_nbr:43_40  str:\"Hello, World\"}".to_string();
    let res = parse_wrapper(parse_entry, &subj);
    assert!(res.is_ok(), "{:?}", res);
    let pout = res.unwrap();
    let claims = pout.p.claims.p;
    assert_eq!(claims[0].p.property.p, "some_nbr");
    assert_eq!(claims[0].p.value_container.p.value.p, PEntryValue::Integer(4340));
    assert_eq!(claims[1].p.property.p, "str");
    assert_eq!(claims[1].p.value_container.p.value.p, PEntryValue::String("Hello, World".to_string()));

    let subj = r#"
    @Entry {
        foo: 42,
        str: "Hello, World"
        wow: 54
    }
    "#.to_string();
    let res = parse_wrapper(parse_entry, &subj);
    assert!(res.is_ok(), "{:?}", res);
    let pout = res.unwrap();
    let claims = pout.p.claims.p;
    assert_eq!(claims[0].p.property.p, "foo");
    assert_eq!(claims[0].p.value_container.p.value.p, PEntryValue::Integer(42));
    assert_eq!(claims[1].p.property.p, "str");
    assert_eq!(claims[1].p.value_container.p.value.p, PEntryValue::String("Hello, World".to_string()));
    assert_eq!(claims[2].p.value_container.p.value.p, PEntryValue::Integer(54));

    let subj = r#"
    @Entry {
        str: "Hello, World",
        foo: ()
    }
    "#.to_string();
    let res = parse_wrapper(parse_entry, &subj);
    assert!(res.is_ok(), "{:?}", res);

    let subj = r#"
        @Entry {
            foo: 42,
            str: "Hello, World"
            wow: 54
        }
        "#.to_string();
    let res = parse_wrapper(parse_entry, &subj);
    assert!(res.is_ok(), "{:?}", res);
    let pout = res.unwrap();
    let claims = pout.p.claims.p;
    assert_eq!(claims[0].p.property.p, "foo");
    assert_eq!(claims[0].p.value_container.p.value.p, PEntryValue::Integer(42));
    assert_eq!(claims[1].p.property.p, "str");
    assert_eq!(claims[1].p.value_container.p.value.p, PEntryValue::String("Hello, World".to_string()));
    assert_eq!(claims[2].p.value_container.p.value.p, PEntryValue::Integer(54));
}

#[test]
fn test_parse_with_custom_intro() {
    let subj = r#"
    @CustomIntro {
        foo: "bar"
    }
    "#.to_string();
    let res = parse_wrapper(parse_entry, &subj);
    assert_matches!(res, Ok(_));
    let val = res.unwrap();
    assert_eq!(val.p.intro.p, "CustomIntro");
}

#[test]
fn test_parse_with_labels() {
    let subj = r#"
    @Entry {
        foo: (),
        
        !labels({
            en: ("Douglas Adams", "English science fiction writer and humourist (1952–2001)", ("DNA"))
            fr: ("Douglas Adams", "desc")
        })
    }
    "#.to_string();
    let res = parse_wrapper(parse_entry, &subj);
    assert!(res.is_ok(), "{:?}", res);
    let pout = res.unwrap();
    let claims = &pout.p.claims.p;
    assert!(get_claim_values(&pout.p, &"name").len() == 2);
    assert!(get_claim_values(&pout.p, &"description").len() == 2);
    assert!(get_claim_values(&pout.p, &"alias").len() == 1);
    assert_eq!(claims[1].p.property.p, "name");
    assert_eq!(claims[1].p.value_container.p.value.p, PEntryValue::String("Douglas Adams".to_string()));
}

#[test]
fn test_parse_spe2() {
    let subj = r#"
    "wow" => {
        foo: "bar"
    }
    "#.to_string();
    let res = parse_wrapper(parse_entry_value_container, &subj);
    assert!(res.is_ok(), "{:?}", res);
}


#[test]
fn test_parse_entry_complex1() {
    let subj = r#"
    @Entry {
        !labels({
            en: ("Douglas Adams", "English science fiction writer and humourist 1999–2001)", "DNA")
            fr: ("Douglas Adams", "desc")
        })
        educated_at: (
            "Q4961791" => {
                start_date: 1970,
                end_date: 1980
            },
            "QRANDOM" => {
                start_date: 1980,
                end_date: 1981
            }
        )
        eye_color: "brown"
    }
    "#.to_string();
    let res = parse_wrapper(parse_entry, &subj);
    dbg!(&res);
    assert!(res.is_ok(), "{:?}", res);
    let pout = res.unwrap();

    
    // verify labels
    // FIXME: verify labels with proper search in the claims

    // verify claims
    assert!(get_claim_value(&pout.p, &"educated_at").is_some());

    // FIXME: get rid of this match by using the provided functions in the extractor
    match get_claim_value(&pout.p, &"educated_at").unwrap() {
        PEntryValue::List(educated_values) => {
            assert_eq!(educated_values[0].p.value.p, PEntryValue::String("Q4961791".to_string()));
        },
        _ => unreachable!()
    }

    assert_eq!(*get_claim_value(&pout.p, &"eye_color").unwrap(), PEntryValue::String("brown".to_string()));

    // FIXME: check for different languages with qualifiers extractor, here we just check for the
    // english
    assert_eq!(*get_claim_value(&pout.p, &"name").unwrap(), PEntryValue::String("Douglas Adams".to_string()));
}

#[test]
fn test_parse_entry_abnormal_braces() {
    // empty entry
    let subj = indoc!{r#"
        @Entry
        {foo: 1
        }
        {free description
        }
    "#}.to_string();
    let res = parse_wrapper(parse_entry, &subj);
    assert_matches!(res, Ok(_));
}

#[test]
fn test_parse_entry_cursor_position() {
    // empty entry
    let subj = "@Entry {} {}".to_string();
    let res = parse_wrapper(parse_entry, &subj);
    assert!(res.is_ok(), "{:?}", res);
    let pout = res.unwrap();
    assert_eq!(pout.end_loc, 11);

    let subj = "@Entry {} {}\n";
    let res = parse_wrapper(parse_entry, &subj);
    assert!(res.is_ok(), "{:?}", res);
    let pout = res.unwrap();
    println!("{}", pout.end_loc);
    // we should not consume the whitespaces after
    assert_eq!(pout.end_loc, 11);
}


#[test]
fn test_parse_entry_output_with_location() {
    // empty entry
    let subj = r#"
        @Entry {
            claimA: "value A",
            claimB: "value B",
                    claimC:         
                        "valueC",
        }
    "#.to_string();
    let res = parse_wrapper(parse_entry, &subj);
    assert!(res.is_ok(), "{:?}", res);
    let pout = res.unwrap();

    assert_eq!(subj.chars().nth(pout.start_loc).unwrap(), '@');
    assert_eq!(pout.start_loc, 9);
}

#[test]
fn test_parse_with_optional_description_scope() {
    // test the possible syntax: @Entry { CLAIMS } { DESCRIPTION }
    let subj = indoc!{r#"
        @Entry {
            claim_a: "Wow this is a value"
        } {
            This is an optional free-text description associated with the entry.
        }
    "#}.to_string();
    let res = parse_wrapper(parse_entry, &subj);
    assert!(res.is_ok(), "{:?}", res);
    let pout = res.unwrap();
    assert_eq!(pout.p.description.unwrap().p, "This is an optional free-text description associated with the entry.");
}

#[test]
fn test_parse_free_description() {
    let subj = indoc!{r#"
        {
            Hello this is a free description
        }
    "#};
    let res = parse_free_description(subj, 0);
    assert_matches!(res, Ok(_));
    let val = res.unwrap();
    dbg!(&val);
    assert!(val.p.contains("Hello this is a free description"));

    let subj = indoc!{r#"
        {Foobared}
    "#};
    let res = parse_free_description(subj, 0);
    assert_matches!(res, Ok(_));
    let val = res.unwrap();
    dbg!(&val);
    assert!(val.p.contains("Foobared"));
}

#[test]
fn test_parse_free_description_empty() {
    let subj = "{}";
    let res = parse_free_description(subj, 0);
    assert_matches!(res, Ok(_));
    let val = res.unwrap();
    assert!(val.p.is_empty());
    assert_eq!(val.start_loc, 0);
    assert_eq!(val.end_loc, 1);
}

#[test]
fn test_parse_entry_intro() {
    let subj = "@Entry {";
    let res = parse_entry_intro(subj, 0);
    let val = res.unwrap();
    assert_eq!(val.start_loc, 0);
    assert_eq!(val.end_loc, 5);
    assert_eq!(val.p, "Entry");
}

#[test]
fn test_parse_entry_intro_camel_case() {
    let subj = "@EntryLongCamelCase {";
    let res = parse_entry_intro(subj, 0);
    let val = res.unwrap();
    assert_eq!(val.p, "EntryLongCamelCase");
}

#[test]
fn test_parse_entry_intro_reject_invalid_name_first_char() {
    let subj = "@invalid {";
    let res = parse_entry_intro(subj, 0);
    assert_matches!(res, Err(_));
    let err_val = res.unwrap_err();
    
    assert_eq!(err_val.location_kind, ParseLocationKind::Header);
    assert!(err_val.msg.contains("Unexpected non-uppercase char in first char"));
}

#[test]
fn test_parse_entry_intro_reject_invalid_name() {
    // snake case is disallowed
    let subj = "@Entry_invalid_name {";
    let res = parse_entry_intro(subj, 0);
    assert_matches!(res, Err(_));
    let err_val = res.unwrap_err();
    
    assert_eq!(err_val.location_kind, ParseLocationKind::Inside);
    assert!(err_val.msg.contains("Unexpected non-alphabetic char"));
}

#[test]
fn test_parse_entry_intro_reject_no_name() {
    let subj = "@";
    let res = parse_entry_intro(subj, 0);
    assert_matches!(res, Err(_));
    let err_val = res.unwrap_err();
    assert!(err_val.msg.contains("No intro name was found"));
}

