use crate::pdel_parser::{ParseError, ParseLocationKind, ParseOutput, WHITESPACES, PEntryClaim, PEntryValueContainer,};
use crate::pdel_parser::values::container::parse_entry_value_container;
use crate::pdel_parser::claim::parse_entry_claim;

pub fn parse_list_generalized<T>(
    pattern_name: &str,
    element_parser: fn (&str, usize) -> Result<ParseOutput<T>, ParseError>,
    delimiter_start: char,
    delimiter_end: char,
    subject: &str, initial_cursor: usize
) -> Result<ParseOutput<Vec<ParseOutput<T>>>, ParseError>
{
    #[derive(PartialEq)]
    #[derive(Debug)]
    enum State {
        Out,
        InElement,
        InSeparator,
        Final
    }
    let mut state = State::Out;
    let mut cursor = initial_cursor;
    let mut local_cursor = 0;
    // the cursor at the first opening delimiter
    let mut start_cursor = 0;

    let mut elements: Vec<ParseOutput<T>> = vec![];
    loop {
        let symbol = match subject.chars().nth(cursor) {
            None => {
                return Err(ParseError {
                    msg: format!("Unexpected end of string while parsing {}", pattern_name),
                    cursor,
                    location_kind: match state {
                        State::Out => ParseLocationKind::Before,
                        _ => ParseLocationKind::Unknown
                    },
                    ..Default::default()
                });
            },
            Some(x) => x
        };

        match state {
            State::Out => {
                if symbol == delimiter_start {
                    start_cursor = cursor;
                    state = State::InSeparator;
                    local_cursor = 1;
                    cursor += 1;
                    continue;
                }
                if !WHITESPACES.contains(&symbol) {
                    // delimiter not seen
                    return Err(ParseError {
                        msg: format!("Unexpected {} before {}", symbol, pattern_name),
                        location_kind: ParseLocationKind::Before, 
                        cursor,
                        ..Default::default()
                    })
                }
            }
            State::InSeparator => {
                if symbol == delimiter_end {
                    state = State::Final;
                    continue;
                }
                if local_cursor > 0 && !WHITESPACES.contains(&symbol) && symbol != ',' {
                    state = State::InElement;
                    local_cursor = 0;
                    continue;
                } 
                if !WHITESPACES.contains(&symbol) && symbol != ',' {
                    return Err(ParseError {
                        msg: format!("Expected an element separator, got {:?} in {}", symbol, pattern_name),
                        location_kind: ParseLocationKind::Inside,
                        cursor,
                        ..Default::default()
                    })
                }
                local_cursor += 1;
            },
            State::InElement => {
                match element_parser(subject, cursor) {
                    Err(err) => {
                        return Err(err);
                    },
                    Ok(output) => {
                        cursor = output.end_loc;
                        elements.push(output);
                        local_cursor = 0;
                        state = State::InSeparator;
                    }
                }
            },
            State::Final => {
                return Ok(ParseOutput {
                    p: elements,
                    start_loc: start_cursor,
                    end_loc: cursor
                })
            }
        }
        cursor += 1;
    }
}

/// parse list of values in form of: `( VALUE, VALUE, ... )`
pub fn parse_list(subject: &str, initial_cursor: usize) 
    -> Result<ParseOutput<Vec<ParseOutput<PEntryValueContainer>>>, ParseError>
{
    parse_list_generalized("List", parse_entry_value_container, '(', ')', subject, initial_cursor)
}

/// parse claims collection in form of: `{ PROPERTY : VALUE_CONTAINER, PROPERTY: VALUE_CONTAINER, ... }`
/// (you will need the curly braces)
pub fn parse_dict(name: &str, subject: &str, initial_cursor: usize)
    -> Result<ParseOutput<Vec<ParseOutput<PEntryClaim>>>, ParseError>
{
    parse_list_generalized(name, parse_entry_claim, '{', '}', subject, initial_cursor)
}

pub fn parse_entry_claims(subject: &str, initial_cursor: usize)
    -> Result<ParseOutput<Vec<ParseOutput<PEntryClaim>>>, ParseError>
{
   parse_dict("Claims", subject, initial_cursor)
}

pub fn parse_entry_labels(subject: &str, initial_cursor: usize)
    -> Result<ParseOutput<Vec<ParseOutput<PEntryClaim>>>, ParseError>
{
   parse_dict("Labels", subject, initial_cursor)
}

pub fn parse_entry_qualifiers(subject: &str, initial_cursor: usize)
    -> Result<ParseOutput<Vec<ParseOutput<PEntryClaim>>>, ParseError>
{
   parse_dict("Qualifiers", subject, initial_cursor)
}

