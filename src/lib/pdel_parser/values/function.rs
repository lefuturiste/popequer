use crate::pdel_parser::{
    PEntryValue, PFunction, PFunctionArgument, ParseError, ParseLocationKind, ParseOutput, WHITESPACES, WHITESPACES_INLINE
};
use crate::pdel_parser::values::parse_entry_value;
use crate::pdel_parser::claim::parse_key;
use crate::pdel_parser::collection::parse_list_generalized;
use crate::pdel_parser::utils::handle_choice_errors;

fn parse_function_named_argument(subject: &str, initial_cursor: usize)
    -> Result<ParseOutput<PFunctionArgument>, ParseError>
{
    let mut cursor = initial_cursor;
    let name: String = match parse_key(subject, cursor) {
        Err(err) => return Err(err),
        Ok(output) => {
            cursor = output.end_loc;
            output.p
        }
    };
    let value: PEntryValue = match parse_entry_value(subject, cursor) {
        Err(err) => return Err(err),
        Ok(output) => {
            cursor = output.end_loc;
            output.p
        }
    };

    Ok(ParseOutput {
        p: PFunctionArgument::Named {
            name,
            value
        },
        start_loc: initial_cursor,
        end_loc: cursor
    })
}

fn parse_function_positional_argument(subject: &str, initial_cursor: usize)
    -> Result<ParseOutput<PFunctionArgument>, ParseError>
{
    let cursor = initial_cursor;
    match parse_entry_value(subject, cursor) {
        Err(err) => Err(err),
        Ok(output) => {
            Ok(ParseOutput {
                p: PFunctionArgument::Positional(output.p),
                start_loc: initial_cursor,
                end_loc: output.end_loc
            })
        }
    }
}

/// accept pattern: `(KEY: VALUE|VALUE)`
fn parse_function_argument(subject: &str, initial_cursor: usize)
    -> Result<ParseOutput<PFunctionArgument>, ParseError>
{
    let cursor = initial_cursor;

    let mut errors: Vec<ParseError> = vec![];
    match parse_function_named_argument(subject, cursor) {
        Ok(pout) => {
            return Ok(pout)
        },
        Err(err) => {
            errors.push(err);
        }
    }
    match parse_function_positional_argument(subject, cursor) {
        Ok(pout) => {
            return Ok(pout)
        },
        Err(err) => {
            errors.push(err);
        }
    }

    let choice_error = handle_choice_errors("FunctionArgument", &errors, &vec!["PositionalArgument", "NamedArgument"]);
    Err(ParseError {
        msg: choice_error.msg,
        cursor: choice_error.cursor,
        ..Default::default()
    })
}

/// acccept pattern: `(FUNCTION_ARGUMENT, FUNCTION_ARGUMENT...)`
pub fn parse_function_arguments(subject: &str, initial_cursor: usize)
    -> Result<ParseOutput<Vec<ParseOutput<PFunctionArgument>>>, ParseError>
{
    parse_list_generalized("Function Arguments", parse_function_argument,  '(', ')', subject, initial_cursor)
}

/// accept pattern: `FUNCTION_NAME ARGUMENTS`
pub fn parse_function(subject: &str, initial_cursor: usize)
    -> Result<ParseOutput<PFunction>, ParseError>
{
    #[derive(PartialEq)]
    #[derive(Clone)]
    enum State {
        Whitespace,
        InHeader,
        InArguments
    }
    let mut state = State::Whitespace;
    let target_state = State::InHeader;
    let mut cursor = initial_cursor;

    let mut name = String::new();

    loop {
        let symbol = match subject.chars().nth(cursor) {
            None => {
                return Err(ParseError {
                    msg: "Unexpected end of subject while parsing function".into(),
                    cursor,
                    location_kind: match state {
                        State::Whitespace => ParseLocationKind::Before,
                        _ => ParseLocationKind::Unknown
                    },
                    ..Default::default()
                });
            },
            Some(symbol) => symbol
        };
        match state {
            State::Whitespace => {
                if !WHITESPACES.contains(&symbol) {
                    state = target_state.clone();
                    continue;
                }
                // consumed_whitespaces += 1;
            },
            State::InHeader => {
                if name.len() > 1 && (WHITESPACES_INLINE.contains(&symbol) || symbol == '(') {
                    state = State::InArguments;
                    continue;
                }

                if !symbol.is_ascii_alphanumeric() {
                    return Err(ParseError {
                        msg: format!("Unexpected non alphanumeric symbol {} in Function header", symbol),
                        cursor,
                        location_kind: ParseLocationKind::Header,
                        ..Default::default()
                    })
                }
                name.push(symbol);
            },
            State::InArguments => {
                let arguments = match parse_function_arguments(subject, cursor) {
                    Ok(pout) => {
                        cursor = pout.end_loc;
                        pout.p
                    },
                    Err(err) => {
                        return Err(err);
                    }
                };
                return Ok(ParseOutput {
                    p: PFunction {
                        name: ParseOutput {
                            start_loc: initial_cursor,
                            end_loc: initial_cursor + name.len(),
                            p: name,
                        },
                        arguments
                    },
                    start_loc: initial_cursor,
                    end_loc: cursor,
                });
            }
        }
       
        cursor += 1;
    }
}

