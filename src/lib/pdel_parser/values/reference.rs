use crate::pdel_parser::{ ParseError, ParseOutput, UnresolvedReference, ParseLocationKind };
use crate::pdel_parser::values::string::parse_string_generalized;

/// Parse a reference to an UnresolvedReference
/// it output 2 cases:
/// - 1. SoftLocation:
///     - `[query string/soft location]`, we would need to query and find the hard location for this soft location later
/// - 2. HardLocation:
///     - `[query string/soft location](hard location)`, the query string is ignored as we only care about the hard location
///     - `[](hard location)`, only the hard location is inputed
pub fn parse_reference(subject: &str, initial_cursor: usize) -> Result<ParseOutput<UnresolvedReference>, ParseError>
{
    // required loose reference (like a name or a slug) in bracket
    // can be empty
    // accept parse error when it fail to aquire the start delimiter
    let soft_ref_po: ParseOutput<String> =
        match parse_string_generalized("Reference Query String", subject, initial_cursor, '[', ']') {
            Err(err) => {
                return Err(err)
            },
            Ok(po) => po
        };

    // optional strict reference (eg. Q5 or wdt:Q5 or https://wikidata.org/wiki/Q5)
    match parse_string_generalized(
        "Reference Hard Location", subject,
        soft_ref_po.end_loc + 1,
        '(', ')'
    ) {
        Ok(hard_ref_po) => {
            // TODO: parse hard location to allow for more granularity (like : the target (inner,
                // extrernal ?))
            return Ok(ParseOutput {
                p: UnresolvedReference::HardLocation(hard_ref_po.p),
                start_loc: initial_cursor,
                end_loc: hard_ref_po.end_loc
            })
        },
        Err(hard_ref_err) if hard_ref_err.location_kind == ParseLocationKind::Before => {
            return Ok(ParseOutput {
                p: UnresolvedReference::SoftLocation(soft_ref_po.p),
                start_loc: initial_cursor,
                end_loc: soft_ref_po.end_loc
            })
        },
        Err(hard_ref_err) => {
            return Err(hard_ref_err)
        }
    }
}

