use crate::pdel_parser::values::reference::parse_reference;
use crate::pdel_parser::UnresolvedReference;

#[test]
fn test_soft_reference_value() {
    let subj = "  [foobar] ".to_string();
    let res = parse_reference(&subj, 0);
    assert!(res.is_ok(), "{:?}", res);
    let pout = res.unwrap();
    // assert!(matches!(pout.p, UnresolvedReference::SoftLocation { .. }));
    assert_eq!(pout.p, UnresolvedReference::SoftLocation("foobar".to_string()));
    assert_eq!(pout.end_loc, 9);
}

#[test]
fn test_hard_reference_value() {
    let subj = "  [foobar](hard ref) ".to_string();
    let res = parse_reference(&subj, 0);
    assert!(res.is_ok(), "{:?}", res);
    let pout = res.unwrap();
    assert_eq!(pout.p, UnresolvedReference::HardLocation("hard ref".to_string()));

    let subj = "  [](wdt:Q42) ".to_string();
    let res = parse_reference(&subj, 0);
    assert!(res.is_ok(), "{:?}", res);
    let pout = res.unwrap();
    assert_eq!(
        pout.p, 
        UnresolvedReference::HardLocation("wdt:Q42".to_string())
    );

    let subj = "  [](https://wikibase.university.edu/wiki/Q42) ".to_string();
    let res = parse_reference(&subj, 0);
    assert!(res.is_ok(), "{:?}", res);
    let pout = res.unwrap();
    assert_eq!(
        pout.p, 
        UnresolvedReference::HardLocation("https://wikibase.university.edu/wiki/Q42".to_string())
    );
}

