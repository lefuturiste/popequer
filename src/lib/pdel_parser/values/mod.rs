use crate::pdel_parser::{ParseError, ParseOutput, PEntryValue};
use crate::pdel_parser::collection::{parse_list, parse_dict};
use crate::pdel_parser::utils::handle_choice_errors;

use super::parse_entry;

pub mod number;
pub mod string;
pub mod reference;
pub mod container;
pub mod function;

#[cfg(test)]
mod test_values;

#[cfg(test)]
mod test_string;

#[cfg(test)]
mod test_number;

#[cfg(test)]
mod test_reference;

#[cfg(test)]
mod test_function;

pub fn parse_entry_value(subject: &str, initial_cursor: usize) -> Result<ParseOutput<PEntryValue>, ParseError>
{
    let cursor = initial_cursor;

    // L'erreur qu'on doit retourner c'est celle ou on est allé le plus loin (on regarde avec
    // le curseur!! )
    // TODO: optimize to use static sied array at runtime
    // let mut errors: [Option<ParseError>; 2] = [None, None];
    let mut errors: Vec<ParseError> = vec![];
    match number::parse_float(subject, cursor) {
        Ok(output) => {
            return Ok(ParseOutput {
                p: PEntryValue::Float(output.p),
                start_loc: initial_cursor,
                end_loc: output.end_loc
            })
        },
        Err(err) => {
            errors.push(err);
        }
    }
    match number::parse_integer(subject, cursor) {
        Ok(output) => {
            return Ok(ParseOutput {
                p: PEntryValue::Integer(output.p),
                start_loc: initial_cursor,
                end_loc: output.end_loc
            })
        },
        Err(err) => {
            errors.push(err);
        }
    }
    match string::parse_string(subject, cursor) {
        Ok(output) => {
            return Ok(ParseOutput {
                p: PEntryValue::String(output.p),
                start_loc: initial_cursor,
                end_loc: output.end_loc
            })
        },
        Err(err) => {
            errors.push(err);
        }
    }
    match reference::parse_reference(subject, cursor) {
        Ok(output) => {
            return Ok(ParseOutput {
                p: PEntryValue::Reference(output.p),
                start_loc: initial_cursor,
                end_loc: output.end_loc
            })
        },
        Err(err) => {
            errors.push(err);
        }
    }
    match function::parse_function(subject, cursor) {
        Ok(output) => {
            return Ok(ParseOutput {
                p: PEntryValue::Function(output.p),
                start_loc: initial_cursor,
                end_loc: output.end_loc
            })
        },
        Err(err) => {
            errors.push(err);
        }
    }
    match parse_list(subject, cursor) {
        Ok(output) => {
            return Ok(ParseOutput {
                p: PEntryValue::List(output.p),
                start_loc: initial_cursor,
                end_loc: output.end_loc
            })
        },
        Err(err) => {
            errors.push(err);
        }
    }
    match parse_dict("General dict", subject, cursor) {
        Ok(output) => {
            return Ok(ParseOutput {
                p: PEntryValue::Dict(output.p),
                start_loc: initial_cursor,
                end_loc: output.end_loc
            })
        },
        Err(err) => {
            errors.push(err);
        }
    }
    match parse_entry(subject, cursor) {
        Ok(output) => {
            return Ok(ParseOutput {
                p: PEntryValue::Entry(output.p),
                start_loc: initial_cursor,
                end_loc: output.end_loc
            })
        },
        Err(err) => {
            errors.push(err);
        }
    }
    // println!("List of errros {:?}", errors);
    // find the ParseError with the highest cursor and promote as the actual error
    let choice_error = handle_choice_errors("EntryValue", &errors, &vec![
        "Integer",
        "Float",
        "String",
        "Reference",
        "List",
        "Function",
        "Entry"
    ]);
    Err(choice_error)
}
