use crate::pdel_parser::values::string::parse_string;

#[test]
fn test_string_value() {
    let subj = "\"foobar\"".to_string();
    let res = parse_string(&subj, 0);
    assert!(res.is_ok(), "{:?}", res);
    let pout = res.unwrap();
    assert_eq!(pout.p, "foobar");
    assert_eq!(pout.end_loc, 7);

    let subj = "  \"another foobar\" lel".to_string();
    let res = parse_string(&subj, 0);
    assert!(res.is_ok(), "{:?}", res);
    let pout = res.unwrap();
    assert_eq!(pout.p, "another foobar");
    assert_eq!(pout.end_loc, 17);

    let subj = "  \"lel\"     ".to_string();
    let res = parse_string(&subj, 0);
    assert!(res.is_ok(), "{:?}", res);
    let pout = res.unwrap();
    assert_eq!(pout.p, "lel");
    assert_eq!(pout.end_loc, 6);

    let subj = r#"  "English science fiction writer and humourist 1952–2001"     "#.to_string();
    let res = parse_string(&subj, 0);
    assert!(res.is_ok(), "{:?}", res);
    let pout = res.unwrap();
    assert_eq!(pout.p, "English science fiction writer and humourist 1952–2001");
}
