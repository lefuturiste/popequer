use crate::pdel_parser::values::number::{
    parse_integer,
    parse_float
};

#[test]
fn test_integer_value() {
    let subj = "42".to_string();
    let res = parse_integer(&subj, 0);
    assert!(res.is_ok());
    let pout = res.unwrap();
    assert_eq!(pout.p, 42);
    assert_eq!(pout.start_loc, 0);
    assert_eq!(pout.end_loc, 1);

    let subj = "42 ".to_string();
    let res = parse_integer(&subj, 0);
    assert!(res.is_ok());
    let pout = res.unwrap();
    assert_eq!(pout.p, 42);
    assert_eq!(pout.start_loc, 0);
    assert_eq!(pout.end_loc, 1);

    let subj = "     1435   ".to_string();
    let res = parse_integer(&subj, 0);
    assert!(res.is_ok());
    let pout = res.unwrap();
    assert_eq!(pout.p, 1435);
    assert_eq!(pout.start_loc, 5);
    assert_eq!(pout.end_loc, 8);

    let subj = "     14 s".to_string();
    let res = parse_integer(&subj, 0);
    assert!(res.is_ok());
    let pout = res.unwrap();
    assert_eq!(pout.p, 14);
    assert_eq!(pout.start_loc, 5);
    assert_eq!(pout.end_loc, 6);

    let subj = "1435".to_string();
    let res = parse_integer(&subj, 0);
    assert!(res.is_ok());
    let pout = res.unwrap();
    assert_eq!(pout.p, 1435);
    assert_eq!(pout.start_loc, 0);
    assert_eq!(pout.end_loc, 3);

    let subj = " 144_540".to_string();
    let res = parse_integer(&subj, 0);
    assert!(res.is_ok());
    let pout = res.unwrap();
    assert_eq!(pout.p, 144_540);
    assert_eq!(pout.start_loc, 1);
    assert_eq!(pout.end_loc, 7);

    let subj = " 54659457".to_string();
    let res = parse_integer(&subj, 0);
    assert!(res.is_ok());
    let pout = res.unwrap();
    assert_eq!(pout.p, 54659457);
    assert_eq!(pout.start_loc, 1);
    assert_eq!(pout.end_loc, 8);

    let subj = "-42".to_string();
    let res = parse_integer(&subj, 0);
    let pout = res.unwrap();
    assert_eq!(pout.p, -42);
    assert_eq!(pout.start_loc, 0);
    assert_eq!(pout.end_loc, 2);

    let subj = " -  43".to_string();
    let res = parse_integer(&subj, 0);
    let pout = res.unwrap();
    assert_eq!(pout.p, -43);
    assert_eq!(pout.start_loc, 1);
    assert_eq!(pout.end_loc, 5);
}

#[test]
fn test_float_value() {
    let subj = "   0003.14159_2654".to_string();
    let res = parse_float(&subj, 0);
    assert!(res.is_ok());
    let pout = res.unwrap();
    assert_eq!(pout.p, 3.141592654);
    assert_eq!(pout.start_loc, 3);

    let subj = "3.141592654".to_string();
    let res = parse_float(&subj, 0);
    assert!(res.is_ok());
    let pout = res.unwrap();
    assert_eq!(pout.p, 3.141592654);
    assert_eq!(pout.start_loc, 0);

    let subj = "  13.56  ".to_string();
    let res = parse_float(&subj, 0);
    assert!(res.is_ok());
    let pout = res.unwrap();
    assert_eq!(pout.p, 13.56);
    assert_eq!(pout.start_loc, 2);

    let subj = "  1356.0  ".to_string();
    let res = parse_float(&subj, 0);
    assert!(res.is_ok());
    let pout = res.unwrap();
    assert_eq!(pout.p, 1356.0);

    let subj = " - 1054.43054".to_string();
    let res = parse_float(&subj, 0);
    let pout = res.unwrap();
    assert_eq!(pout.p, -1054.43054);
    assert_eq!(pout.start_loc, 1);
}

