use crate::pdel_parser::{
    PEntryValue,
    PFunctionArgument
};
use crate::pdel_parser::values::function::parse_function;

#[test]
fn test_function_with_positional_arguments() {
    let subj = "  Geo(132, 54) ".to_string();
    let res = parse_function(&subj, 0);
    assert!(res.is_ok(), "{:?}", res);
    let pout = res.unwrap();
    assert_eq!(pout.p.name.p, "Geo");
    assert_eq!(pout.p.arguments[0].p, PFunctionArgument::Positional(PEntryValue::Integer(132)));
    assert_eq!(pout.p.arguments[1].p, PFunctionArgument::Positional(PEntryValue::Integer(54)));

    let subj = "  FooBar (132) ".to_string();
    let res = parse_function(&subj, 0);
    assert!(res.is_ok(), "{:?}", res);
    let pout = res.unwrap();
    assert_eq!(pout.p.name.p, "FooBar");
    assert_eq!(pout.p.arguments[0].p, PFunctionArgument::Positional(PEntryValue::Integer(132)));
}

#[test]
fn test_function_with_named_arguments() {
    let subj = "  Geo(lat: 132, lng: 54) ".to_string();
    let res = parse_function(&subj, 0);
    assert!(res.is_ok(), "{:?}", res);
    let pout = res.unwrap();
    assert_eq!(pout.p.name.p, "Geo");
    assert_eq!(
        pout.p.arguments[0].p,
        PFunctionArgument::Named { name: "lat".to_string(), value: PEntryValue::Integer(132) }
    );
    assert_eq!(
        pout.p.arguments[1].p,
        PFunctionArgument::Named { name: "lng".to_string(), value: PEntryValue::Integer(54) }
    );
}

#[test]
fn test_function_with_mixed_arguments() {
    let subj = "Function(54, x: 13, y: 31) ".to_string();
    let res = parse_function(&subj, 0);
    assert!(res.is_ok(), "{:?}", res);
    let pout = res.unwrap();
    assert_eq!(pout.p.name.p, "Function");
    assert_eq!(
        pout.p.arguments[0].p,
        PFunctionArgument::Positional(PEntryValue::Integer(54))
    );
    assert_eq!(
        pout.p.arguments[1].p,
        PFunctionArgument::Named { name: "x".to_string(), value: PEntryValue::Integer(13) }
    );
    assert_eq!(
        pout.p.arguments[2].p,
        PFunctionArgument::Named { name: "y".to_string(), value: PEntryValue::Integer(31) }
    );
}


#[test]
fn test_function_cursor_positions() {
    let subj = "get(13, 243)".to_string();
    let res = parse_function(&subj, 0);
    assert!(res.is_ok(), "{:?}", res);
    let pout = res.unwrap();
    assert_eq!(pout.start_loc, 0);
    assert_eq!(pout.end_loc, 11);
}

