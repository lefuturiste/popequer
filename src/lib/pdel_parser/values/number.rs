use crate::pdel_parser::{ParseError, ParseOutput, WHITESPACES};

pub fn parse_integer(subject: &str, initial_cursor: usize) -> Result<ParseOutput<i64>, ParseError>
{
    #[derive(PartialEq)]
    #[derive(Debug)]
    #[derive(Clone)]
    enum State {
        Whitespace,
        InMinus,
        In,
        Final
    }
    let mut state = State::Whitespace;
    let mut target_state = State::InMinus;
    let mut tmp_str = String::new();
    let mut cursor = initial_cursor;
    let mut has_minus_sign = false;
    let mut cursor_after_whitespaces: Option<usize> = None;

    loop {
        let symbol = match subject.chars().nth(cursor) {
            None => {
                if state == State::In && !tmp_str.is_empty() {
                    state = State::Final;
                    cursor -= 1;
                    continue;
                }
                return Err(ParseError {
                    msg: "Unexpected end of subject while parsing Integer".into(),
                    cursor,
                    ..Default::default()
                });
            },
            Some(symbol) => symbol
        };
        match state {
            State::Whitespace => {
                if !WHITESPACES.contains(&symbol) {
                    state = target_state.clone();
                    if cursor_after_whitespaces.is_none() {
                        cursor_after_whitespaces = Some(cursor.clone());
                    }
                    continue;
                }
            },
            State::InMinus => {
                if symbol != '-' {
                    state = State::In;
                    continue;
                }
                has_minus_sign = true;
                target_state = State::In;
                state = State::Whitespace;
            },
            State::In => {
                // sort of tokenize thing
                if tmp_str.is_empty() && symbol == '-' {
                    has_minus_sign = true;
                    cursor += 1;
                    continue;
                }
                if has_minus_sign && symbol == '-' {
                    return Err(ParseError {
                        msg: "Unexpected minus symbol in middle of Integer".into(),
                        cursor,
                        ..Default::default()
                    });
                }
                if !tmp_str.is_empty() && symbol == '_' {
                    cursor += 1;
                    continue;
                }
                if !tmp_str.is_empty() && !symbol.is_ascii_digit() {
                    state = State::Final;
                    cursor -= 1;
                    continue;
                }
                if !symbol.is_ascii_digit() {
                    return Err(ParseError {
                        msg: format!("Unexpected symbol {} in Integer", symbol),
                        cursor,
                        ..Default::default()
                    });
                }
                tmp_str.push(symbol);
            },
            State::Final => {
                if has_minus_sign {
                    tmp_str.insert(0, '-');
                }
                match tmp_str.parse::<i64>() {
                    Ok(res) => {
                        return Ok(ParseOutput {
                            p: res,
                            start_loc: match cursor_after_whitespaces {
                                None => initial_cursor,
                                Some(p) => p
                            },
                            end_loc: cursor
                        });
                    },
                    Err(_) => {
                        return Err(ParseError {
                            msg: "Cannot parse as i32".into(),
                            cursor,
                            ..Default::default()
                        })
                    }
                }
            },
        }
        
        cursor += 1;
    }
}

pub fn parse_float(subject: &str, initial_cursor: usize) -> Result<ParseOutput<f64>, ParseError> {
    #[derive(PartialEq)]
    #[derive(Debug)]
    #[derive(Clone)]
    enum State {
        Whitespace,
        InMinus,
        In,
        Final
    }
    let mut state = State::Whitespace;
    let mut target_state = State::InMinus;
    let mut tmp_str = String::new();
    let mut cursor = initial_cursor;
    let mut in_decimal = false;
    let mut has_minus_sign = false;
    let mut cursor_after_whitespaces: Option<usize> = None;

    loop {
        let symbol = match subject.chars().nth(cursor) {
            None => {
                if state == State::In && !tmp_str.is_empty() {
                    state = State::Final;
                    cursor -= 1;
                    continue;
                }
                return Err(ParseError {
                    msg: "Unexpected end of subject while parsing Float".into(),
                    cursor,
                    ..Default::default()
                });
            },
            Some(symbol) => symbol
        };
        match state {
            State::Whitespace => {
                if !WHITESPACES.contains(&symbol) {
                    state = target_state.clone();
                    if cursor_after_whitespaces.is_none() {
                        cursor_after_whitespaces = Some(cursor.clone());
                    }
                    continue;
                }
            },
            State::InMinus => {
                if symbol != '-' {
                    state = State::In;
                    continue;
                }
                has_minus_sign = true;
                target_state = State::In;
                state = State::Whitespace;
            },
            State::In => {
                // sort of tokenize thing
                if symbol == '-' {
                    return Err(ParseError {
                        msg: "Unexpected minus symbol in middle of Float".into(),
                        cursor,
                        ..Default::default()
                    });
                }
                if symbol == '.' && !in_decimal {
                    in_decimal = true;
                    cursor += 1;
                    tmp_str.push(symbol);
                    continue;
                }
                if symbol == '.' && in_decimal {
                    return Err(ParseError {
                        msg: "Unexpected dot symbol in decimal part of Float".into(),
                        cursor,
                        ..Default::default()
                    });
                }
                if !tmp_str.is_empty() && symbol == '_' {
                    cursor += 1;
                    continue;
                }
                if !tmp_str.is_empty() && !symbol.is_ascii_digit() {
                    state = State::Final;
                    cursor -= 1;
                    continue;
                }
                if !symbol.is_ascii_digit() {
                    return Err(ParseError {
                        msg: format!("Unexpected symbol {} in Float", symbol),
                        cursor,
                        ..Default::default()
                    });
                }
                tmp_str.push(symbol);
            },
            State::Final => {
                if !in_decimal {
                    return Err(ParseError {
                        msg: "Expected a decimal part in Float".into(),
                        cursor,
                        ..Default::default()
                    })
                }
                if has_minus_sign {
                    tmp_str.insert(0, '-');
                }
                match tmp_str.parse::<f64>() {
                    Ok(res) => {
                        return Ok(ParseOutput {
                            p: res,
                            start_loc: match cursor_after_whitespaces {
                                None => initial_cursor,
                                Some(p) => p
                            },
                            end_loc: cursor
                        });
                    },
                    Err(_) => {
                        return Err(ParseError {
                            msg: "Cannot parse as f64".into(),
                            cursor,
                            ..Default::default()
                        })
                    }
                }
            }
        }
        
        cursor += 1;
    }
}
