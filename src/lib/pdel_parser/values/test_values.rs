use std::assert_matches::assert_matches;

use crate::pdel_parser::{ParseError, PEntryValue, UnresolvedReference};
use crate::pdel_parser::values::{handle_choice_errors, parse_entry_value};
use crate::unwrap_enum_variant;

#[test]
fn test_handle_choice_errors() {
    // test different cursor
    let errors: Vec<ParseError> = vec![
        ParseError {
            msg: "".to_string(),
            cursor: 10,
            ..Default::default()
        },
        ParseError {
            msg: "".to_string(),
            cursor: 1,
            ..Default::default()
        },
        ParseError {
            msg: "".to_string(),
            cursor: 9,
            ..Default::default()
        }
    ];
    let output_error = handle_choice_errors("", &errors, &vec![]);
    assert_eq!(output_error.cursor, 10);

    // test homogenus cursor
    let errors: Vec<ParseError> = vec![
        ParseError {
            msg: "".to_string(),
            cursor: 10,
            ..Default::default()
        },
        ParseError {
            msg: "".to_string(),
            cursor: 10,
            ..Default::default()
        },
        ParseError {
            msg: "".to_string(),
            cursor: 10,
            ..Default::default()
        }
    ];
    let output_error = handle_choice_errors("Dummy", &errors, &vec!["Foo", "Bar"]);
    assert_eq!(output_error.cursor, 10);
    assert_eq!(output_error.msg, "Dummy: Expected one of [\"Foo\", \"Bar\"]");
}


#[test]
fn test_parse_entry_value() {
    let subj = "43".to_string();
    let res = parse_entry_value(&subj, 0);
    assert!(res.is_ok(), "{:?}", res);
    let pout = res.unwrap();
    assert_eq!(pout.p, PEntryValue::Integer(43));
    assert_eq!(pout.end_loc,1);

    let subj = "  4242".to_string();
    let res = parse_entry_value(&subj, 1);
    assert!(res.is_ok(), "{:?}", res);
    let pout = res.unwrap();
    assert_eq!(pout.p, PEntryValue::Integer(4242));
    assert_eq!(pout.end_loc, 5);

    let subj = "  49.189".to_string();
    let res = parse_entry_value(&subj, 1);
    assert!(res.is_ok(), "{:?}", res);
    let pout = res.unwrap();
    assert_eq!(pout.p, PEntryValue::Float(49.189));
    assert_eq!(pout.end_loc, 7);

    let subj = "\"foobar\"".to_string();
    let res = parse_entry_value(&subj, 0);
    assert!(res.is_ok(), "{:?}", res);
    let pout = res.unwrap();
    assert_eq!(pout.p, PEntryValue::String("foobar".to_string()));

    let subj = "[Reference to Smth]".to_string();
    let res = parse_entry_value(&subj, 0);
    assert!(res.is_ok(), "{:?}", res);
    let pout = res.unwrap();
    assert_eq!(pout.p, PEntryValue::Reference(
        UnresolvedReference::SoftLocation("Reference to Smth".to_string())
    ));

    let subj = ")(".to_string();
    let res = parse_entry_value(&subj, 0);
    assert!(res.is_err(), "{:?}", res);
    let err = res.unwrap_err();
    assert!(
        err.msg.starts_with("EntryValue: Expected one of"),
        "{}",
        err.msg
    );
}

#[test]
fn test_parse_entry_value_with_embedded_entry() {
    // verify recursive entry definition
    let subj = "@Entry { foo: 10 }".to_string();
    let res = parse_entry_value(&subj, 0);
    assert_matches!(res, Ok(_));
    let pout = res.unwrap();
    assert_matches!(pout.p, PEntryValue::Entry(_));
    let entry = unwrap_enum_variant!(pout.p, PEntryValue::Entry);
    assert_eq!(entry.claims.p.get(0).unwrap().p.property.p, "foo");
}

