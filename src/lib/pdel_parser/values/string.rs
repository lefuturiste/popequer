use crate::pdel_parser::{ParseError, ParseOutput, WHITESPACES, ParseLocationKind};

pub fn parse_string_generalized(pattern_name: &str, subject: &str, initial_cursor: usize, delimiter_start: char, delimiter_end: char) -> Result<ParseOutput<String>, ParseError>
{
    #[derive(PartialEq)]
    enum State {
        InString,
        OutString
    }
    let mut state = State::OutString;
    let mut parsed = String::new();
    let mut cursor = initial_cursor;

    loop {
        let current_symbol = match subject.chars().nth(cursor) {
            None => {
                return Err(ParseError {
                    msg: format!("Unexpected end of subject while parsing {}", pattern_name),
                    cursor,
                    location_kind: match state {
                        State::OutString => ParseLocationKind::Before,
                        _ => ParseLocationKind::Inside
                    },
                    ..Default::default()
                });
            },
            Some(symbol) => symbol
        };
        if state == State::InString {
            if current_symbol == delimiter_end {
                return Ok(ParseOutput {
                    p: parsed,
                    start_loc: initial_cursor,
                    end_loc: cursor
                });
            }
            parsed.push(current_symbol);
        }
        if state == State::OutString {
            if current_symbol == delimiter_start {
                state = State::InString;
                cursor += 1;
                continue;
            }
            if !WHITESPACES.contains(&current_symbol) {
                return Err(ParseError {
                    msg: format!("Unexpected {} before {}", current_symbol, pattern_name),
                    cursor,
                    location_kind: ParseLocationKind::Before,
                    ..Default::default()
                });
            }
        }
       
        cursor += 1;
    }
}

pub fn parse_string(subject: &str, initial_cursor: usize) -> Result<ParseOutput<String>, ParseError>
{
    parse_string_generalized("String", subject, initial_cursor, '"', '"')
}

