use crate::pdel_parser::{ParseError, ParseOutput, WHITESPACES, PEntryValue, PEntryClaim, PEntryValueContainer, look_ahead};
use crate::pdel_parser::collection::parse_entry_qualifiers;
use crate::pdel_parser::values::parse_entry_value;

/// for more flexibility, we separate the claim value and the claim qualifier from the claim property
pub fn parse_entry_value_container(subject: &str, initial_cursor: usize) -> Result<ParseOutput<PEntryValueContainer>, ParseError>
{
    let mut value: Option<ParseOutput<PEntryValue>> = None;
    let mut qualifiers: Vec<ParseOutput<PEntryClaim>> = vec![];

    #[derive(PartialEq)]
    #[derive(Clone)]
    #[derive(Debug)]
    enum State {
        Whitespace,
        InValue,
        InArrow,
        InQualifiers,
        Final
    }
    let mut state = State::Whitespace;
    let mut target_state = State::InValue;
    let mut cursor = initial_cursor;
    let mut consumed_whitespaces = 0;

    loop {
        // println!("entry value contrainer {:?}", state);
        let symbol = match subject.chars().nth(cursor) {
            None => {
                if state == State::Whitespace && target_state == State::InArrow {
                    state = State::Final;
                    cursor -= 1;
                    continue;
                }
                return Err(ParseError {
                    msg: "Unexpected end of string while parsing EntryValueContainer".into(),
                    cursor,
                    ..Default::default()
                });
            },
            Some(symbol) => symbol
        };
        match state {
            State::Whitespace => {
                if !WHITESPACES.contains(&symbol) {
                    state = target_state.clone();
                    continue;
                }
                consumed_whitespaces += 1;
            },
            State::InValue => {
                value = match parse_entry_value(subject, cursor) {
                    Err(err) => return Err(err),
                    Ok(output) => {
                        cursor = output.end_loc;
                        Some(output)
                    }
                };
                target_state = State::InArrow;
                consumed_whitespaces = 0;
                state = State::Whitespace;
            },
            State::InArrow => {
                let look_ahead = look_ahead(subject, cursor, "=>");
                if look_ahead.is_none() {
                    // this value container don't have qualifiers
                    state = State::Final;
                    // don't consume the extra whitespace
                    cursor -= consumed_whitespaces + 1;
                    continue;
                }
                target_state = State::InQualifiers;
                consumed_whitespaces = 0;
                state = State::Whitespace;
                cursor += 2;
                continue;
            },
            State::InQualifiers => {
                qualifiers = match parse_entry_qualifiers(subject, cursor) {
                    Err(err) => return Err(err),
                    Ok(output) => {
                        cursor = output.end_loc;
                        output.p
                    }
                };
                // println!("qualifiers: {:?}", qualifiers);
                target_state = State::Final;
                consumed_whitespaces = 0;
                state = State::Whitespace;
                continue;
            },
            State::Final => {
                return Ok(ParseOutput {
                    p: PEntryValueContainer {
                        value: value.unwrap(),
                        qualifiers
                    },
                    start_loc: initial_cursor,
                    end_loc: cursor
                })
            }
        }
       
        cursor += 1;
    }
}
