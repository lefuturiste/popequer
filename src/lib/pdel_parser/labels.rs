use crate::pdel_parser::{PEntry, PEntryValue, get_string_entry_value, get_string_list_from_entry_value, PEntryValueContainer, UnresolvedReference};
use crate::database::labels::LanguageLabels;

/// get a String from a EntryValueContainer that is a Reference, will extract the string to locate
/// the target
fn get_reference_string(value_container: &PEntryValueContainer) -> String {
    let ur = match &value_container.value.p {
        PEntryValue::Reference(ur) => ur,
        _ => panic!("Expected a EntryValue::Reference")
    };
    (match ur {
        UnresolvedReference::SoftLocation(sl) => sl,
        UnresolvedReference::HardLocation(hl) => hl
    }).to_string()
}

// FIXME: this will be later using the "PostEntry" the entry adapted for the database
fn get_language_label_component<'a>(component_slug: &str, entry: &'a PEntry, target_lang_code: &str) -> Option<&'a PEntryValue> {
    let component_claim = entry.claims.p.iter().find(|claim|
        claim.p.property.p == component_slug &&
        match claim.p.value_container.p.qualifiers.iter()
            // FIXME: if there is no qualifier, it's english, or may be we need to add a qualifier
            // at the post-process level, when we will construct the simple version from the parsed
            // version, for now we can just hack it
            .find(|qualifier| qualifier.p.property.p == "language") {
                None => true,
                // here we need to find the right language with the reference string
                Some(qualifier) => get_reference_string(&qualifier.p.value_container.p) == target_lang_code
            }
    )?;
    Some(&component_claim.p.value_container.p.value.p)
}

pub fn get_language_labels(entry: &PEntry, target_lang_code: &str) -> LanguageLabels {
    LanguageLabels {
        name: get_language_label_component("name", entry, target_lang_code).and_then(|x| get_string_entry_value(&x)),
        description: get_language_label_component("description", entry, target_lang_code).and_then(|x| get_string_entry_value(&x)),
        aliases: get_language_label_component("aliases", entry, target_lang_code)
            .and_then(|x| get_string_list_from_entry_value(&x)).unwrap_or(vec![]),
    }
}

/// Recursive function
/// will get a printable String from an EntryValue (cleaning the nested ParseOutput)
pub fn entry_value_to_str(entry_value: PEntryValue) -> Option<String> {
    match entry_value {
        PEntryValue::List(list) => {
            let mut out = String::new();
            for e in list {
                out.push_str(&format!(
                  "{}, ", entry_value_to_str(e.p.value.p)?.to_string()
                ))
            }
            Some(out)
        },
        PEntryValue::String(str) => Some(str),
        PEntryValue::Float(val)  => Some(format!("{}", val)),
        PEntryValue::Integer(val)  => Some(format!("{}", val)),
        // EntryValue::Reference(val)  => Some(format!("{:?}", val)),
        // EntryValue::Function(val)  => Some(format!("{:?}", val)),
        // EntryValue::Dict(val)  => Some(format!("{:?}", val)),
        val => Some(format!("{:?}", val)),
    }
}

