use crate::pdel_parser::markdown::*;

#[test]
fn test_empty_content() {
    let subj = "";
    let res = parse_markdown(subj, 0);
    assert!(res.is_ok(), "{:?}", res);
    let pout = res.unwrap().tree;
    assert_eq!(pout.p.len(), 0); 
}

#[test]
fn test_parse_multiple_entries() {
    // multiple empty entries
    let subj = "@Entry {} {}\n@Entry {} {}\n";
    let res = parse_markdown(subj, 0);
    assert!(res.is_ok(), "{:?}", res);
    let pout = res.unwrap().tree;
    assert_eq!(pout.p.len(), 2); 
    
    // multiple empty entries in a document
    let subj = "# Hello world\n @Entry {\n} {\n}\n\n@Entry { } { }";
    let res = parse_markdown(subj, 0);
    assert!(res.is_ok(), "{:?}", res);
    let pout = res.unwrap().tree;
    assert_eq!(pout.p.len(), 2); 
    
    // multiple empty entries in a document
    let subj = r#"
        # Hello world

        this is a paragraph

        This is an @Entry:
        @Entry {
            foo: "bar"
        }
        This is another @Entry:
        ```
        @Entry {
            bar: "foo"
        }
        ```

        This is some code
        ```php
        hello
        ```
    "#;
    let res = parse_markdown(subj, 0);
    assert!(res.is_ok(), "{:?}", res);
    let pout = res.unwrap().tree;
    assert_eq!(pout.p.len(), 2); 
    let entry = pout.p.get(0).unwrap();
    assert_eq!(
        entry.p.claims.p.get(0).unwrap().p.property.p,
        "foo"
    );

    let entry = pout.p.get(1).unwrap();
    assert_eq!(
        entry.p.claims.p.get(0).unwrap().p.property.p,
        "bar"
    );
}

#[test]
fn test_parse_multiple_entries_unicode_edgecases() {
    let subj = r#"
        # Ma super note de fou
        
        ## Premier paragraphe

        @Entry {
            name: "River"
        }

é
        filling text between two entries

        @Entry {
            is: [River]
            name: "Rivière Romaine"
        }
    "#;
    let res = parse_markdown(subj, 0);
    assert!(res.is_ok(), "{:?}", res);
    assert_eq!(res.unwrap().tree.p.len(), 2);
}

#[test]
fn test_parse_multiple_next_by_next() {
    let subj = r#"
        @Entry {
            name: "Entry A"
        }
        @Entry {
            name: "Entry B"
        }
        @Entry {
            name: "Entry C"
        }
        @Entry {
            name: "Entry D"
        }
        "#;
    let res = parse_markdown(subj, 0);
    assert!(res.is_ok(), "{:?}", res);
    assert_eq!(res.unwrap().tree.p.len(), 4);
}

#[test]
fn test_parse_multiple_with_custom_entry_intro() {
    let subj = r#"
        @Entry {
            name: "Human"
        }
        @Human {}
        @Human {}
        @Human {}
        "#;
    let res = parse_markdown(subj, 0);
    assert!(res.is_ok(), "{:?}", res);
    assert_eq!(res.unwrap().tree.p.len(), 4);
}
