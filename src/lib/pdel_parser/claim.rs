use crate::pdel_parser::{ParseError, ParseOutput, WHITESPACES, PEntryClaim, PEntryValueContainer, };
use crate::pdel_parser::values::container::parse_entry_value_container;

/// Accept pattern: `KEY:`
pub fn parse_key(subject: &str, initial_cursor: usize) -> Result<ParseOutput<String>, ParseError>
{
    let mut cursor = initial_cursor;
    let mut key = String::new();
    loop {
        let symbol = match subject.chars().nth(cursor) {
            Some(symbol) => symbol,
            None => {
                return Err(ParseError {
                    msg: "Unexpected end of subject while parsing key".into(),
                    cursor,
                    ..Default::default()
                });
            }
        };
        
        // TODO: disallow space inside identifier
        if WHITESPACES.contains(&symbol) {
            cursor += 1;
            continue;
        }
        if symbol == ':' {
            return Ok(ParseOutput {
                p: key,
                start_loc: initial_cursor,
                end_loc: cursor + 1
            });
        }
        if !symbol.is_alphanumeric() && symbol != '_' {
            return Err(ParseError {
                msg: format!("Unexpected symbol {:?} while parsing identifier", symbol),
                cursor,
                ..Default::default()
            })
        }
        key.push(symbol);
        cursor += 1;
    }
}

pub fn parse_entry_claim(subject: &str, initial_cursor: usize) -> Result<ParseOutput<PEntryClaim>, ParseError>
{
    let mut cursor = initial_cursor;
    let property: ParseOutput<String> =
        match parse_key(subject, cursor) {
            Err(err) => return Err(err),
            Ok(pout) => {
                cursor = pout.end_loc;
                pout
            }
        };

    let value_container: ParseOutput<PEntryValueContainer> =
        match parse_entry_value_container(subject, cursor) {
            Err(err) => return Err(err),
            Ok(pout) => {
                cursor = pout.end_loc;
                pout
            }
        };

    Ok(ParseOutput {
        p: PEntryClaim {
            property,
            value_container
        },
        start_loc: initial_cursor,
        end_loc: cursor
    })
}
