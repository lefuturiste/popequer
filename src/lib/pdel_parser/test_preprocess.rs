use super::preprocess::{apply_expansions, preprocess_code, Expansion};
use indoc::indoc;

#[test]
fn test_preprocess_remove_comments() {
    let subj: String = indoc!{r#"
        aa bb
        // to remove
        aa bb
    "#}.to_string();
    let res = preprocess_code(&subj);
    assert!(res.is_ok(), "{:?}", res);
    let pout = res.unwrap();
    assert_eq!(pout.code_output, indoc!{r#"
        aa bb
        aa bb
    "#}.to_string());

    let subj: String = indoc!{r#"
        // to remove
        aa bb
        aa bb
    "#}.to_string();
    let res = preprocess_code(&subj);
    assert!(res.is_ok(), "{:?}", res);
    let pout = res.unwrap();
    assert_eq!(pout.code_output, indoc!{r#"
        aa bb
        aa bb
    "#}.to_string());
}

#[test]
fn test_preprocess_remove_comments_inline() {
    let subj: String = indoc!{r#"
        some code // a comment to remove
        some other code
    "#}.to_string();
    let res = preprocess_code(&subj);
    assert!(res.is_ok(), "{:?}", res);
    let pout = res.unwrap();

    assert_eq!(pout.code_output, indoc!{r#"
        some code
        some other code
    "#}.to_string());
}

#[test]
fn test_preprocess_remove_comments_inline_with_quotes() {
    let subj: String = indoc!{r#"
        let code = "some // string with slashs" // the real comment
    "#}.to_string();
    let res = preprocess_code(&subj);
    assert!(res.is_ok(), "{:?}", res);
    let pout = res.unwrap();

    assert_eq!(pout.code_output, indoc!{r#"
        let code = "some // string with slashs"
    "#}.to_string());
}

#[test]
fn test_preprocess_remove_comments_following() {
    // two comments next to each others
    let subj: String = indoc!{r#"
        // some comment
        // some other comment
        some code
    "#}.to_string();
    let res = preprocess_code(&subj);
    assert!(res.is_ok(), "{:?}", res);
    let pout = res.unwrap();

    assert_eq!(pout.code_output, indoc!{r#"
        some code
    "#}.to_string());
}

#[test]
fn test_preprocess_remove_comments_combined() {
    let subj: String = indoc!{r#"
        // a comment to remove A
        some code // a comment to remove B
        some other code
        // a comment to remove C
        // wow
        // hello
    "#}.to_string();
    let res = preprocess_code(&subj);
    assert!(res.is_ok(), "{:?}", res);
    let pout = res.unwrap();

    assert_eq!(pout.code_output, indoc!{r#"
        some code
        some other code
    "#}.to_string());
}

#[test]
fn test_preprocess_remove_comments_immediate() {
    let subj: String = r#"// Hé hé oh è"#.to_string();
    let res = preprocess_code(&subj);
    assert!(res.is_ok(), "{:?}", res);
    let pout = res.unwrap();
    assert_eq!(pout.code_output.trim(), "");
}


#[test]
fn test_preprocess_expand_shortcut() {
    let subj: String = r#"
    !empty()
    "#.to_string();
    let res = preprocess_code(&subj);
    assert!(res.is_ok(), "{:?}", res);
    let pout = res.unwrap();
    assert!(pout.code_output.trim() == "");
}

#[test]
fn test_preprocess_shortcut_do_not_mistake() {
    // ne pas confondre un shortcut avec juste un point d'exclamation
    let subj: String = r#"
        wow c'est vraiment incroyable!
    "#.to_string();
    let res = preprocess_code(&subj);
    assert!(res.is_ok(), "{:?}", res);
    assert_eq!(res.unwrap().code_output.trim(), "wow c'est vraiment incroyable!");

    let subj: String = r#"
        !vraimentchelou
    "#.to_string();
    let res = preprocess_code(&subj);
    assert!(res.is_ok(), "{:?}", res);
    assert_eq!(res.unwrap().code_output.trim(), "!vraimentchelou");
}


#[test]
fn test_preprocess_expand_lorem() {
    let subj: String = r#"
    !lorem()
    "#.to_string();
    let res = preprocess_code(&subj);
    assert!(res.is_ok(), "{:?}", res);
    let pout = res.unwrap();
    assert!(pout.code_output.trim().starts_with("Lorem ipsum"));
}

#[test]
fn test_preprocess_expand_labels_shortcut_single_language() {
    let subj: String = r#"
    !labels(("The English Title", "The english description", ("Alias 1", "Alias 2")))
    "#.to_string();
    let res = preprocess_code(&subj);
    assert!(res.is_ok(), "{:?}", res);
    let pout = res.unwrap();
    let expected_out = vec![
        r#"name: "The English Title" => { language: [en] },"#,
        r#"description: "The english description" => { language: [en] },"#,
        r#"alias: ("Alias 1", "Alias 2") => { language: [en] },"#,
    ];
    crate::utils::assert_line_by_line(&pout.code_output, expected_out);
}

#[test]
fn test_preprocess_expand_labels_shortcut_multiple_languages() {
    let subj: String = r#"
    !labels({
        fr: ("title A", "description fr", ("alias1", "alias2")),
        en: ("title B", "description en", ("another alias1", "another alias2"))
    })
    "#.to_string();
    let res = preprocess_code(&subj);
    assert!(res.is_ok(), "{:?}", res);
    let pout = res.unwrap();
    let expected_out = vec![
        r#"name: "title A" => { language: [fr] },"#,
        r#"description: "description fr" => { language: [fr] },"#,
        r#"alias: ("alias1", "alias2") => { language: [fr] },"#,
        r#"name: "title B" => { language: [en] },"#,
        r#"description: "description en" => { language: [en] },"#,
        r#"alias: ("another alias1", "another alias2") => { language: [en] },"#
    ];
    crate::utils::assert_line_by_line(&pout.code_output, expected_out);
}

#[test]
fn test_preprocess_expand_inline_shortcut() {
    let subj: String = r#"
    hello: !42();
    "#.to_string();
    let res = preprocess_code(&subj);
    assert!(res.is_ok(), "{:?}", res);
    let pout = res.unwrap();
    assert_eq!(pout.code_output.trim(), "hello: \"fourty-two\";");
}

#[test]
fn test_preprocess_shortcut_offsets() {
    let subj: String = r#"!empty()"#.to_string();
    let res = preprocess_code(&subj);
    let offsets = res.unwrap().offsets;
    assert_eq!(offsets.get(0).unwrap().input_range, (0, 8));
    assert_eq!(offsets.get(0).unwrap().output_range, (0, 0));
    assert_eq!(offsets.get(0).unwrap().offset(), -8);

    let subj: String = r#"some things !empty() hey hey"#.to_string();
    let res = preprocess_code(&subj);
    let offsets = res.unwrap().offsets;
    assert_eq!(offsets.get(0).unwrap().input_range, (12, 20));
    assert_eq!(offsets.get(0).unwrap().output_range, (12, 12));

    let subj: String = r#"!fill()"#.to_string();
    let res = preprocess_code(&subj);
    let offsets = res.unwrap().offsets;
    assert_eq!(offsets.iter().nth(0).unwrap().offset(), 36);
}


#[test]
fn test_preprocess_comments_offsets() {
    let subj: String = indoc! {r#"
        Some code 2
        // something to be ignored
        Some code 1
    "#}.to_string();

    let res = preprocess_code(&subj);
    let value = res.unwrap();

    assert_eq!(value.offsets.get(0).unwrap().input_range, (12, 39));
    // 27 chars are deleted
    assert_eq!(value.offsets.get(0).unwrap().output_range, (12, 12));
}

#[test]
fn test_preprocess_comments_offsets_inline() {
    let subj: String = indoc! {r#"
        Something code 2 // something to be ignored
        Some code 1
    "#}.to_string();

    let res = preprocess_code(&subj);
    let value = res.unwrap();

    assert_eq!(value.offsets.get(0).unwrap().input_range, (16, 42));
    assert_eq!(value.offsets.get(0).unwrap().output_range, (16, 16));
}

#[test]
fn test_compute_expansion_offset() {
    // xxXX
    // xx
    assert_eq!((Expansion { input_range: (2, 4), output_range: (1, 1) }).offset(), -2);
    assert_eq!((Expansion { input_range: (2, 4), output_range: (2, 6) }).offset(), 2);
}

#[test]
fn test_apply_expansions() {
    /*
    shrink case
    original:       bonjour *** c'est incroyable.
                                      ^    
    preprocessed:   bonjour * c'est incroyable.
                                    ^    
    (8, 11) reduced to (8, 9)
    so cursor 16 must be 18
    */
    assert_eq!(
        apply_expansions(&vec![
            Expansion { input_range: (8, 11), output_range: (8, 9) }
        ], 16),
        18
    );

    /*
    expansion case
    original:       bonjour !pi c'est incroyable.
                                      ^    
    preprocessed:   bonjour 3.14159 c'est incroyable.
                                          ^    
    (8, 11) maps to (8, 15)
    so cursor 18 must be 22
    */
    assert_eq!(
        apply_expansions(&vec![
            Expansion { input_range: (8, 11), output_range: (8, 15) }
        ], 22),
        18
    );

    /*
    expansion case
    original:       *****c'est incroyable.
                         ^ 
    preprocessed:   c'est incroyable.
                    ^  
    */
    assert_eq!(
        apply_expansions(&vec![
            Expansion { input_range: (0, 5), output_range: (0, 0) }
        ], 0),
        5
    );
}
