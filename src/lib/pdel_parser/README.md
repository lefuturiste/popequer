# Popequer Data Entry Language

This module is responsible to parse and create a pseudo syntax tree.

## Macros or shortcuts

macros or shortcuts are used to cut off repetitive work when declaring
they are declared with `!` exclamation mark then the name of the macro plus functions arguments

- `!parent()`, will include a link to the parent entry
- `!labesl()` is a shortcut to set the labels, instead of doing 

    name: "the name in english" => {
        language: [English]
    }
    name: "le nom en français" => {
        language: [Français]
    }
