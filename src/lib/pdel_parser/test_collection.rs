use crate::pdel_parser::collection::*;
use crate::pdel_parser::PEntryValue;
use crate::pdel_parser::parse_wrapper;

#[test]
fn test_parse_list() {
    // empty list
    let subj = "()".to_string();
    let res = parse_wrapper(parse_list, &subj);
    assert!(res.is_ok(), "{:?}", res);
    let pout = res.unwrap();
    assert_eq!(pout.p.len(), 0);
    assert_eq!(pout.end_loc, 1);

    // single element list
    let subj = "( 3243 )".to_string();
    let res = parse_wrapper(parse_list, &subj);
    assert!(res.is_ok(), "{:?}", res);
    assert_eq!(res.unwrap().p.len(), 1);

    let subj = "( 32, 12, 43 )".to_string();
    let res = parse_wrapper(parse_list, &subj);
    assert!(res.is_ok(), "{:?}", res);
    let pout = res.unwrap();
    assert_eq!(pout.p.len(), 3);
    assert_eq!(pout.p[0].p.value.p, PEntryValue::Integer(32));
    assert_eq!(pout.p[1].p.value.p, PEntryValue::Integer(12));
    assert_eq!(pout.p[2].p.value.p, PEntryValue::Integer(43));

    let subj = "( \"hello world\", \"English science fiction writer and humourist 1952–2001\", 43 )".to_string();
    let res = parse_wrapper(parse_list, &subj);
    assert!(res.is_ok(), "{:?}", res);
    let pout = res.unwrap();
    assert_eq!(pout.p.len(), 3);
    assert_eq!(pout.p[0].p.value.p, PEntryValue::String("hello world".to_string()));
    assert_eq!(pout.p[1].p.value.p, PEntryValue::String("English science fiction writer and humourist 1952–2001".to_string()));
    assert_eq!(pout.p[2].p.value.p, PEntryValue::Integer(43));

    // test trailing comma
    let subj = "( 0, 0, 110,  )".to_string();
    let res = parse_wrapper(parse_list, &subj);
    assert!(res.is_ok(), "{:?}", res);
    let pout = res.unwrap();
    assert_eq!(pout.p.len(), 3);
    assert_eq!(pout.p[2].p.value.p, PEntryValue::Integer(110));

    // test trailing comma
    let subj = "( 0, 0, 110,  )".to_string();
    let res = parse_wrapper(parse_list, &subj);
    assert!(res.is_ok(), "{:?}", res);
    let pout = res.unwrap();
    assert_eq!(pout.p.len(), 3);
    assert_eq!(pout.p[2].p.value.p, PEntryValue::Integer(110));
}

#[test]
fn test_parse_list_with_qualifiers() {
    // test trailing comma
    let subj = r#"(
        0 => {},
        0 => {},
        110 => {},
        110 => {
            start_date: "adslasd",
            foo: 32
            wow: 10009501
        },
        10 => {
        }
    )"#.to_string();
    let res = parse_wrapper(parse_list, &subj);
    assert!(res.is_ok(), "{:?}", res);
    let pout = res.unwrap();
    assert_eq!(pout.p.len(), 5);
    assert_eq!(pout.p[2].p.value.p, PEntryValue::Integer(110));
    assert_eq!(pout.p[2].p.qualifiers.len(), 0);
}


#[test]
fn test_parse_entry_claims_with_correct_parsed_positions() {
    let subj = "   { foo: 43, hey: 12 }    ".to_string();
    let res = parse_wrapper(parse_entry_claims, &subj);
    assert!(res.is_ok(), "{:?}", res);
    let pout = res.unwrap();
    assert_eq!(pout.start_loc, 3);
    assert_eq!(pout.end_loc, 22);
}


#[test]
fn test_parse_entry_claims_collection() {
    // empty collection
    let subj = "{}".to_string();

    let res = parse_wrapper(parse_entry_claims, &subj);
    assert!(res.is_ok(), "{:?}", res);
    let pout = res.unwrap();
    assert_eq!(pout.p.len(), 0);
    assert_eq!(pout.end_loc, 1);

    let subj = "{ foo: 43 hey: 12 }".to_string();
    let res = parse_wrapper(parse_entry_claims, &subj);
    assert!(res.is_ok(), "{:?}", res);
    let pout = res.unwrap();
    assert_eq!(pout.p[0].p.property.p, "foo");
    assert_eq!(pout.p[1].p.property.p, "hey");

    let subj = "{ str: \"some\" hey: 12 }".to_string();
    let res = parse_wrapper(parse_entry_claims, &subj);
    assert!(res.is_ok(), "{:?}", res);
    let pout = res.unwrap();
    assert_eq!(pout.p[0].p.value_container.p.value.p, PEntryValue::String("some".to_string()));
    assert_eq!(pout.p[1].p.value_container.p.value.p, PEntryValue::Integer(12));
}

#[test]
fn test_parse_entry_claims_collection_with_qualifiers() {
    let subj = r#"{
        is: "Some instance",
        birth_date: "Some date" => {
            correctness: 42,
            foo: "bar"
        }
    }"#.to_string();
    let res = parse_wrapper(parse_entry_claims, &subj);
    assert!(res.is_ok(), "{:?}", res);
    let pout = res.unwrap();
    assert_eq!(pout.p[0].p.property.p, "is");
    assert_eq!(pout.p[1].p.property.p, "birth_date");
    assert_eq!(pout.p[1].p.value_container.p.value.p, PEntryValue::String("Some date".to_string()));
    assert_eq!(pout.p[1].p.value_container.p.qualifiers[0].p.property.p, "correctness");
    assert_eq!(
        pout.p[1].p.value_container.p.qualifiers[0].p.value_container.p.value.p,
        PEntryValue::Integer(42)
    );
}

#[test]
fn test_parse_entry_claims_collection_with_groups_and_qualifiers() {
    let subj = r#"{
        prop1: (
            "value1",
            "value2",
            "value3" => {
                foo: "bar"
            }
        )
    }"#.to_string();
    let res = parse_wrapper(parse_entry_claims, &subj);
    assert!(res.is_ok(), "{:?}", res);
    let pout = res.unwrap();
    println!("{:#?}", pout);
}

