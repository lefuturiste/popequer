use std::ops::Range;
use crate::utils::Substring;

use super::{
    extract_current_line, look_ahead,
    get_list_entry_value, get_string_from_entry_value_container, get_string_list_from_entry_value_container,
    PEntryValue, PEntryValueContainer, PFunction, PFunctionArgument, ParseError, ParseLocationKind, ParseOutput,
    values::function::parse_function
};
use crate::{database::labels::LanguageLabels, debug_state_machine};

use anyhow::Result;
use fully_pub::fully_pub;
use serde::{Serialize, Deserialize};

const DEFAULT_LANGUAGE: &str = "en";

#[fully_pub]
#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
struct Expansion {
    input_range: (usize, usize),
    output_range: (usize, usize),
}

impl Expansion {
    /// compute relative offset from mapping ranges
    pub fn offset(&self) -> i32 {
        // positive offset means we have added things, negative means mean we have less
        (self.output_range.1 as i32 - self.output_range.0 as i32) - (self.input_range.1 as i32 - self.input_range.0 as i32)
    }
}


/// will recompute the real position in the original buffer taking in account the shrinking or
/// expansions of some parts of the buffer
pub fn apply_expansions(mappings: &Vec<Expansion>, cursor: usize) -> usize {
    /*
    PreProcessed:   Bonjour c'est incroyable.
                                  ^    
    Original:       Bonjour *** c'est incroyable.
                                      ^    
    ===
    expansion case

    PreProcessed:   Bonjour 3.14159 c'est incroyable.
                                          ^    
    Original:       Bonjour !pi c'est incroyable.
                                      ^    

    So the parser identified something in the pre processed code at pos x
    We must compute corrected position y that can be used on the original non-pre-processed string
    */

    let mut cursor: i32 = cursor as i32;
    for exp in mappings {
        // ignore if expansion start after
        if exp.output_range.0 as i32 > cursor {
            continue;
        }
        cursor -= exp.offset();
    }
    if cursor < 0 {
        panic!("Failed to apply expansion: final cursor is negative!");
    }
    cursor as usize
}

pub trait AdaptableRange {
    fn apply_expansions(&self, expansions: &Vec<Expansion>) -> Self;
}

impl AdaptableRange for Range<usize> {
    fn apply_expansions(&self, expansions: &Vec<Expansion>) -> Self {
        (apply_expansions(&expansions, self.start))..(apply_expansions(&expansions, self.end))
    }
}

#[fully_pub]
#[derive(Debug)]
struct PreprocessOutput {
    code_output: String,
    offsets: Vec<Expansion>
}

pub fn preprocess_code(subject: &str) -> Result<PreprocessOutput, ParseError> {
    #[derive(PartialEq)]
    #[derive(Debug)]
    enum State {
        Normal,
        PreWhitespace, // state after a new line
    }
    let mut state = State::PreWhitespace;

    let mut code_output = String::new();
    let mut cursor = 0;
    let mut expansions: Vec<Expansion> = vec![];

    // hacky workaround quotes
    let mut quote_count = 0;

    loop {
        let symbol = match subject.chars().nth(cursor) {
            None => {
                // end of the preprocessing
                return Ok(PreprocessOutput {
                    code_output,
                    offsets: expansions
                }); 
            },
            Some(x) => x
        };

        // debug_state_machine!("preprocess_code", subject, state, cursor);
        match state {
            State::PreWhitespace => {
                // we are at the start of a newline
                let (line_end_pos, line) = extract_current_line(subject, cursor);
                if line.trim().starts_with("//") {
                    // we shrink the whole line
                    expansions.push(Expansion {
                        input_range: (cursor, line_end_pos+1),
                        output_range: (cursor, cursor)
                    });
                    state = State::PreWhitespace;
                    cursor = line_end_pos+1;
                    continue;
                }
                state = State::Normal;
                continue;
            },

            State::Normal => {
                // detect shortcut via the hint of the exclamation mark
                if symbol == '"' {
                    quote_count += 1;
                }
                // look for inline comments (space is important, we also check for quote count)
                if look_ahead(subject, cursor, " //").is_some() && quote_count % 2 == 0 {
                    let (line_end_pos, _line) = extract_current_line(subject, cursor);
                    expansions.push(Expansion {
                        input_range: (cursor, line_end_pos-1),
                        output_range: (cursor, cursor)
                    });
                    state = State::PreWhitespace;
                    cursor = line_end_pos;
                    continue;
                }
                // FIXME: only go ahead if we detect the pattern of a ! followed by a lower case word and a open brace
                if look_ahead(subject, cursor, "!").is_some() {
                    // shortcut is an alias for macro, we use shortcut because it's not a reserved keyword
                    // try to expand the macro
                    // get the macro type
                    match parse_function(subject, cursor+1) {
                        Ok(pout) => {
                            state = State::Normal;
                            cursor = pout.end_loc + 1;

                            let (expanded_code, offset) = expand_parsed_function_as_shortcut(pout)
                                .map_err(|(parse_err, _)| parse_err)?;
                            // account for the !
                            let new_offset = Expansion {
                                input_range: (offset.input_range.0-1, offset.input_range.1+1),
                                output_range: (offset.output_range.0-1, offset.output_range.1-1)
                            };
                            expansions.push(new_offset);
                            code_output.push_str(&expanded_code);
                            continue;
                        },
                        Err(err) => {
                            if err.location_kind == ParseLocationKind::Before || err.location_kind == ParseLocationKind::Header {
                                // skip if the function pattern is not detected
                                state = State::Normal;
                                // still add the missing content
                                code_output.push_str(&subject.to_string().sub(cursor..(err.cursor)).expect("To get back the content that have been skipped."));
                                cursor = err.cursor;
                                continue;
                            }
                            return Err(ParseError {
                                cursor: err.cursor,
                                msg: "Could not parse shortcut!".to_string(),
                                location_kind: ParseLocationKind::Inside,
                                ..Default::default()
                            });
                        }
                    }
                }
                code_output.push(symbol);
                if symbol == '\n' {
                    cursor += 1;
                    state = State::PreWhitespace;
                    continue;
                }
            }
        }
        cursor += 1;
    }
}

fn string_list_to_pdel_list(subject: Vec<String>) -> String {
    format!("({})", subject.iter().map(|s| format!("{:?}", s)).collect::<Vec<String>>().join(", "))
}

enum ExpansionError {
    UnknownShortcutName,
    InvalidArgumentCount,
    InvalidArgumentType,
}

/// Expand shortcut (or macro)
fn expand_parsed_function_as_shortcut(parsed_fn_wrapped: ParseOutput<PFunction>) -> Result<(String, Expansion), (ParseError, ExpansionError)> {
    let parsed_fn = &parsed_fn_wrapped.p;
    let output: String = match parsed_fn.name.p.as_str() {
        "lorem" => {
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum".to_string()
        },
        "empty" => {
            "".to_string()
        },
        "fill" => {
            "The quick brown fox jumps over the lazy dog".to_string()
        },
        "42" => {
            "\"fourty-two\"".to_string()
        },
        "labels" => {
            // the labels macro is used to define the labels (name, description, aliases) of a
            // single language OR multiple languages at the same time
            // it will expand to the claims of the labels
            // to be clear, this macro is to remove me the need to implements labels in a separate
            // space (which was the case before)
            // but we will need to re-translate to a separate thing when uploading to wikidata, so
            // it will be inefficient
            let labels_arg = &parsed_fn.arguments.iter()
                .nth(0)
                .ok_or((
                    ParseError::from_parsed_output("Expected at least one argument", &parsed_fn_wrapped),
                    ExpansionError::InvalidArgumentCount
                ))?;
            let labels = match &labels_arg.p {
                PFunctionArgument::Named { value, ..} => value,
                PFunctionArgument::Positional (value) => value
            };
            let mut out_code = String::new();

            fn output_language(
                target_language_code: &str,
                out_code: &mut String,
                parsed_components: &Vec<ParseOutput<PEntryValueContainer>> 
            ) -> Result<(), (ParseError, ExpansionError)> {
                assert!(parsed_components.len() <= 3);
                let out_language_labels = LanguageLabels {
                    name: 
                        match parsed_components.iter().nth(0) {
                            None => None,
                            Some(parsed_name) => Some(
                                get_string_from_entry_value_container(&parsed_name.p).ok_or((
                                    ParseError::from_parsed_output(
                                        "Expected a String as a name in language labels",
                                        parsed_name,
                                    ),
                                    ExpansionError::UnknownShortcutName
                                ))?
                            )
                        },
                    description: 
                        match parsed_components.iter().nth(1) {
                            None => None,
                            Some(parsed_description) => Some(
                                get_string_from_entry_value_container(&parsed_description.p).ok_or((
                                    ParseError::from_parsed_output(
                                        "Expected a String as a description in language labels",
                                        parsed_description,
                                    ),
                                    ExpansionError::UnknownShortcutName
                                ))?
                            )
                        },
                    aliases: 
                        parsed_components.iter()
                            .nth(2)
                            .and_then(|x| get_string_list_from_entry_value_container(&x.p))
                            .unwrap_or(vec![])
                };
                if let Some(name) = out_language_labels.name {
                    out_code.push_str(format!(
                        "name: {:?} => {{ language: [{}] }},\n",
                        name, target_language_code
                    ).as_str());
                }
                if let Some(description) = out_language_labels.description {
                    out_code.push_str(format!(
                        "description: {:?} => {{ language: [{}] }},\n",
                        description, target_language_code
                    ).as_str());
                }
                if out_language_labels.aliases.len() > 0 {
                    out_code.push_str(format!(
                        "alias: {} => {{ language: [{}] }},\n",
                        string_list_to_pdel_list(out_language_labels.aliases), target_language_code
                    ).as_str());
                }
                out_code.push_str("\n");
                Ok(())
            }
           

            // if the argument is a dict, we define multiples labels, if it's a list only the
            // default language
            match labels {
                PEntryValue::Dict(claims) => {
                    for claim in claims {
                        let target_language_code = &claim.p.property.p;
                        let list_language_components = get_list_entry_value(&claim.p.value_container.p.value.p)
                            .ok_or((
                                ParseError::from_parsed_output(
                                    "expected to get a list as value of a language definition",
                                    &claim,
                                ),
                                ExpansionError::UnknownShortcutName
                            ))?;
                        output_language(&target_language_code, &mut out_code, list_language_components)?;
                    }
                },
                PEntryValue::List(list) => {
                    output_language(DEFAULT_LANGUAGE, &mut out_code, list)?;
                }
                _ => return Err((
                    ParseError::from_parsed_output("Expected to get a dict as first argument", &labels_arg),
                    ExpansionError::InvalidArgumentType
                ))
            };

            out_code
        },
        _ => {
            return Err((
                ParseError::from_parsed_output("Unknown shortcut name", &parsed_fn_wrapped.p.name),
                ExpansionError::UnknownShortcutName
            ));
        }
    };
    let output_len = output.len();

    let input_range = (parsed_fn_wrapped.start_loc, parsed_fn_wrapped.end_loc);
    let output_range = (parsed_fn_wrapped.start_loc, parsed_fn_wrapped.start_loc + output_len);
    Ok((output, Expansion {
        input_range,
        output_range,
    }))
}
