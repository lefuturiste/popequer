use fully_pub::fully_pub;

use crate::pdel_parser::{parse_entry, PEntry, ParseError, ParseLocationKind, ParseOutput, ENTRY_INTRO, WHITESPACES};
use crate::{debug_state_machine, get_symbol_or_final};

use crate::utils::Substring;

#[derive(Debug)]
#[fully_pub]
/// a list of parsed snippets along the expansions
struct ParsedFileCode {
    tree: ParseOutput<Vec<ParseOutput<PEntry>>>,
}


/// Take a mardown-like document and extract PDEL snippets from the content
/// A PDEL snippet SHOULD be separated from the markdown by at least one new line.
/// A PDEL snippet MUST be announced by the ENTRY_INTRO substring and only preceeded by WHITESPACES
///     on a newline.
/// This assume that the input was already preprocessed
pub fn parse_markdown(initial_subject: &str, initial_cursor: usize) -> Result<ParsedFileCode, ParseError>
{
    // TODO: Skip erronous entries but still report the parse err along in the ParsedFileCode
    // struct

    // I'm thinking we should rethink completely the parser to return for each component the start
    // position and end position in the text (like a tokenizer but more advanced)
    let mut snippets: Vec<ParseOutput<PEntry>> = vec![];

    let subject = initial_subject;

    #[derive(Debug, PartialEq)]
    enum State {
        Markdown,
        InEntry,
        Final
    }
    let mut cursor = initial_cursor;
    let mut state = State::InEntry;
    loop {
        // debug_state_machine!("parse_markdown", subject, state, cursor);
        match state {
            State::Markdown => {
                let symbol = get_symbol_or_final!(subject, state, cursor);
                if symbol == '\n' {
                    // optimistic parsing
                    state = State::InEntry;
                }
            },
            State::InEntry => {
                match parse_entry(&subject, cursor) {
                    Ok(pout) => {
                        state = State::Markdown;
                        cursor = pout.end_loc + 1;
                        snippets.push(pout);
                        continue;
                    },
                    Err(err) => {
                        if err.location_kind == ParseLocationKind::Before || err.location_kind == ParseLocationKind::Header {
                            cursor = err.cursor;
                            state = State::Markdown;
                            continue;
                        }
                        // skip this snippet or abord the whole markdown file?
                        // for now I will abort the whole md file since I don't known how to detect
                        // the end of the entry
                        return Err(ParseError {
                            msg: format!("Failed to parse entry: {}", err.msg),
                            cursor: err.cursor,
                            ..Default::default()
                        })
                    }
                }
            },
            State::Final => {
                return Ok(
                    ParsedFileCode {
                        tree: ParseOutput {
                            p: snippets,
                            start_loc: initial_cursor,
                            end_loc: cursor
                        }
                    }
                )
            }
        }
        cursor += 1;
    }
}
