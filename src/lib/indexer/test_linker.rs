use crate::database::labels::LabelEntryIndex;
use crate::indexer::linker::resolve_reference;
use crate::pdel_parser::UnresolvedReference;
use crate::database::models::{ResolvedReference, URI};

#[test]
fn test_resolve_reference_to_wikidata() {
    let res = resolve_reference(
        &LabelEntryIndex::empty(),
        UnresolvedReference::HardLocation("wdt:Q5".to_string()));
    assert_eq!(
        res.unwrap(),
        ResolvedReference::ExternalReference(URI("https://wikidata.org/wiki/Q5".to_string()))
    );
}
