use std::fs;
use std::path::PathBuf;
use std::assert_matches::assert_matches;


use crate::database::filter::filter_instance_of;
use crate::database::models::Entry;
use crate::database::search::search_entry_by_label;
use crate::indexer;
use crate::pdel_parser::preprocess::preprocess_code;
use crate::{fs_notebook::NotebookContext, indexer::add_id_on_entry_code};
use crate::pdel_parser::parse_entry;

use indoc::{indoc, formatdoc};
use temp_dir::TempDir;



#[test]
fn test_add_id_on_entry() {
    let entry_code = indoc! {r#"
        @Entry {
            foo: "bar",
            foo2: "bar",
        }
    "#}.to_string();

    let res = parse_entry(&entry_code, 0);
    let pout = res.unwrap();
    let res = add_id_on_entry_code(&vec![], &entry_code, &pout);
    assert!(res.is_ok(), "{:?}", res);
    let (id, new_code) = res.unwrap();

    let expected_output_code = formatdoc! {r#"
        @Entry {{
            id: "{id}",
            foo: "bar",
            foo2: "bar",
        }}
    "#}.to_string();

    assert_eq!(new_code, expected_output_code);
}

#[test]
fn test_add_id_on_entry_with_expansions() {
    let entry_code = indoc! {r#"
        // ééééé
        @Entry {
            foo: "bar",
            foo2: "bar",
        }
    "#}.to_string();

    dbg!(&entry_code);

    let preprocess_res = preprocess_code(&entry_code);
    assert_matches!(preprocess_res, Ok(_));
    let preprocess_val = preprocess_res.unwrap();

    dbg!(&preprocess_val);

    let parse_res = parse_entry(&preprocess_val.code_output, 0);
    assert_matches!(parse_res, Ok(_));

    let pout = parse_res.unwrap();
    let res = add_id_on_entry_code(&preprocess_val.offsets, &entry_code, &pout);
    assert_matches!(res, Ok(_));

    let (id, new_code) = res.unwrap();
    let expected_output_code = formatdoc! {r#"
        // ééééé
        @Entry {{
            id: "{id}",
            foo: "bar",
            foo2: "bar",
        }}
    "#}.to_string();

    assert_eq!(new_code, expected_output_code);
}


#[test]
fn test_basic_indexing_on_fs() {
    // let tmp_dir = std::path::Path::new("/tmp").join(format!("{}", uuid::Uuid::new_v4()));
    // assert_matches!(fs::create_dir(&tmp_dir), Ok(_));
    let tmp_dir = TempDir::new()
            .unwrap();
    let context = NotebookContext {
        base_path: 
            tmp_dir.path().join("notebook")
    };
    // create a notebook in this dir
    assert_matches!(fs::create_dir(&context.base_path), Ok(_));
    assert!(fs::create_dir(&context.sources_path()).is_ok());
    assert!(fs::create_dir(&context.internals_path()).is_ok());

    assert!(fs::write(&context.manifest_path(), r#"{"name":"Test notebook", "description": "Incroyable"}"#).is_ok());
    assert!(fs::write(&context.sources_path().join("note.md"), r#"
        # Ma super note de fou

        ## Premier paragraphe

        @Entry {
            name: "River"
        }

é
        filling text between two entries

        @Entry {
            is: [River]
            name: "Rivière Romaine"
        }
    "#).is_ok());

    let notebook = indexer::index_and_save(&context).unwrap();

    dbg!(&notebook.entries.iter().map(|x| &x.pure_entry).collect::<Vec<&Entry>>());

    let a_file = notebook.files.get(0).unwrap();
    let a_entry = notebook.entries.get(0).unwrap();
    let b_entry = notebook.entries.get(1).unwrap();

    assert_eq!(a_file.path, PathBuf::from("note.md"));
    assert_eq!(a_entry.source_file_id, a_file.id);
    
    assert_eq!(a_entry.pure_entry.name(), "River");
    assert_eq!(b_entry.pure_entry.name(), "Rivière Romaine");
}

#[test]
fn test_indexing_on_fs_contacts_notebook() {
    let tmp_dir = TempDir::new()
            .unwrap();
    let context = NotebookContext {
        base_path: 
            tmp_dir.path().join("notebook")
    };
    assert_matches!(fs::create_dir(&context.base_path), Ok(_));
    assert!(fs::create_dir(&context.sources_path()).is_ok());
    assert!(fs::create_dir(&context.internals_path()).is_ok());

    assert!(fs::write(&context.manifest_path(), r#"{"name":"Test notebook", "description": "Incroyable"}"#).is_ok());
    assert!(fs::write(&context.sources_path().join("note.md"), r#"
        @Entry {
            name: "Human"
        }
        @Human {
            name: "Some woman"
        }
        @Human {
            name: "Some man"
        }
        @Human {
            name: "Some child"
        }
    "#).is_ok());

    let notebook = indexer::index_and_save(&context).unwrap();
    dbg!(&notebook);

    let human_entry = search_entry_by_label(&notebook, "Human").get(0).unwrap().1;
    let humans = filter_instance_of(&notebook, &human_entry.pure_entry.id);
    assert_eq!(humans.len(), 3);
}

#[test]
fn test_indexing_on_fs_with_subentries() {
    let tmp_dir = TempDir::new().unwrap();
    let context = NotebookContext {
        base_path: tmp_dir.path().join("notebook")
    };
    // create a notebook in this dir
    assert_matches!(fs::create_dir(&context.base_path), Ok(_));
    assert!(fs::create_dir(&context.sources_path()).is_ok());
    assert!(fs::create_dir(&context.internals_path()).is_ok());

    assert!(fs::write(&context.manifest_path(), r#"{"name":"Test notebook", "description": "Incroyable"}"#).is_ok());
    assert!(fs::write(&context.sources_path().join("note.md"), r#"
        @Entry {
            name: "Event"
        }

        @Entry {
            is: [Event]
            participant: @Human {
                name: "John Doe"
            }
        }
    "#).is_ok());

    let notebook = indexer::index_and_save(&context).unwrap();

    dbg!(&notebook.entries.iter().map(|x| &x.pure_entry).collect::<Vec<&Entry>>());
}
