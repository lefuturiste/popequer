use crate::database::labels::LabelEntryIndex;
use crate::database::models::{EntryClaim, EntryValue, Reference};
use crate::pdel_parser::UnresolvedReference;
use crate::database::models::{EntryContainer, ResolvedReference, URI};
use crate::database::ids::Id;
use anyhow::{anyhow, Result, Context};

/// get the searchable name for an entity
/// TODO: add a label index as argument
pub fn resolve_reference(labels_index: &LabelEntryIndex, unresolved_ref: UnresolvedReference) -> Result<ResolvedReference> {
    match unresolved_ref {
        UnresolvedReference::SoftLocation(loc) => {
            // search in our database first then in external database like wikidata
            match labels_index.singular_search(loc) {
                Some(id) => Ok(ResolvedReference::InternalReference(id)),
                None => Err(anyhow!("Did not find an entry based on the soft location"))
            }
        },
        UnresolvedReference::HardLocation(loc) => {
            if loc.starts_with("wdt:Q") || loc.starts_with("Q") {
                return Ok(ResolvedReference::ExternalReference(
                    URI(
                        format!("https://wikidata.org/wiki/{}",
                            loc.split("wdt:").nth(1)
                                .ok_or(anyhow!("Invalid wikidata ref"))?
                        )
                    )
                ));
            }
            let id = Id::from_repr(&loc).context("Cannot parse ID")?;
            return Ok(ResolvedReference::InternalReference(id))
        }
    }
}

pub fn link_claims(labels_index: &LabelEntryIndex, claims: &mut Vec<EntryClaim>) -> Result<usize> {
    let mut linked_count = 0;
    for claim in claims.iter_mut() {
        match &claim.value {
            EntryValue::Reference(r) => {
                match r {
                    Reference::Unresolved(unresolved_ref) => {
                        // for now we just try to resolve, and we ignore if we don't manage to
                        // resolve
                        // FIXME: Error or warn if the resolve reference failed for a hard
                        // location
                        match resolve_reference(labels_index, unresolved_ref.clone()) {
                            Ok(resolved) => {
                                linked_count += 1;
                                claim.value = EntryValue::Reference(
                                    Reference::Resolved(resolved)
                                )
                            },
                            Err(e) => {
                                eprintln!("Warning: failed to link claim: {:?} {:?}", unresolved_ref, e)
                            }
                        }
                    },
                    _ => ()
                }
            },
            _ => ()
        }
    }
    Ok(linked_count)
}
