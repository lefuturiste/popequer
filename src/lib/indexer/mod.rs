use crate::database::ids::Id;
use crate::database::labels::LabelEntryIndex;
use crate::database::graph::BackLinksIndex;
use crate::database::models::{self, filter_claims_by_property, Entry, EntryContainer, EntryValue, Manifest, Notebook, Property, SourceFile};
use crate::database::parse_extractor::{flatten_embedded_entries, get_pure_claims, ExtractErr};
use crate::pdel_parser::preprocess::{apply_expansions, preprocess_code, AdaptableRange, Expansion};
use crate::pdel_parser::{PEntryValue, UnresolvedReference};
use crate::pdel_parser::{markdown::parse_markdown, parse_wrapper, PEntry, ParseOutput};
use crate::unwrap_or_return;
use crate::indexer::manifest::ScanManifestErr;
use crate::fs_notebook::NotebookContext;
use crate::utils::Substring;

use uuid::Uuid;
use bincode::serialize;
use anyhow::{anyhow, Result, Context};
use notify::{Config, RecommendedWatcher, RecursiveMode, Watcher};
use std::path::{Path, PathBuf};
use std::io;
use std::fs;
// import the write trait (not directly used)
use std::io::Write;

pub mod linker;

mod manifest;

#[cfg(test)]
mod test_linker;

#[cfg(test)]
mod test_indexer;


#[derive(Debug)]
pub enum IndexingErr {
    CannotOpen,
    IoErr,
    PreprocessingErr,
    ParseErr,
    LinkErr,
    CannotProcessCodeToAddId,
    CannotWriteFileToAddId(io::Error),
    ExtractErr(anyhow::Error),
    RExtractErr(ExtractErr),
    ManifestErr(ScanManifestErr)
}

#[derive(Debug)]
struct IndexingResult {
    files: Vec<SourceFile>,
    entries: Vec<EntryContainer>
    // TODO: include parse error here
}

/// 
/// # Arguments
///
/// * `file_id` - the id that you known
/// 
/// - parse entry
/// - check for existing id
/// - if id missing add the code
/// - reparse
/// - extract the pure entry
/// TODO: have a temp buffer that store the content of the file to prevent rewriting it everytime
fn index_file(base_dir: &Path, file_path: &Path, file_id: Uuid) -> Result<IndexingResult, IndexingErr> {
    eprintln!("Indexing file {:?}", file_path);
    // keep an existing id or get a new one
    // or use the git id?
    let mut contents = fs::read_to_string(file_path)
        .expect("Should be able to read the file, there is something wrong with it");

    let preprocessed_val = preprocess_code(&contents)
        .map_err(|_x| IndexingErr::PreprocessingErr)?;
    let source_file = SourceFile {
            id: file_id,
            path: file_path
                .strip_prefix(base_dir)
                .expect("Should be able to strip prefix")
                .to_path_buf(),
            expansions: preprocessed_val.offsets
        };
    
    let parsed_file = parse_markdown(&preprocessed_val.code_output, 0)
        .map_err(|_e|
            IndexingErr::ParseErr
        )?;
    
    // TODO: call a function to flatten all the entries (handle embedded entries)
    // it would be great if we can completely abstract the add missing id and rewrite process, to
    // be able to test it more easily

    let mut entries_containers: Vec<EntryContainer> = vec![];
    let mut parsed_entries = parsed_file.tree.p;
    parsed_entries.reverse();
    while let Some(pentry) = parsed_entries.pop() {
        let mut pentry = pentry;
        let pure_entry = match pentry.p.claims.p.iter().find(|c| c.p.property.p == "id") {
            None => {
                // missing id claim, create the id
                // pass the context of the source file
                let (_new_id, contents) = add_id_on_entry_code(&source_file.expansions, &contents, &pentry)
                    .map_err(|_e| IndexingErr::CannotProcessCodeToAddId)?;

                // FIXME(performance): is possible, when multiple new entries are discovered, don't rewrite all code
                // but we will need to have a way to update the parsed entry with new position
                // (start_loc and end_loc) values that reflec the fact that an Id was inserted
                fs::write(file_path, contents)
                    .map_err(|e| IndexingErr::CannotWriteFileToAddId(e))?;
                return index_file(base_dir, file_path, file_id);
            },
            Some(_id_claim) => {
                // sometime an entry can contains in fact two or more entries (embedded)
                let flatten_val = flatten_embedded_entries(&pentry);

                // for each sub entries detected:
                //     add it to the parsed entries stack
                //     if it has an id claim:
                //         set the parent claim to a reference
                //     else:
                //         set the "child need indexing flag" to true
                //  if the child need indexing flag is set:
                //     continue and skip the rest of the loop
                //  (to avoid extract the entry uselessly)

                let mut need_indexing: bool = false;
                for (parent_claim_pos, sub_entry) in flatten_val {
                    parsed_entries.push(sub_entry.clone());
                    // check if the sub entry has an id
                    // extract this id and set the claim
                    if let Some(sub_id_claim) = sub_entry.p.claims.p.iter()
                        .find(|c| c.p.property.p == "id") {
                        let id_repr = match &sub_id_claim.p.value_container.p.value.p {
                            PEntryValue::String(v) => v,
                            _ => panic!("Expected a String as a claim value type for the id property")
                        };
                        let parent_claim = pentry.p.claims.p
                            .get_mut(parent_claim_pos)
                            .expect("Refind the claim that define the subentry");
                        parent_claim.p.value_container.p.value.p =
                            PEntryValue::Reference(UnresolvedReference::HardLocation(id_repr.to_string()));
                        continue;
                    }
                    need_indexing = true;
                    break;
                }
                if need_indexing {
                    continue;
                }

                Entry::from_parsed_entry_with_id(&pentry)
                    .map_err(|e| IndexingErr::RExtractErr(e))?
            }
        };

        entries_containers.push(EntryContainer {
            source_file_id: file_id,
            pure_entry,
            parsed_entry: pentry,
        })
    }

    Ok(IndexingResult {
        files: vec![source_file],
        entries: entries_containers
    })
}

/// depth-first search recursive indexing of a directory
fn index_dir(base_dir: &Path, dir_path: &Path) -> Result<IndexingResult, IndexingErr> {
    let files_to_index = match fs::read_dir(dir_path) {
        Err(_err) => {
            return Err(IndexingErr::CannotOpen);
        },
        Ok(files) => files
    };

    let mut entries_containers: Vec<EntryContainer> = vec![];
    let mut indexed_files: Vec<SourceFile> = vec![];

    // TODO: have a database of checksum to only reparse if the file has changed? benefices pas
    // certains
    for file in files_to_index{
        let path = file.unwrap().path();

        match 
            match path.is_dir() {
                true => index_dir(base_dir, &path),
                false => index_file(base_dir, &path, Uuid::new_v4())
            }
         {
            Ok(index_res) => {
                indexed_files.extend(index_res.files);
                entries_containers.extend(index_res.entries);
            },
            Err(err) => {
                return Err(err);
            }
        }
    }

    // we preparsed the entites, now going to link them
    // TODO: index into a graph db? IndraDB or sqlite?
    //
    // for each entity, index the names into the table,
    // then check if the names that is referenced in the entry are valid
    // if valid, replace them with actual id (or pointer) of the referenced entry
    // look for an existing id,
    // if not generate an ID, and prepend it to the claim

    Ok(IndexingResult {
        files: indexed_files,
        entries: entries_containers
    })
}


/// Add an id on Entry code String, based on the ParseOutput positions
fn add_id_on_entry_code(expansions: &Vec<Expansion>, inp_file_code: &String, pout: &ParseOutput<PEntry>) -> Result<(Id, String)> {
    // TODO: handle cases with macros, apply macros expansions offsets on positions
    // generate new id
    let id = Id::generate();

    let first_claim = pout.p.claims.p.iter().nth(0).ok_or(anyhow!("Expected at least one claim"))?;

    // detect whitespace alignment
    let whitespace_range = (pout.p.claims.start_loc..first_claim.start_loc)
        .apply_expansions(&expansions);

    let alignment = inp_file_code.sub(whitespace_range)
        .ok_or(anyhow!("Cannot compute whitespace alignment"))?
        .chars().filter(|c| *c == ' ' || *c == '\t')
        .count();


    let id_claim_pdel_code = format!("\n{}id: {:?},", " ".repeat(alignment), id.to_repr());

    // in progress of fix: insert at unicode scalar position instead of byte position
    let insert_position = apply_expansions(&expansions, pout.p.claims.start_loc)+1;
    let end_position = inp_file_code.len();
    let out_file_code = format!(
        "{}{}{}",
        inp_file_code.sub(0..insert_position)
            .ok_or(anyhow!("Could not extract code before the place to insert new code"))?,
        id_claim_pdel_code,
        inp_file_code.sub(insert_position..end_position)
            .ok_or(anyhow!("Could not extract code after the place to insert new code"))?,
    );

    Ok((id, out_file_code))
}

fn index_notebook(context: &NotebookContext) -> Result<Notebook, IndexingErr>
{
    // scan manifest file to get the name, description and others setting of the notebook
    let manifest = Manifest::from_path(&context.manifest_path())
        .map_err(|e| IndexingErr::ManifestErr(e))?;

    let index_res = match index_dir(&context.sources_path(), &context.sources_path()) {
        Ok(res) => res,
        Err(err) => {
            return Err(err)
        }
    };

    Notebook::build(
        manifest,
        index_res.files,
        index_res.entries
    )
}

/// serialize and save database to binary file
fn save_db_in_fs(notebook: &Notebook, database_path: &Path) -> Result<(), IndexingErr> {
    let mut file = match fs::File::create(database_path) {
        Ok(res) => res,
        Err(e) => {
            
            return Err(IndexingErr::IoErr);
        }
    };
    let bytes = match serialize(&notebook) {
        Ok(res) => res,
        Err(e) => {
            dbg!(e);
            return Err(IndexingErr::IoErr);
        }
    };
    match file.write_all(&bytes) {
        Ok(_) => {},
        Err(e) => {
            dbg!(e);
            return Err(IndexingErr::IoErr);
        }
    }
    Ok(())
}

pub fn index_and_save(notebook_context: &NotebookContext) -> Result<Notebook, IndexingErr> {
    // we use a file inside the internals directory which will store multiple files in the future.
    let notebook = index_notebook(&notebook_context)?;
    save_db_in_fs(&notebook, &notebook_context.database_path())?;

    Ok(notebook)
}

/// Daemon main code
/// TODO: implement partial re-indexing of Notebook
pub fn index_watch_daemon(notebook_context: &NotebookContext) -> Result<(), IndexingErr> {
    // fully index one time and then watch and update the first notebook object
    let mut notebook: Notebook = unwrap_or_return!(
        index_and_save(notebook_context),
        |err| err
    );
    println!("Indexed notebook found {} entries", notebook.entries.len());
    println!("Starting the watch daemon...");

    let src_path = notebook_context.sources_path();

    match watch(src_path) {
        Ok(_) => {},
        Err(err) => {
            println!("Watch failed: {:?}", err);
        }
    }
    println!("end of watch");

    Ok(())
}


fn watch<P: AsRef<Path>>(path: P) -> notify::Result<()> {
    let (tx, rx) = std::sync::mpsc::channel();

    // Automatically select the best implementation for your platform.
    // You can also access each implementation directly e.g. INotifyWatcher.
    let mut watcher = RecommendedWatcher::new(tx, Config::default())?;

    // Add a path to be watched. All files and directories at that path and
    // below will be monitored for changes.
    watcher.watch(path.as_ref(), RecursiveMode::Recursive)?;

    // infinite channel
    // FIXME: handle channel errors?
    for res in rx {
        match res {
            Ok(event) => println!("changed: {:?}", event),
            Err(e) => println!("watch error: {:?}", e),
        }
    }

    Ok(())
}

impl Notebook {
    /// build a notebook from components
    pub fn build(
        manifest: Manifest,
        files: Vec<SourceFile>,
        entries_containers: Vec<EntryContainer>
    ) -> Result<Notebook, IndexingErr> {
        let mut containers = entries_containers;
        // 2. index all entries labels in a tree
        // HashMap<(name|alias), Id>
        // we can reuse the hashmap later, we can save it in the database file
        let labels_index = LabelEntryIndex::from_pure_entries(
            containers.iter().map(|x| &x.pure_entry).collect()
        );

        // 3. now link the entities with the HashMap
        let mut linked_claims_count = 0;
        for container in &mut containers {
            linked_claims_count += linker::link_claims(&labels_index, &mut container.pure_entry.claims)
                .map_err(|_| IndexingErr::LinkErr)?;
        }
        eprintln!("Indexing: Linked {} entries claims", linked_claims_count);
        
        // 4. now we can create a directed Graph to find the backlinks
        let backlinks = BackLinksIndex::build_from_entries(
            containers.iter().map(|x| &x.pure_entry).collect()
        );
        eprintln!("Indexing: Created backlinks graph");

        Ok(Notebook {
            manifest,
            created_at: chrono::offset::Utc::now(),
            updated_at: chrono::offset::Utc::now(),
            entries: containers,
            files,
            labels_index,
            backlinks
        })
    }

    /// build virtual notebook from one single markdown string
    /// for now does not support macros
    pub fn build_virtual(
        manifest: Manifest,
        markdown_code: &str
    )-> Result<Notebook, IndexingErr> {
        let markdown_val = parse_markdown(&markdown_code, 0).map_err(
            |_e| IndexingErr::ParseErr
        )?;
        let sf = SourceFile {
            id: Uuid::new_v4(),
            path: PathBuf::from("./all_in_one_virtual.md"),
            expansions: vec![]
        };

        let mut entries_containers: Vec<EntryContainer> = vec![];

        for parsed_entry in markdown_val.tree.p {
            entries_containers.push(EntryContainer {
                pure_entry: Entry::from_parsed_entry_with_id(&parsed_entry)
                    .map_err(|e| IndexingErr::RExtractErr(e))?,
                parsed_entry,
                source_file_id: sf.id
            })
        }

        Notebook::build(
            manifest,
            vec![sf],
            entries_containers
        )
    }
}

