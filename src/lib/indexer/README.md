# Indexer/Database

Popequer system indexer. To allow for note taking.

Draft v1:

- Parse every markdown file
- Parse multiple entries
- Process the parsed Entries
- Put everything inside a big binary file with serde

Components:
- understand a reference
    - search a reference with fuzzy finding in the database
    - make the link with wikidata
- parse and execute a query

Create views:
- calendar view
- contacts view

Ingress:
- import contacts from carddav
- import events from caldav

Design questions:
- How to handle a way to extend wikidata items with private infos
- How to handle link to files or blob

## Indexing steps

- Index dir recursively
    - index files
        - parse markdown
            - preprocess to remove comments, expand shortcuts, which give a list of mapping
            - detect each entry "snippet" in the preprocessed output
                - parse the entry into syntax tree with ParseOutput
        - extract each parsed entry into a pure entry
        - if missing, add id to entries, it use the expansions offsets
- Link the entries, resolve the references in the pure entries

## The query language

I want to design a more simple query language than SparQL 
Probably something more inspired from [Overpass-QL](https://wiki.openstreetmap.org/wiki/Overpass_API/Overpass_QL)
- When we need to query shit, use a made-up query language from the same claims syntax

ex: `is: [Human], age >= 10`
ex: `is: [Human], age >= 10`

