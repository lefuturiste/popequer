use std::path::Path;
use std::io;
use std::fs;
use fully_pub::fully_pub;
use crate::database::models::Manifest;

#[fully_pub]
#[derive(Debug)]
enum ScanManifestErr {
    FileNotFound,
    /// Invalid file
    CannotReadFile(io::Error),
    /// Invalid JSON
    ParseError(serde_json::Error),
    /// missing a field or something
    InvalidScheme
}

impl Manifest {
    /// Scan a fanifest file
    pub fn from_path(manifest_path: &Path) -> Result<Manifest, ScanManifestErr> {
        if !manifest_path.exists() { return Err(ScanManifestErr::FileNotFound); }
        
        let manifest_content = fs::read_to_string(manifest_path)
            .map_err(|err| ScanManifestErr::CannotReadFile(err))?;
        
        let data: Manifest = serde_json::from_str(&manifest_content)
            .map_err(|err| ScanManifestErr::ParseError(err))?;

        Ok(data)
    }
       
    pub fn example() -> Manifest {
        Manifest {
            name: "Example Notebook".to_string(),
            description: "Notebook description".to_string(),
        }
    }
}
