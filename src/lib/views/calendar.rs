use anyhow::{anyhow, Result};
use chrono::{DateTime, Duration, Utc};
use fully_pub::fully_pub;

use crate::database::{
    filter::filter_instance_of,
    models::{filter_claims_by_property, Entry, EntryContainer, EntryValue, Notebook},
    search::search_entry_by_label
};

#[fully_pub]
#[derive(Debug)]
struct Event<'a> {
    name: String,
    description: String,
    start_time: DateTime<Utc>,
    end_time: Option<DateTime<Utc>>,
    entry: &'a Entry
}

/// will scan notebook for entries that inherit events and return upcoming events
pub fn get_upcoming_events<'a>(notebook: &'a Notebook) -> Result<Vec<Event<'a>>> {

    let entry_class_search = search_entry_by_label(notebook, "Event");
    
    let entry_class: &EntryContainer = entry_class_search
        .iter()
        .filter(|x| x.0 <= 1)
        .nth(0)
        .ok_or(anyhow!("Cannot find event class"))
        .map(|x| x.1)
    ?;
    
    let upcoming_events = filter_instance_of(notebook, &entry_class.pure_entry.id);

    Ok(upcoming_events.iter().filter_map(|e| {
        let start_time = match (
            match filter_claims_by_property(&e.pure_entry.claims, "start_time").get(0) {
                None => { return None; },
                Some(c) => c
            }).value {
                EntryValue::Time(x) => x,
                _ => { return None; } // we discard events with no start_time
            };

        let horizon = Utc::now().checked_sub_signed(Duration::hours(8)).unwrap();
        if start_time < horizon {
            return None;
        }

        let end_time = filter_claims_by_property(&e.pure_entry.claims, "end_time")
            .get(0)
            .and_then(|x|
                match x.value {
                    EntryValue::Time(x) => Some(x),
                    _ => None
                }
            );

        Some(Event {
            name: e.pure_entry.name(),
            description: "".to_string(),
            start_time,
            end_time,
            entry: &e.pure_entry
        })
    }).collect())
}
