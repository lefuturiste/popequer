use crate::database::filter::filter_by_claims;
use crate::database::parse_extractor::get_pure_claims;
use crate::database::models::{Notebook, EntryContainer};
use crate::indexer::linker::link_claims;
use anyhow::{anyhow, Context, Result};

/// Will parse and run a complex query expression
/// TODO: create a fully featured language with | and & expressions 
pub fn query<'a>(notebook: &'a Notebook, complex_query: &str) -> Result<Vec<&'a EntryContainer>> {
    let parsed_claims = crate::pdel_parser::parse_claims_query(&complex_query)
        .map_err(|_e| anyhow!("Could not parse claims query"))?;
        
    let mut query_claims = get_pure_claims(&parsed_claims.p)
        .map_err(|_e| anyhow!("Could not extract pure claims"))?;
    
    // link
    let _link_count = link_claims(&notebook.labels_index, &mut query_claims);

    // we reparse complex query as entries claims, but we will need something to later associate
    // for now we just do a AND on all the claims
    // will apply claims filter 
    Ok(filter_by_claims(notebook, query_claims))
}

