use crate::pdel_serializer::{serialize, SerializerSettings};
use crate::pdel_serializer::overloading::{load_overloading, OverloadingResult};
use std::path::Path;
use std::fs;
use std::path;
use std::io::Write;
use wikidata;

// const LOCALES: Vec<&str> = vec!["fr", "en"];

// https://example.org

#[derive(Debug)]
pub enum FetchItemError {
    JsonParsingError(serde_json::Error),
    DeserializationError(wikidata::EntityError),
    TextParsingError(reqwest::Error),
    HttpError(reqwest::Error)
}

const ITEM_CACHING_PATH: &str = "./.tmp/wikidata";

/// Will fetch JSON of the item and deserialize to a typed WikiData entry
/// Can also cache in a temporary file
pub fn fetch_item(id: u64) -> Result<wikidata::Entity, FetchItemError> {
    // check for existing file
    let mut text_content = String::new();
    let cached_file_str_path = format!("./{ITEM_CACHING_PATH}/Q{id}.json");
    let cached_file_path = path::Path::new(&cached_file_str_path);

    if cached_file_path.exists() {
        text_content = fs::read_to_string(cached_file_path).expect("Read caching file that exists");
    } else {
        let url = format!("https://www.wikidata.org/wiki/Special:EntityData/Q{}.json", id);
        // let url = format!("http://localhost:6969/wikidata_sources/Q{}.json", id);
        // println!("{}", url);
        let response_res = reqwest::blocking::get(url);
        if let Err(err) = response_res {
            return Err(FetchItemError::HttpError(err));
        }
        let text_parsing_res = response_res.unwrap().text();
        if let Err(err) = text_parsing_res {
            return Err(FetchItemError::TextParsingError(err))
        }
        text_content = text_parsing_res.unwrap();

        // write the tmp file
        let mut new_caching_file = fs::File::create(cached_file_path).expect("Create caching file");
        writeln!(&mut new_caching_file, "{text_content}").expect("Write to caching file");
        // new_caching_file.close();
    }


    let json_parsing_res: Result<serde_json::Value, serde_json::Error> = serde_json::from_str(&text_content);
    if let Err(err) = json_parsing_res {
        return Err(FetchItemError::JsonParsingError(err));
    }

    let deserialization_res = wikidata::Entity::from_json(json_parsing_res.unwrap());
    if let Err(err) = deserialization_res {
        return Err(FetchItemError::DeserializationError(err));
    }

    Ok(deserialization_res.unwrap())
}

pub fn load_properties_overloading(overloading_dir_path: &Path) -> OverloadingResult {
    let name = "wdt_properties_overloading.yaml";
    let path = overloading_dir_path.join(name);
    load_overloading(&path)
}

pub fn load_items_overloading(overloading_dir_path: &Path) -> OverloadingResult {
    let name = "wdt_items_overloading.yaml";
    let path = overloading_dir_path.join(name);
    load_overloading(&path)
}

#[derive(Debug)]
pub enum MaterializeError {
    FetchItemError(FetchItemError),
    OverloadingLoadError
}

pub fn materialize_item(entity: wikidata::Entity) -> Result<String, MaterializeError> {

    // need to split between fetching the thing, and serialize the item with wikidata specificities

    let settings = SerializerSettings {
        languages: vec![wikidata::Lang("fr".into()), wikidata::Lang("en".into())]
    };

    let overloading_dir_path = Path::new("./data_schema");
    let props_overloading = match load_properties_overloading(overloading_dir_path) {
        Ok(o) => o,
        Err(_) => {
            return Err(MaterializeError::OverloadingLoadError)
        }
    };
    let items_overloading = match load_items_overloading(overloading_dir_path) {
        Ok(o) => o,
        Err(_) => {
            return Err(MaterializeError::OverloadingLoadError)
        }
    };
    let serializer_out = serialize(
        settings, props_overloading, items_overloading, &entity
    );

    Ok(serializer_out)
}

