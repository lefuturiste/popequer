use std::ops::Range;

pub trait Substring {
    fn sub(&self, range: Range<usize>) -> Option<String>;
}

impl Substring for String {
    fn sub(&self, range: Range<usize>) -> Option<String> {
        if range.start > self.len() || range.end > self.len() {
            return None;
        }
        Some(self.chars()
            .skip(range.start)
            .take(range.end - range.start)
            .collect())
    }
}


// 'ident' is short for identifier token
#[macro_export]
macro_rules! pub_fields {
    {
        $(#[doc = $doc:expr])?
        $(#[derive($($macros:tt)*)])*
        struct $name:ident {
            $(
                $(#[$field_macro:expr])*
                $field:ident: $t:ty$(,)?
            )*
        }
    } => {
        $(#[derive($($macros)*)])*
        pub struct $name {
            $(
                $(#[$field_macro])*
                pub $field: $t,
            )*
        }
    }
}


#[macro_export]
macro_rules! os_str_to_str {
    ( $e: expr, $err: expr ) => {
        match $e.to_str() {
            Some(r) => r.to_string(),
            None => {
                return Err($err)
            }
        }
    }
}


#[macro_export]
macro_rules! unwrap_or_return {
    ( $to_match: expr, $err_func: expr ) => {
        match $to_match {
            Ok(res) => res,
            Err(err) => {
                return Err($err_func(err))
            }
        }
    }
}


#[macro_export]
macro_rules! unwrap_or_break {
    ( $to_match: expr, $err_func: expr ) => {
        match $to_match {
            Ok(res) => res,
            Err(err) => {
                $err_func(err);
                break;
            }
        }
    }
}

#[macro_export]
macro_rules! unwrap_opt_or_return {
    ( $e: expr, $err: expr ) => {
        match $e {
            Some(res) => res,
            None => {
                return Err($err);
            }
        }
    }
}

/// macros for state machines
/// TODO: have a module "state machine"
#[macro_export]
macro_rules! get_symbol_or_final {
    ($subject:ident, $state:ident, $cursor:ident) => {
        match $subject.chars().nth($cursor) {
            None => {
                $state = State::Final;
                continue;
            },
            Some(x) => x
        }
    }
}

pub fn clamp(lower: usize, upper: usize, subj: usize) -> usize {
    if subj > upper {
        return upper;
    }
    if subj < lower {
        return lower;
    }
    subj
}

/// the debug state machine macro can be used in the state machine loop and will print the state
/// and the position of the cursor along with a preview of what the machine is seeing
#[macro_export]
macro_rules! debug_state_machine {
    ($prefix:expr, $subject:expr, $state:expr, $cursor:expr) => {
        let subj: &str = $subject;
        let start_bound: usize = $cursor;
        let end_bound: usize = crate::utils::clamp(0, subj.len(), $cursor+10);
        let extract_opt = subj.to_string()
            .sub(start_bound..end_bound);

        match extract_opt {
            None => eprintln!("{}: {:?} - {:?} OUT_OF_BOUND", $prefix, $state, $cursor),
            Some(extract) => eprintln!("{}: {:?} - {:?} {:?}", $prefix, $state, $cursor, extract)
        }
    };
}


// /// Check if a vector contains many things at once
// /// ($($thing:ident)*)
// #[macro_export]
// macro_rules! contains_many {
//     ($array:expr, $($val:expr),*) => {
//         $( $array.contains($val) ) && *
//     };
// }


#[macro_export]
macro_rules! contains {
    ($array:expr, [$( $val:expr),*] ) => {
        $( $array.contains($val) ) && *
    };
    ($array:expr, not [$( $not_val:expr ),*], [$( $val:expr),*] ) => {
        ($( !$array.contains($not_val) ) && *) && $( $array.contains($val) ) && *
    };
}

/// Test if two buffers are equals line by line
/// Useful in unit tests
pub fn assert_line_by_line(actual_output: &str, expected_output_lines: Vec<&str>) {
    let actual_output_lines: Vec<&str> = actual_output
        .split("\n")
        .filter(|l| !l.trim().is_empty())
        .collect();
    for (actual_line, expected_line) in actual_output_lines
        .iter().zip(expected_output_lines)
    {
        assert_eq!(actual_line.trim(), expected_line);
    }
}

#[macro_export]
macro_rules! unwrap_enum_variant {
    {$base:expr, $variant:path} => {
        match $base {
            $variant(inner_val) => inner_val,
            _ => panic!("unwrap_enum_variant: Unexpected enum variant.")
        }
    }
}
