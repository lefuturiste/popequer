use fully_pub::fully_pub;
use std::path::{Path, PathBuf};
use std::env;

const MANIFEST_PATH: &str = "notebook.json";

/// a struct to ease the access to common path in a Notebook
#[fully_pub]
#[derive(Debug)]
struct NotebookContext {
    base_path: PathBuf,
}

impl NotebookContext {
    pub fn sources_path(&self) -> PathBuf {
        self.base_path.join("src")
    }

    pub fn internals_path(&self) -> PathBuf {
        self.base_path.join(".internals")
    }

    pub fn database_path(&self) -> PathBuf {
        self.internals_path().join("db.bin")
    }

    pub fn manifest_path(&self) -> PathBuf {
        self.base_path.join("notebook.json")
    }
}


#[fully_pub]
#[derive(Debug)]
enum CheckNotebookDirectoryErr {
    /// no git found
    NoGit,
    /// no notebook manifest file "notebook.json" found
    NoManifest,
    /// no internal dir found (to store the db)
    NoInternalsDir,
    /// no source dir found (./src) (to store the markdown files)
    NoSourceDir
}

pub fn check_notebook_directory(path: &PathBuf) -> Result<(), CheckNotebookDirectoryErr> {
    // check for a notebook.json file
    let manifest_path = path.join(MANIFEST_PATH);
    if !(manifest_path.exists() && manifest_path.is_file()) {
        return Err(CheckNotebookDirectoryErr::NoManifest);
    }

    // check for the git presence
    let git_path = path.join(".git");
    if !(git_path.exists() && git_path.is_dir()) {
        return Err(CheckNotebookDirectoryErr::NoGit);
    }

    // check for internals dir presence (under .internals)
    let internal_path = path.join(".internals");
    if !(internal_path.exists() && internal_path.is_dir()) {
        return Err(CheckNotebookDirectoryErr::NoInternalsDir);
    }

    // check for sources dir presence
    let internal_path = path.join("src");
    if !(internal_path.exists() && internal_path.is_dir()) {
        return Err(CheckNotebookDirectoryErr::NoInternalsDir);
    }

    Ok(())
}


#[derive(Debug)]
#[fully_pub]
enum GetNotebookPathErr {
    /// tried to get the working dir but failed to do so after looking for a env var
    CannotGetWorkingDir,
    /// the path provided by the POPEQUER_NOTEBOOK env var doesn't exists
    EnvPathUnknown,
    /// the path provided by the POPEQUER_NOTEBOOK env var is not a directory
    EnvPathNotDir
}

/// acquire a path to a supposed notebook root directory
/// will first check for a env var containing the path,
/// if not present will use the current working directory
/// the path returned doesn't is necessarly a notebook directory
pub fn get_notebook_path() -> Result<PathBuf, GetNotebookPathErr> {
    if let Ok(raw_path) = env::var("POPEQUER_NOTEBOOK") {
        let path = Path::new(&raw_path);
        if !path.exists() {
            return Err(GetNotebookPathErr::EnvPathUnknown);
        }
        if !path.is_dir() {
            return Err(GetNotebookPathErr::EnvPathNotDir);
        }
        return Ok(path.to_path_buf());
    }

    let working_dir = match env::current_dir() {
        Ok(path) => path,
        Err(..) => return Err(GetNotebookPathErr::CannotGetWorkingDir)
    };
    if !working_dir.exists() {
        return Err(GetNotebookPathErr::CannotGetWorkingDir);
    }

    Ok(working_dir)
}

