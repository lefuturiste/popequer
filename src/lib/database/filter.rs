use crate::database::{ids::Id, models::{EntryContainer, EntryClaim, EntryValue, Notebook, Property, Reference, ResolvedReference}};

use super::models::filter_claims_by_prop_and_val;

pub fn filter_by_claims<'a>(notebook: &'a Notebook, query_claims: Vec<EntryClaim>) -> Vec<&'a EntryContainer> {
    notebook.entries.iter().filter(|entry_container| {
        query_claims.iter().all(|qc| {
           entry_container.pure_entry.claims
                .iter().any(|ec| {
                    qc.property == ec.property && qc.value == ec.value
                })
        })
    }).collect()
}

/// get a list of entry that matches the instances_of property value or that inherit from sub_class
pub fn filter_instance_of<'a>(notebook: &'a Notebook, parent_class_id: &Id) -> Vec<&'a EntryContainer> {
    notebook.backlinks.0.get(&parent_class_id)
        .unwrap_or(&vec![])
        .iter()
        .map(|re_id| {
            let re = notebook.entries.iter().find(|ec| ec.pure_entry.id == *re_id)
                .expect("Expected to resolve internal ref");

            if !filter_claims_by_prop_and_val(
                &re.pure_entry.claims,
                "sub",
                EntryValue::Reference(Reference::Resolved(ResolvedReference::InternalReference(parent_class_id.clone())))
            ).is_empty() {
                filter_instance_of(notebook, &re.pure_entry.id)
            } else {
                vec![re]
            }
        }).flatten().collect()
}

