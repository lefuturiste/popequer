use std::assert_matches::assert_matches;

use crate::database::ids::Id;

use super::{models::{Manifest, Notebook}, search::search_complex};

#[test]
fn test_filter_by_single_claim() {
    let entries_code = r#"
        @Entry {
            id: "AAAAAAAAAA",
            foo: "bar"
        }
        @Entry {
            id: "AAAAAAAAAB",
            foo: "another_value"
        }
    "#;
    let notebook_res = Notebook::build_virtual(
        Manifest::example(),
        entries_code
    );
    assert_matches!(notebook_res, Ok(_));
    let notebook = notebook_res.unwrap();
    assert_eq!(notebook.entries.len(), 2);

    let search_res = search_complex(&notebook, r#"{ foo: "bar" }"#);
    let found_entries = search_res.unwrap();

    assert_eq!(found_entries.len(), 1);
    assert_eq!(
        found_entries.get(0).unwrap().pure_entry.id,
        Id::from_repr("AAAAAAAAAA").unwrap()
    )
}

#[test]
fn test_filter_by_claims_and() {
    let entries_code = r#"
        @Entry {
            id: "AAAAAAAAAA",
            name: "Bike"
        }
        @Entry {
            id: "AAAAAAAAAC",
            is: [Bike],
            mass: 15
        }
        @Entry {
            id: "AAAAAAAAAB",
            foo: "another_value"
        }
    "#;
    let notebook_res = Notebook::build_virtual(
        Manifest::example(),
        entries_code
    );
    assert_matches!(notebook_res, Ok(_));
    let notebook = notebook_res.unwrap();

    let search_res = search_complex(&notebook, "{ is: [Bike], mass: 15 }");
    let found_entries = search_res.unwrap();

    assert_eq!(found_entries.len(), 1);
    assert_eq!(
        found_entries.get(0).unwrap().pure_entry.id,
        Id::from_repr("AAAAAAAAAC").unwrap()
    )
}

#[test]
fn test_filter_by_claims_includes_tree_subs() {
    // we define Event as the root of our Tree,
    // The filter by Event must all leaf children that inherit from Event
    let entries_code = r#"
        @Entry {
            id: "AAAAAAAAA2",
            name: "Event"
        }
        @Entry {
            id: "AAAAAAAAAB",
            sub: [Event],
            name: "Proclamation"
        }
        @Entry {
            id: "AAAAAAAAAC",
            sub: [Event],
            name: "Inauguration"
        }
        @Entry {
            id: "AAAAAAAAAD",
            sub: [Event],
            name: "Accident"
        }
        @Entry {
            id: "AAAAAAAAAE",
            sub: [Accident],
            name: "Traffic Accident"
            description: "This is the item that represent the topic of a traffic accident"
        }
        @Entry {
            id: "AAAAAAAAAF",
            is: [Accident],
            name: "Some precise accident"
        }
    "#;
    let notebook_res = Notebook::build_virtual(
        Manifest::example(),
        entries_code
    );
    assert_matches!(notebook_res, Ok(_));
    let notebook = notebook_res.unwrap();
    assert_eq!(notebook.entries.len(), 6);

    // for ec in &notebook.entries {
    //     dbg!(&ec.pure_entry);
    // }

    let search_res = search_complex(&notebook, "{ is: [Event] }");
    let found_entries = search_res.unwrap();

    // dbg!(&found_entries);
    assert_eq!(found_entries.len(), 1);
}

