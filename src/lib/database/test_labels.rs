use crate::database::models::{filter_claims_by_property, Entry, EntryValue, Reference};
use crate::database::labels::LanguageLabels;
use crate::pdel_parser::{parse_wrapper, parse_entry, UnresolvedReference};

#[test]
fn test_transform_into_pure_entry_with_labels_single_language() {
    let subj = r#"
    @Entry {
        !labels(("Le titre français", "La description française", ("French alias 1", "French alias 2"))),
    }
    "#.to_string();
    let res = parse_wrapper(parse_entry, &subj);
    assert!(res.is_ok(), "{:?}", res);
    let pout = res.unwrap();
    let parsed_entry = &pout.p;

    let pure_entry_res = Entry::from_parsed_entry_without_id(&parsed_entry);
    assert!(&pure_entry_res.is_ok(), "{:?}", pure_entry_res);
    let pure_entry = pure_entry_res.unwrap();
    
    let name_claims = filter_claims_by_property(&pure_entry.claims, "name");
    assert_eq!(name_claims.len(), 1);
    assert_eq!(name_claims.get(0).unwrap().value, EntryValue::String("Le titre français".to_string()));
    let l_qualifiers = filter_claims_by_property(&name_claims.get(0).unwrap().qualifiers, "language");
    assert_eq!(l_qualifiers.len(), 1);
    assert_eq!(
        l_qualifiers.get(0).unwrap().value,
        EntryValue::Reference(Reference::Unresolved(UnresolvedReference::SoftLocation("en".to_string())))
    );

    // test language labels extractors
    let labels: LanguageLabels = LanguageLabels::from_entry_and_lang(&pure_entry, &"en");
    assert_eq!(labels.name, Option::Some("Le titre français".to_string()));
    assert_eq!(labels.description, Option::Some("La description française".to_string()));
    assert_eq!(labels.aliases, vec!["French alias 1".to_string(), "French alias 2".to_string()]);

    let description_claims = filter_claims_by_property(&pure_entry.claims, "description");
    assert_eq!(description_claims.len(), 1);
    assert_eq!(description_claims.get(0).unwrap().value, EntryValue::String("La description française".to_string()));

    let alias_claims = filter_claims_by_property(&pure_entry.claims, "alias");
    assert_eq!(alias_claims.len(), 2);
    assert_eq!(alias_claims.get(0).unwrap().value, EntryValue::String("French alias 1".to_string()));
    assert_eq!(alias_claims.get(1).unwrap().value, EntryValue::String("French alias 2".to_string()));
}

#[test]
fn test_transform_into_pure_entry_with_labels_multiple_languages() {
    let subj = r#"
    @Entry {
        !labels({
            fr: ("Le label français", "La description française", ("French alias 1", "French alias 2"))
            en: ("English label", "English description", ("English alias 1"))
        }),
    }
    "#.to_string();
    let res = parse_wrapper(parse_entry, &subj);
    assert!(res.is_ok(), "{:?}", res);
    let pout = res.unwrap();
    let parsed_entry = &pout.p;

    let pure_entry_res = Entry::from_parsed_entry_without_id(&parsed_entry);
    assert!(&pure_entry_res.is_ok(), "{:?}", pure_entry_res);
    let pure_entry = pure_entry_res.unwrap();

    assert!(filter_claims_by_property(&pure_entry.claims, "name").len() == 2);
    assert!(filter_claims_by_property(&pure_entry.claims, "description").len() == 2);
    assert!(filter_claims_by_property(&pure_entry.claims, "alias").len() == 3);
}
