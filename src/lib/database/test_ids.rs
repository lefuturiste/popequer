use crate::database::ids::Id;

#[test]
fn test_ids() {
    let repr = "ABCDEFGHIK";
    let res = Id::from_repr(&repr);
    assert!(res.is_ok());
    assert_eq!(res.as_ref().unwrap().0, 360886946596896 as u64);
    assert_eq!(res.unwrap().to_repr(), repr);
}

