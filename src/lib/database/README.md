# Popequer database

Implementation details and explanation.

## Id management

We could use an uuid v4

but it's very long for nothing

the ids need to be easily writable on an envelope or on a paper

We could have an index saved into the notebook database to store small ids

I propose a 64 bit ids that map on a custom 32 chars set (base32)

char set for Id : ABCDEFGHIJKLMNPQRSTUVXYZ12345670

I propose a 10 chars id. so that's 32**10 = 1.12E15 possibilities

ex: `4DSA54DS43`

## The query language

I want to design a more simple query language than SparQL 
Probably something more inspired from [Overpass-QL](https://wiki.openstreetmap.org/wiki/Overpass_API/Overpass_QL)
- When we need to query shit, use a made-up query language from the same claims syntax

ex: `is: [Human], age >= 10`

