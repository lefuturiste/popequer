use std::collections::HashMap;
use serde::{Serialize, Deserialize};

use super::{ids::Id, models::{Entry, EntryValue, Reference, ResolvedReference::InternalReference}};

/// allow to quickly find entries that link to an entry
#[derive(Debug, Serialize, Deserialize)]
pub struct BackLinksIndex(pub HashMap<Id, Vec<Id>>);

impl BackLinksIndex {
    /// for each entry, scan every value, if it's a reference, we index it as a backlink
    /// then, if we want to scan for is,
    /// It's not a big deal to mix up properties because
    /// we know that usually an item eg. Event is always
    /// referenced with the property is or sub in others items
    pub fn build_from_entries<'a>(entries: Vec<&'a Entry>) -> Self {
        let mut index = BackLinksIndex(HashMap::new());

        entries.iter().for_each(|entry| {
            entry.claims.iter().for_each(|claim| {
                if let EntryValue::Reference(Reference::Resolved(InternalReference(internal_ref))) =
                    &claim.value
                {
                    index
                        .0
                        .entry(internal_ref.clone())
                        .or_insert_with(Vec::new)
                        .push(entry.id.clone());
                }
            });
        });
        index
    }
}
