use std::io::BufReader;
use anyhow::Result;
use std::fs::File;
use crate::unwrap_or_return;

use super::models::Notebook;
use crate::fs_notebook::NotebookContext;

#[derive(Debug)]
pub enum ReadErr {
    CannotReadDB
}

pub fn read_db(context: &NotebookContext) -> Result<Notebook, ReadErr> {
    let db_file = unwrap_or_return!(
        File::open(&context.database_path()),
        |_err| ReadErr::CannotReadDB
    );
    let reader = BufReader::new(db_file);
    let notebook: Notebook = match bincode::deserialize_from(reader) {
        Ok(res) => res,
        Err(e) => {
            dbg!(e);
            return Err(ReadErr::CannotReadDB);
        }
    };
    Ok(notebook)
}

