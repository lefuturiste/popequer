use std::path::PathBuf;
use std::fmt;

use uuid::Uuid;
use serde::{Serialize, Deserialize};
use fully_pub::fully_pub;
use chrono::{DateTime, Utc};

use crate::{fs_notebook::NotebookContext, pdel_parser::{preprocess::Expansion, PEntry, ParseOutput, UnresolvedReference}};

use super::{graph::BackLinksIndex, ids::Id, labels::LabelEntryIndex};

/// A user-config for the Notebook
/// Result of the deserialization of a Notebook manifest.json file
#[derive(Serialize, Deserialize, Debug)]
#[fully_pub]
struct Manifest {
    name: String,
    description: String
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
#[fully_pub]
struct URI(String);

// TODO: unfinished ResolvedReference
#[fully_pub]
#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
enum ResolvedReference {
    ExternalReference(URI), // uri
    InternalReference(Id)
}

#[fully_pub]
#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
enum Reference {
    Unresolved(UnresolvedReference),
    Resolved(ResolvedReference)
}

#[fully_pub]
#[derive(Debug, Serialize, Deserialize)]
struct SourceFile {
    /// a unique id in the notebook that refer to a markdown file
    /// we could use a filesystem inode
    /// but this won't work if we move in a different filesystem
    /// also we don't really know the implementation behind it.
    /// also if we rename a note file but without using the move feature of the OS, the inode will
    /// change
    id: Uuid,

    /// the path of the file relative from the notebook base path
    path: PathBuf,

    /// a list of "mapping" expansion offsets on this file code
    /// it's the history of transformation by the pre-processor on thi file
    expansions: Vec<Expansion>
}

// HERE

#[fully_pub]
#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
enum EntryValue {
    // we can also have unresolved reference, because you know you may want to allow the creation
    // of the thing later
    // Reference of an EntryValue need to store: in which repository I need to get the data, and
    // where I need to look
    Reference(Reference),
    FQuantity(f64),
    IQuantity(i64),
    /// coordinate on a globe, by default the earth, but we can define it on other globe
    GlobeCoordinate { lat: f64, lng: f64 },
    Time(DateTime<Utc>),
    String(String)
    // Dict(Vec<EntryClaim>),
    // here, there is no functions
    // PointInTime
}

#[fully_pub]
#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
enum Property {
    Remote { remote_id: Uuid, remote_property_id: u32 }, // eg. wikidata property
    Internal { property_id: u32 },
    Custom(String)
}

#[fully_pub]
#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
struct EntryClaim {
    property: Property,
    value: EntryValue,
    qualifiers: Vec<EntryClaim>,
    // TODO: add importance: u32 (rang de la propriété)
}

#[fully_pub]
#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
struct Entry {
    id: Id,
    claims: Vec<EntryClaim>
}

#[fully_pub]
#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
enum FunctionArgument {
    Named { name: String, value: EntryValue },
    Positional(EntryValue)
}

#[fully_pub]
#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
struct Function {
    name: String,
    arguments: Vec<FunctionArgument>
}

/// Provide context to the entry data
#[fully_pub]
#[derive(Debug, Serialize, Deserialize, PartialEq)]
struct EntryContainer {
    /// an Abstract Syntax Tree, which was outputed by the parser,
    /// useful because you have access to context or where the data is in the source file
    parsed_entry: ParseOutput<PEntry>,

    /// the final interpreted Entry, more pratical to work with, if you have only data query
    pure_entry: Entry, 

    /// a reference to the file that originally contained this entry 
    source_file_id: Uuid
}


#[fully_pub]
#[derive(Debug, Serialize, Deserialize)]
struct Notebook {
    created_at: DateTime<Utc>,
    updated_at: DateTime<Utc>,
    files: Vec<SourceFile>,
    entries: Vec<EntryContainer>,
    // TODO: add a labels index
    manifest: Manifest,
    labels_index: LabelEntryIndex,
    backlinks: BackLinksIndex
}

/// functions to work with pure entry

/// get claims matching a property search string from list of claims
pub fn filter_claims_by_property<'a>(claims: &'a Vec<EntryClaim>, property_str: &str) -> Vec<&'a EntryClaim> {
    claims
        .iter()
        .filter(|c| c.property == Property::Custom(property_str.to_string()))
        .collect()
}

pub fn filter_claims_by_prop_and_val<'a>(claims: &'a Vec<EntryClaim>, property_str: &str, value: EntryValue) -> Vec<&'a EntryClaim> {
    claims
        .iter()
        .filter(|c|
            c.property == Property::Custom(property_str.to_string()) &&
            c.value == value
        )
        .collect()
}

// pub fn filter_claims_value<T>(claims: &'a Vec<EntryClaim>, property_str: &str, ) {}

impl fmt::Display for EntryValue {
    /// String display for EntryValue
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use EntryValue as EV;
        let res = match self {
            EV::String(str) => str.to_string(),
            EV::FQuantity(val)  => format!("{}", val),
            EV::IQuantity(val)  => format!("{}", val),
            EV::GlobeCoordinate { lat, lng } => format!("Geo({lat}, {lng})"),
            EV::Time(val) => format!("Date({})", val),
            val => format!("{:?}", val),
        };
        write!(f, "{}", res)
    }
}

impl EntryContainer {
    /// return the coresponding SourceFile where the Entry was indexed
    pub fn source_file<'a>(&self, notebook: &'a Notebook) -> &'a SourceFile {
        notebook.files.iter()
            .find(|x| x.id == self.source_file_id)
            .expect("Cannot find the source file struct.")
    }
}

impl SourceFile {
    /// get absolute path of the source file
    pub fn abs_path(&self, context: &NotebookContext) -> PathBuf {
        context.sources_path().join(self.path.clone())
    }
}
