use crate::{database::models::{Notebook, EntryContainer}, indexer::linker::link_claims};
use textdistance::str::damerau_levenshtein;
use anyhow::{anyhow, Context, Result};
use crate::database::parse_extractor::get_pure_claims;

use super::{filter::filter_instance_of, models::{EntryValue, Property, Reference, ResolvedReference}};

// the best score is zero
pub fn search_entry_by_label<'a>(notebook: &'a Notebook, label_query: &str) -> Vec<(usize, &'a EntryContainer)> {
    let mut results: Vec<(usize, &EntryContainer)> = vec![];
    let final_query: String = label_query.to_string().to_lowercase();
    for entry_container in &notebook.entries {
        results.push((
            damerau_levenshtein(&entry_container.pure_entry.name().to_lowercase(), &final_query),
            entry_container
        ))
    }
    results.sort_by(|a,b| a.0.cmp(&b.0));
    results
}

pub fn search_complex<'a>(notebook: &'a Notebook, complex_query: &str) -> Result<Vec<&'a EntryContainer>> {
    // we reparse complex query as entries claims, but we will need something to later associate
    // for now we just do a AND on all the claims
    // TODO: parse more complex query languages with closes and expressions
    // TODO: make use filter_instance_of when you have "is"

    let parsed_claims = crate::pdel_parser::parse_claims_query(&complex_query)
        .map_err(|_e| anyhow!("Could not parse claims query"))?;
        
    let mut query_claims = get_pure_claims(&parsed_claims.p)
        .map_err(|_e| anyhow!("Could not extract pure claims"))?;
    
    // link
    let _link_count = link_claims(&notebook.labels_index, &mut query_claims);

    let mut query_results: Vec<&EntryContainer> = vec![];
    
    // basic claims AND filter
    let base_results = notebook.entries.iter().filter(|entry_container| {
        query_claims.iter().all(|qc| {
           entry_container.pure_entry.claims
                .iter().any(|ec| {
                    qc.property == ec.property && qc.value == ec.value
                })
        })
    });
    for r in base_results {
        if query_results.iter().any(|qr| qr.pure_entry.id == r.pure_entry.id) {
            continue;
        }
        query_results.push(r);
    }
    dbg!(&query_results);
    
    for qc in &query_claims {
        // additional resolution to handle instance_of prop
        if qc.property == Property::Custom("is".into()) {
            let target_id = match &qc.value {
                EntryValue::Reference(r) => {
                    match r {
                        Reference::Unresolved(_u) => {
                            return Err(anyhow!("Found unresolved references in query claims"));
                        },
                        Reference::Resolved(resolved) => match resolved {
                            ResolvedReference::InternalReference(id) => id,
                            _ => { continue; }
                        }
                    }
                },
                _ => { continue; }
            };
            for r in filter_instance_of(notebook, &target_id) {
                if query_results.iter().any(|qr| qr.pure_entry.id == r.pure_entry.id) {
                    continue;
                }
                query_results.push(r);
            }
        }
    }
    Ok(query_results)
}
