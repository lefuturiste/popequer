use std::assert_matches::assert_matches;

use crate::database::parse_extractor::get_pure_value;
use crate::database::models::{filter_claims_by_property, Entry, EntryValue, Property, Reference};
use crate::pdel_parser::{parse_wrapper, parse_entry, UnresolvedReference};
use crate::pdel_parser::values::parse_entry_value;
use chrono::{DateTime, Utc};

#[test]
fn test_transform_into_pure_value_with_globe_coordinate() {
    let subj = r#"
    Geo(
        lat: 49.109,
        lng: 1.35093547
    )
    "#.to_string();
    let res = parse_entry_value(&subj, 0);
    assert!(res.is_ok(), "{:?}", res);
    let pout = res.unwrap();

    let res = get_pure_value(&pout.p);
    assert!(res.is_ok(), "{:?}", res);

    let pure_value = res.unwrap();
    let EntryValue::GlobeCoordinate {lat, lng} = pure_value else {
        unreachable!()
    };
    assert_eq!(lat, 49.109);
    assert_eq!(lng, 1.35093547);
}

/// util function
/// TODO: move it to appropriate place in the code tree
fn format_datum(time: DateTime<Utc>, format: &str) -> String {
    time.naive_local().format(format).to_string()
}

#[test]
fn test_transform_into_pure_value_with_datum_date() {
    let subj = r#"Datum("2023-11-20")"#.to_string();
    let res = parse_entry_value(&subj, 0);
    assert!(res.is_ok(), "{:?}", res);
    let pout = res.unwrap();

    let res = get_pure_value(&pout.p);
    assert!(res.is_ok(), "{:?}", res);

    let pure_value = res.unwrap();
    let EntryValue::Time(time) = pure_value else {
        unreachable!()
    };

    assert_eq!(format_datum(time, "%Y"), "2023");
    assert_eq!(format_datum(time, "%m"), "11");
    assert_eq!(format_datum(time, "%d"), "19"); // 19 instead of 20 because we are in UTC+1
}

#[test]
fn test_transform_into_pure_value_with_datum_datetime() {
    let subj = r#"Datum("2023-11-19 11:55:00")"#.to_string();
    let res = parse_entry_value(&subj, 0);
    assert!(res.is_ok(), "{:?}", res);
    let pout = res.unwrap();

    let res = get_pure_value(&pout.p);
    assert!(res.is_ok(), "{:?}", res);

    let pure_value = res.unwrap();
    let EntryValue::Time(time) = pure_value else {
        unreachable!()
    };
    dbg!("wow {:?}", pure_value);

    assert_eq!(format_datum(time, "%d"), "19");
    assert_eq!(format_datum(time, "%H"), "10"); // UTC+1
    assert_eq!(format_datum(time, "%M"), "55");
}

#[test]
fn test_transform_into_pure_entry() {
    let subj = r#"
    @Entry {
        is: [Human],
        birth_date: Datum("2001-01-01"),
        birth_place: [Vernon],
    }
    "#.to_string();
    let res = parse_wrapper(parse_entry, &subj);
    assert!(res.is_ok(), "{:?}", res);
    let pout = res.unwrap();
    let parsed_entry = &pout.p;

    let pure_entry_res = Entry::from_parsed_entry_without_id(&parsed_entry);
    assert!(&pure_entry_res.is_ok(), "{:?}", pure_entry_res);
    let pure_entry = pure_entry_res.unwrap();

    assert!(filter_claims_by_property(&pure_entry.claims, "is").len() == 1);
    assert!(filter_claims_by_property(&pure_entry.claims, "birth_date").len() == 1);
    assert!(filter_claims_by_property(&pure_entry.claims, "birth_place").len() == 1);

    assert_eq!(
        filter_claims_by_property(&pure_entry.claims, "is").pop().unwrap().value,
        EntryValue::Reference(
            Reference::Unresolved(
                UnresolvedReference::SoftLocation("Human".to_string())
            )
        )
    );
}

#[test]
fn test_transform_into_pure_entry_with_list_and_qualifiers() {
    let subj = r#"
    @Entry {
        is: [Human],
        birth_date: Datum("2001-01-10"),
        birth_place: [Vernon],
        in_circle: (
            [Club de machin-bidule] => {
                start_date: Datum("2008")
                end_date: Datum("2009")
            },
            [Club de robotique],
            [Bande de potes JPP] => {
                start_date: Datum("2010-11-20"),
                end_date: Datum("2023-11-20")
            }
        )
    }
    "#.to_string();
    let res = parse_wrapper(parse_entry, &subj);
    assert!(res.is_ok(), "{:?}", res);
    let pout = res.unwrap();
    let parsed_entry = &pout.p;

    let pure_entry_res = Entry::from_parsed_entry_without_id(&parsed_entry);
    assert!(&pure_entry_res.is_ok(), "{:?}", pure_entry_res);
    let pure_entry = pure_entry_res.unwrap();

    assert!(
        filter_claims_by_property(&pure_entry.claims, "in_circle").len() == 3
    );

    assert!(
        filter_claims_by_property(&pure_entry.claims, "in_circle")
            .iter()
            .find(|c|
                c.value == EntryValue::Reference(
                    Reference::Unresolved(
                        UnresolvedReference::SoftLocation("Club de machin-bidule".to_string())
                    )
                )
            )
            .unwrap()
            .qualifiers
            .iter()
            .find(|q| q.property == Property::Custom("start_date".to_string()))
            .is_some()
    );
}

#[test]
fn test_transform_into_pure_entry_with_custom_entry_intro() {
    let subj = r#"
        @Human {
            id: "AAAAAAAAAA",
            first_name: "John",
            last_name: "Doe"
        }
    "#.to_string();
    let res = parse_wrapper(parse_entry, &subj);
    assert_matches!(res, Ok(_));
    let pout = res.unwrap();

    let pure_entry_res = Entry::from_parsed_entry_with_id(&pout);
    assert_matches!(pure_entry_res, Ok(_));

    let pure_entry = pure_entry_res.unwrap();
    assert_eq!(filter_claims_by_property(&pure_entry.claims, "is").len(), 1);
    assert_eq!(filter_claims_by_property(&pure_entry.claims, "first_name").len(), 1);
    assert_eq!(
        filter_claims_by_property(&pure_entry.claims, "is").pop().unwrap().value,
        EntryValue::Reference(
            Reference::Unresolved(
                UnresolvedReference::SoftLocation("Human".to_string())
            )
        )
    );
}
