use chrono::{NaiveDate, NaiveDateTime, NaiveTime, Utc};
use chrono_tz::Europe::Paris;
use anyhow::{Result, anyhow};
use fully_pub::fully_pub;

use crate::pdel_parser::{PEntry, PEntryClaim, PEntryValue, PFunction, PFunctionArgument, ParseOutput, UnresolvedReference};
use crate::database::models::{EntryValue, EntryClaim, Property};

use super::ids::Id;
use super::models::{filter_claims_by_property, Entry, Reference};

#[derive(Debug)]
#[fully_pub]
enum ExtractErr {
    ExpectedId,
    CannotParseId,
    CannotExtractClaims
}

/// to work with parsed entry
pub fn get_claim_values<'a>(entry: &'a PEntry, target_property: &str) -> Vec<&'a PEntryValue> {
    entry.claims.p.iter()
        .filter(|claim| claim.p.property.p == target_property)
        .map(|claim| &claim.p.value_container.p.value.p)
        .collect()
}

/// to work with parsed entry
pub fn get_claim_value<'a>(entry: &'a PEntry, target_property: &str) -> Option<&'a PEntryValue> {
    entry.claims.p.iter()
        .find(|claim| claim.p.property.p == target_property)
        .map(|claim| &claim.p.value_container.p.value.p)
}

// TODO: instead of using String as properties identifiers, we will use IDs
pub fn get_pure_property(property_locator: &String) -> Property {
    Property::Custom(property_locator.to_string())
}

/// handle the complicated and special logic to expand PEntryValue::List into to many claims with
/// common property
pub fn get_pure_claims(parsed_claims: &Vec<ParseOutput<PEntryClaim>>) -> Result<Vec<EntryClaim>> {
    let mut out: Vec<EntryClaim> = vec![];
    for pclaim in parsed_claims {
        let parsed_value = &pclaim.p.value_container.p.value.p;
        let pure_property = get_pure_property(&pclaim.p.property.p);
        let pure_qualifiers = get_pure_claims(&pclaim.p.value_container.p.qualifiers)?;
        match parsed_value {
            PEntryValue::List(value_container_list) => {
                for vc in value_container_list {
                    out.push(EntryClaim {
                        property: pure_property.clone(),
                        qualifiers: [pure_qualifiers.clone(), get_pure_claims(&vc.p.qualifiers)?].concat(),
                        value: get_pure_value(&vc.p.value.p)?
                    })
                }
            },
            _ => {
                out.push(EntryClaim {
                    property: get_pure_property(&pclaim.p.property.p),
                    qualifiers: pure_qualifiers,
                    value:  get_pure_value(parsed_value)?
                })
            }
        }
    }
    Ok(out)
}


fn get_argument_value(arg: &PFunctionArgument) -> &PEntryValue {
    match arg {
        PFunctionArgument::Named { value, .. } => value,
        PFunctionArgument::Positional(value) => value
    }
}

/// Return `Some(x)` if it find x the argument matching `target_arg_name` inside `parsed_func` call
/// arguments
/// otherwise return `None`
fn get_argument_by_name<'a>(parsed_func: &'a PFunction, target_arg_name: &str) -> Option<&'a PFunctionArgument> {
    parsed_func.arguments.iter()
        .find(|a| match &a.p {
            PFunctionArgument::Named { name, .. } => name == target_arg_name,
            _ => false
        })
        .map(|x| &x.p)
}

pub fn get_pure_value(parsed_value: &PEntryValue) -> Result<EntryValue> {
    Ok(match parsed_value {
        PEntryValue::Float(inner) => EntryValue::FQuantity(inner.clone()),
        PEntryValue::Integer(inner) => EntryValue::IQuantity(inner.clone()),
        PEntryValue::String(inner) => EntryValue::String(inner.clone()),
        PEntryValue::Reference(parsed_ref) => EntryValue::Reference(Reference::Unresolved(parsed_ref.clone())),
        PEntryValue::Function(func) => {
            match func.name.p.as_str() {
                "Geo" => EntryValue::GlobeCoordinate {
                    // https://stackoverflow.com/questions/18636564/lat-long-or-long-lat
                    // lat, then lon
                    lat: get_argument_by_name(&func, &"lat")
                        .or(func.arguments.iter().nth(0).map(|x| &x.p))
                        .map(get_argument_value)
                        .ok_or(anyhow!("Cannot get lat argument"))?
                        .try_into().map_err(|_| anyhow!("Lat argument is not float"))?,

                    lng: get_argument_by_name(&func, &"lng")
                        .or(func.arguments.iter().nth(1).map(|x| &x.p))
                        .map(get_argument_value)
                        .ok_or(anyhow!("Cannot get lng argument"))?
                        .try_into().map_err(|_| anyhow!("Lng argument is not float"))?
                },
                "Datum" => {
                    // for now we don't associate timezone, we will have to add a default timezone
                    // to the notebook setting
                    let raw_date: String = func.arguments.iter().nth(0).map(|x| &x.p)
                        .map(get_argument_value)
                        .ok_or(anyhow!("Cannot get date input argument"))?
                        .try_into().map_err(|_| anyhow!("Date argument is not String"))?;
                    
                    let mut out = NaiveDate::parse_from_str(&raw_date, "%Y-%m-%d")
                        .map(|x| x.and_time(NaiveTime::from_hms_opt(0, 0, 0).unwrap()))
                        .map_err(|_| anyhow!("Could not parse date as %Y-%m-%d"));
                    if raw_date.len() == 4 {
                        // parse only year
                        // FIXME: have the first error show up when all the options are exausted
                        // FIXME: we will need to have a more advanced parser to carry the
                        // precision along with the date, we cannot just output a date like that
                        let nd = NaiveDate::from_yo_opt(
                                raw_date.parse::<i32>().map_err(|_e| anyhow!("Failed to parse date year as an integer"))?,
                                1
                            );
                        out = nd
                            .map(|x| x.and_time(NaiveTime::from_hms_opt(0, 0, 0).unwrap()))
                            .ok_or(anyhow!("Could not parse date as %Y-%m-%d"));
                    }
                    if out.is_err() {
                        out = NaiveDateTime::parse_from_str(&raw_date, "%Y-%m-%d %H:%M")
                            .map_err(|_| anyhow!("Could not parse date as %Y-%m-%d %H:%M"));
                    }
                    if out.is_err() {
                        out = NaiveDateTime::parse_from_str(&raw_date, "%Y-%m-%d %H:%M:%S")
                            .map_err(|_| anyhow!("Could not parse date {:?} as %Y-%m-%d %H:%M:%S", raw_date));
                    }
                    let naive_dt = out.map_err(|e| anyhow!("Failed to parse date argument {:?}", e))?;
                    let l = naive_dt.and_local_timezone(Paris).unwrap().with_timezone(&Utc);
                    EntryValue::Time(l)
                },
                "VaultEntry" => {
                    let entry_path: String = func.arguments.iter().nth(0).map(|x| &x.p)
                        .map(get_argument_value)
                        .ok_or(anyhow!("Cannot get argument"))?
                        .try_into().map_err(|_| anyhow!("Entry path argument is not String"))?;
                    EntryValue::String(format!("vault_entry://{}", entry_path))
                }
                _ => return Err(anyhow!("Extractor encountered unsupported function {:?}", func.name.p))
            }
        },
        _ => return Err(anyhow!("Entry value type not supported by extractor {:?}", parsed_value))
    })
}


impl Entry {
    /// Special entry builder to use with caution
    pub fn from_parsed_entry_without_id(parsed_entry: &PEntry) -> Result<Entry> {
        Ok(Entry {
            id: Id::generate(),
            claims: get_pure_claims(&parsed_entry.claims.p)?
        })
    }

    /// Build the pure entry from the parsed entry, apply claims transformation
    pub fn from_parsed_entry_with_id(parsed_entry: &ParseOutput<PEntry>) -> Result<Entry, ExtractErr> {
        let original_pure_claims = get_pure_claims(&parsed_entry.p.claims.p)
            .map_err(|_err| ExtractErr::CannotExtractClaims)?;

        // get the id from claims
        let id_claim = filter_claims_by_property(&original_pure_claims, "id")
            .get(0)
            .map(|x| *x)
            .ok_or(ExtractErr::ExpectedId)?;
        let id_repr = if let EntryValue::String(id_repr) = id_claim.value.clone() {
            id_repr.to_string()
        } else {
            return Err(ExtractErr::CannotParseId);
        };
        let id = Id::from_repr(&id_repr)
                    .map_err(|_err| ExtractErr::CannotParseId)?;

        let mut claims: Vec<EntryClaim> = original_pure_claims.clone();

        // apply the parsed entry custom intro to the claims
        let entry_intro = &parsed_entry.p.intro.p;
        if entry_intro != "Entry" {
            claims = claims.into_iter()
                .filter(|x| x.property != Property::Custom("is".into()))
                .collect();
            if claims.len() != original_pure_claims.len() {
                // this means that a claim with property 'is' was removed and will be overwritten
                eprintln!("Warning: `is` property was overwritten by custom Entry intro");
            }
            // the entry intro string is the reference
            let instance_claim = EntryClaim {
                property: Property::Custom("is".into()),
                value: EntryValue::Reference(
                    Reference::Unresolved(
                        UnresolvedReference::SoftLocation(
                            entry_intro.clone()
                        )
                    )
                ),
                qualifiers: vec![]
            };
            claims.insert(0, instance_claim)
        }
        
        // remove the "id: " claim from the pure entry and clone claims
        let final_claims = claims
                .into_iter()
                .filter_map(|x| {
                    if x.property != Property::Custom("id".to_string()) {
                        Some(x)
                    } else {
                        None
                    }
                })
                .collect();
        Ok(Entry {
            id,
            claims: final_claims
        })
    }

}

/// Return the list of parsed entries contained in the parent entry followed at last by the parent
/// entry.
/// Can potentially mess up the parsed tree from the reality in the file.
/// it generate an ad-hoc id, that will need to be managed later
pub fn flatten_embedded_entries(pentry: &ParseOutput<PEntry>) -> Vec<(usize, ParseOutput<PEntry>)> {
    let mut parent_entry = pentry.clone();
    let mut entries: Vec<(usize, ParseOutput<PEntry>)> = vec![];

    for (i, claim) in parent_entry.p.claims.p.iter_mut().enumerate() {
        let embedded_entry = match &claim.p.value_container.p.value.p {
            PEntryValue::Entry(inner) => inner,
            _ => { continue; }
        };

        entries.push((i, ParseOutput {
            start_loc: claim.p.value_container.start_loc,
            end_loc: claim.p.value_container.end_loc,
            p: embedded_entry.clone()
        }));
    }
    entries
}

