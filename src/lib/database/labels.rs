use super::{ids::Id, models::{filter_claims_by_property, Entry, EntryClaim, EntryValue, Property, Reference}, parse_extractor::get_pure_claims};
use crate::pdel_parser::UnresolvedReference;
use std::collections::HashMap;
use serde::{Serialize, Deserialize};
use fully_pub::fully_pub;
use textdistance::str::damerau_levenshtein;
use anyhow::Result;

/// Get a component (eg. a description or a name) of a specific language from a Pure Entry
/// Return a list of the label values of the kind specified by component_property_name sorted by
/// preference, the labels that match the target_lang_code will be first
fn get_language_label_components<'a>(component_property_name: &str, entry: &'a Entry, target_lang_code: &str) -> Vec<String> {
    let mut claims_with_score: Vec<(usize, &EntryClaim)> = filter_claims_by_property(&entry.claims, component_property_name)
        .iter()
        .map(|claim| {
            let score = claim.qualifiers.iter().any(|qualifing_claim|
                qualifing_claim.property == Property::Custom("language".to_string()) &&
                if let EntryValue::Reference(component_value) = &qualifing_claim.value {
                    // FIXME: compare against a Resolved location actual item, not a SoftLocation
                    *component_value == Reference::Unresolved(UnresolvedReference::SoftLocation(target_lang_code.to_string()))
                } else { false }) as usize;
             (score, *claim)
        })
        .collect();
    claims_with_score.sort_by_key(|x| x.0);
    claims_with_score.iter()
        .map(|(_p, claim)|
            if let EntryValue::String(val) = claim.value.clone() {
                val
            } else {
                panic!("get_language_label_components: Unexpected EntryValue that is not EntryValue::String when collecting labels")
            }
        ).collect()
}

impl LanguageLabels {
    /// extract language labels components in a single language
    pub fn from_entry_and_lang(entry: &Entry, prefered_lang_code: &str) -> Self {
        LanguageLabels {
            name: get_language_label_components("name", entry, prefered_lang_code).get(0).cloned(),
            description: get_language_label_components("description", entry, prefered_lang_code).get(0).cloned(),
            aliases: get_language_label_components("alias", entry, prefered_lang_code)
        }
    }
}


impl Entry {
    /// Auto description to build a name from claims of entities
    /// eg. if entry is human, use first and last name
    /// eg. if entry is an event with a circle and a date, use "envent of {circle} at {date}"
    fn auto_label(&self, target_lang_code: &str) -> Option<String> {
        // TODO: implements get_auto_label
        Some("<auto label>".to_string())
    }

    /// from an Entry output a String in English to represent it
    ///
    /// Strategy to get name from entries :
    /// - list all the languages labels, try to get in english, if not found, try to get it in the
    /// next language
    /// - get the explicit name with language
    /// - get the name from the name field
    /// - specific behaviour by items "instance of" P31
    ///     - example for a human take the first and last name
    ///     - for a building take the "name after" prop
    ///
    /// example: a building in a location : (building in XX)
    pub fn name(&self) -> String {
        if let Some(name) = LanguageLabels::from_entry_and_lang(self, "en").name {
            return name;
        }

        if let Some(auto_name) = self.auto_label("en") {
            return auto_name;
        }
        
        return "<Unknown?>".to_string();
    }

    pub fn aliases(&self) -> Vec<String> {
        let labels = LanguageLabels::from_entry_and_lang(self, "en");
        let mut aliases: Vec<String> = labels.aliases;
        if let Some(name) = labels.name {
            aliases.insert(0, name);
        }
        if let Some(auto_name) = self.auto_label("en") {
            aliases.push(auto_name);
        }
        aliases
    }

}

/// used to specify labels associated to an entry
pub type EntryLabels = HashMap<String, LanguageLabels>;

/// describe a name, description, aliases
#[fully_pub]
#[derive(Debug, Serialize, Deserialize)]
struct LanguageLabels {
    name: Option<String>,
    description: Option<String>,
    aliases: Vec<String>
}

#[fully_pub]
#[derive(Debug, Serialize, Deserialize)]
struct LabelEntryIndex(HashMap<String, Id>);

impl LabelEntryIndex {

    pub fn empty() -> Self {
        LabelEntryIndex {
            0: HashMap::new()
        }
    }

    pub fn from_pure_entries<'a>(entries: Vec<&'a Entry>) -> Self {
        let mut b: Self = LabelEntryIndex(HashMap::new());
        for entry in entries {
            for alias in entry.aliases() {
                b.0.insert(alias.trim().to_lowercase(), entry.id.clone());
            }
        }
        b
    }

    pub fn raw_search(&self, label_query: String) -> Vec<(usize, Id)> {
        let mut results: Vec<(usize, Id)> = vec![];
        let final_query: String = label_query.to_string().to_lowercase();
        for (key, entry) in self.0.iter() {
            results.push((
                damerau_levenshtein(key.to_lowercase().as_ref(), &final_query),
                entry.clone()
            ))
        }
        results.sort_by(|a,b| a.0.cmp(&b.0));
        results
    }

    /// most efficient search for only one item
    pub fn singular_search(&self, label_query: String) -> Option<Id> {
        self.0.get(&label_query.trim().to_lowercase())
            .map(|x| x.clone())
            .or_else(|| 
                self.raw_search(label_query)
                    .get(0)
                    .filter(|x| x.0 < 2)
                    .map(|x| x.1.clone())
            )
    }
}
