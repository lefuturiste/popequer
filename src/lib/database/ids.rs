use std::fmt;
use core::fmt::Debug;
use anyhow::{anyhow, Result};
use serde::{Serialize, Deserialize};

#[derive(Clone, Hash, PartialEq, Eq, Deserialize, Serialize)]
pub struct Id(pub u64);

// we strip the 0 and 1
// O=0, 1=I
const CHARMAP: &str = "ABCDEFGHIJKLMNOPQRSTUVXYZ2345678";

// 2**5 = 32
// so we can represent any Id as concatenation of 5 bits position
// if we have 10 chars, we need 5 bits times 10 so 50 bits, it fit on 64

impl Id {
    pub fn generate() -> Self {
        Id(rand::random())
    }

    pub fn from_repr(inp: &str) -> Result<Self> {
        // Ok(Id(inp.chars().enumerate().map(
        //     |(i, c)| {
        //         (2 as u64).pow(i as u32) * (CHARMAP.chars()
        //             .position(|x| x == c)
        //             .expect("Invalid Id representation") as u64)
        //     }
        // ).collect::<Vec<u64>>().iter().sum::<u64>()))
        // Ok(Id(
        //     u64::from_str_radix(inp, 32)
        //         .expect("Cannot convert str to Id: Invalid id representation")
        // ))
        if inp.len() != 10 {
            // FIXME: may be we can pad?
            return Err(anyhow!("The string repr of Id must have 10 chars"));
        }
        
        let mut repr: u64 = 0;

        for (i, c) in inp.chars().enumerate() {
            let position = CHARMAP.chars().position(|x| x == c)
                .ok_or(anyhow!("The string repr of Id contains char not accepted in the Id CharMap"))? as u64;
        
            let offset: usize = 5*i;
            repr |= position << offset;
            // eprintln!("offset={} position={} {}", offset, position, format!("{:0>5}", format!("{:b}", position)));
            // eprintln!("{}", format!("{:0>64}", format!("{:b}", repr)));
        }

        Ok(Id(repr))
    }

    /// get a 10-char long owned String from u64
    pub fn to_repr(&self) -> String {
        let mut result = String::new();

        // max 64 bits is 0xffffffffffffffff
        for i in 0..10 {
            let offset = 5*i;
            let mask = 0b11111 << offset;
            let position = (self.0 & mask) >> offset;
            result.push(
                CHARMAP.chars().nth(position as usize)
                    .expect("Invalid internal Id repr")
            )
        }
        result.chars().collect()
    }
}

impl fmt::Display for Id {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
      write!(f, "{}", self.to_repr())
    }
}

impl Debug for Id {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Id({:?})", self.to_repr())
    }
}
