#![feature(assert_matches)]


pub mod utils;
pub mod database;
pub mod cli;
pub mod pdel_parser;
pub mod indexer;
pub mod fs_notebook;
// pub mod wikidata_editor;
// pub mod wikidata_converter;
// pub mod pdel_serializer;
pub mod exporters;
pub mod views;
