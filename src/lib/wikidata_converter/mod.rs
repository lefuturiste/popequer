// use serde_json::{Result, Value};

#[derive(Debug)]
pub enum WikidataJsonLdDeserializerError {
    JsonError(serde_json::Error),
    InvalidLayout,
    InvalidGraph
}


pub type Qid = u64;

pub fn load_json_ld_entry(json_ld_subject: String, subject_item: Qid) -> Result<(), WikidataJsonLdDeserializerError> {
    // Parse the string of data into serde_json::Value.
    let value: serde_json::Value =
        serde_json::from_str(&json_ld_subject).map_err(WikidataJsonLdDeserializerError::JsonError)?;
    
    let graph_pool = value.get("@graph")
        .and_then(|v| v.as_array())
        .ok_or(WikidataJsonLdDeserializerError::InvalidLayout)?;
    
    // get the json object (edge) that has an @id key that refer
    // to the wikidata item we're working on
    // and has no @type key
    // that will give us the main object (root) where we can start exploring the graph
    let res = graph_pool.iter()
        .find(|edge| {
            if !edge.is_object() { return false };
            if let Some(edge_str) = edge.get("@id") {
                return
                    edge_str.as_str() == Some(&format!("wd:Q{}", subject_item)) &&
                    edge.as_object().unwrap().keys().all(|e| e != "@type")
            }
            false
        })
        .ok_or(WikidataJsonLdDeserializerError::InvalidGraph)?;
    
    // for each claim primitive we could search for the specific statement
    
    // for each primitive, we could search for the statement detail
    // then search for the value detail

    // where you have @id: s:Qid-UUID

    dbg!(res);
    
    Ok(())
}

