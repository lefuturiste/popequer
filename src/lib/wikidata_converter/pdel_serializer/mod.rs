#![feature(slice_group_by)]

use std::collections::HashSet;
use wikidata::Qid;
use crate::pdel_serializer::overloading::{OverloadingDefinition, OverloadingItem};

pub mod overloading;

pub struct SerializerSettings {
    pub languages: Vec<wikidata::Lang>
}

fn serialize_external_ref(eid: &Qid, items_overloading: &OverloadingDefinition) -> String {
    // try to find it in the overloading
        
    match items_overloading.items.get(&format!("{}", eid)) {
        Some(def) => {
            return format!("[{}](wdt:{})", def.aliases[0], eid)
        },
        None => {
            return format!("[](wdt:{})", eid);
        }
    }
}

pub fn serialize(
    settings: SerializerSettings,
    props_overloading: OverloadingDefinition,
    items_overloading: OverloadingDefinition,
    entry: &wikidata::Entity
) -> String
{
    // format!("{:?}", entry);
    let mut output_txt = String::new();

    output_txt.push_str("@Entry {\n");

    let mut available_locales: HashSet<String> = HashSet::new();

    // create a macro to execute the for loop for each locale_item
    macro_rules! index_locales_on_localizables {
        ( $localizables:expr ) => {
            for (k, _) in $localizables.iter() {
                if settings.languages.iter().any(|e| e == k) {
                    available_locales.insert(k.0.clone());
                }
            }
        }
    }
    index_locales_on_localizables!(entry.labels);

    // dbg!(&available_locales);
    for locale_str in &available_locales {
        let locale = wikidata::Lang(locale_str.to_string());
        output_txt.push_str(&format!("    {}: (", &locale.0));

        let mut components: [Option<String>; 3] = [None, None, None];
        if let Some(label) = entry.labels.get(&locale) {
            components[0] = Some(format!("\"{label}\""));
        }
        if let Some(description) = entry.descriptions.get(&locale) {
            components[1] = Some(format!("\"{description}\""));
        }
        if let Some(aliases) = entry.aliases.get(&locale) {
            let mut tmp_str = String::from("(");
            for (i, a) in aliases.iter().enumerate() {
                tmp_str.push_str(&format!("\"{a}\" "));
                if i > 0 && i+1 != aliases.len() {
                    tmp_str.push_str(",");
                }
            }
            tmp_str.push_str(")");
            components[2] = Some(tmp_str);
        }
        
        let mut some_to_add = components.iter().filter(|e| e.is_some()).count();
        for i in 0..components.len() {
            let mut to_add: &str = "";
            if some_to_add > 0 && components[i].is_none() {
                to_add = "null";
            }
            if let Some(compo) = &components[i] {
                to_add = &compo;
                some_to_add -= 1;
            }
            if i != 0 && !to_add.is_empty() {
                output_txt.push_str(", ");
            }
            output_txt.push_str(&to_add);
        }
        output_txt.push_str(")\n");
        
        // match entry.labels.get(&Lang(locale)) {
        //     Some(label) => 
        //     _ => ()
        // }
    }

    output_txt.push_str("}{\n");

    // it would be nice if we could sort claims and for example show first the claims that have a
    // priority_value
    
    // we need to separate list of claims : the ones that have an identifier and the ones that does
    // not
    
    // let mut ordered_claims: Vec<(u32, wikidata::Pid, &wikidata::ClaimValue, Option<&PropertySchema>)> = vec![];
    struct ClaimGroup<'a> {
        priority: u64,
        pid: wikidata::Pid,
        schema_binding: Option<&'a OverloadingItem>,
        claims_values: Vec<&'a wikidata::ClaimValue>
    }

    let mut claims_groups: Vec<ClaimGroup> = vec![];
    for (pid, claim) in &entry.claims {
        // find the binding the the schema corresponding to the property of claim
        let prop_schema_opt: Option<&OverloadingItem> = 
            props_overloading.items.get(&format!("P{}", pid.0));
        let priority: u64 = match prop_schema_opt {
            Some(prop_schema) => prop_schema.priority,
            None => u64::MAX
        };
        // use some kind of bucketing strategy
        // if a bucket with that props already exists then append the claim here, else create the
        // bucket
        // ordered_claims.push((prop_priority, *pid, claim, prop_schema_opt))
        if let Some(pos) = claims_groups.iter().position(|g| g.pid == *pid) {
            claims_groups[pos].claims_values.push(claim)
        } else {
            claims_groups.push(ClaimGroup {
                priority,
                pid: *pid,
                schema_binding: prop_schema_opt,
                claims_values: vec![claim]
            })
        }
    }
    claims_groups.sort_by_key(|x| x.priority);

    // group ordered_claims by property
    // let claims_by_property = ordered_claims.group_by(|a, b| a.1 == b.1);

    for claim_group in claims_groups {
        // println!("{:?} {:?}", claim_group.pid, claim_group.claims_values);

        let prop_identifier: String = match claim_group.schema_binding {
            Some(prop_schema) => prop_schema.aliases.get(0).unwrap().to_string(),
            None => format!("P{}", claim_group.pid)
        };
         
        if claim_group.claims_values.len() > 1 {
            output_txt.push_str(&format!("    {prop_identifier}: (\n"));
        }
        for claim_value in &claim_group.claims_values {
            // map claim_value to string
            let claim_value = (match &claim_value.data {
                // format!("[]({})", eid)
                wikidata::ClaimValueData::Item(eid) =>
                    serialize_external_ref(eid, &items_overloading),
                wikidata::ClaimValueData::GlobeCoordinate { lat, lon, .. } =>
                    format!("Coord({}, {})", lat, lon),
                wikidata::ClaimValueData::String(str) => str.to_string(),
                wikidata::ClaimValueData::Quantity { amount, lower_bound: _, upper_bound: _, unit: unit_opt } => {
                    if let Some(unit) = unit_opt {
                        format!("Quantity({}, {})", amount, unit)
                    } else {
                        format!("Quantity({})", amount)
                    }
                }
                wikidata::ClaimValueData::DateTime { date_time, .. } => 
                    date_time.format("Date(%Y, %m, %d)").to_string(),
                wikidata::ClaimValueData::Url(url) =>
                    format!("{:?}", url),
                wikidata::ClaimValueData::CommonsMedia(commons_id) =>
                    format!("Commons({:?})", commons_id),
                wikidata::ClaimValueData::ExternalID(ext_ref) =>
                    format!("Ref(\"{}\")", ext_ref),
                wikidata::ClaimValueData::MonolingualText(text) => {
                    if !available_locales.contains(&text.lang.0) { continue; }
                    format!("Text({:?}, {:?})", text.lang.0, text.text)
                }
                _ => "UnsupportedValue()".to_string()
            }).to_string();

            if claim_group.claims_values.len() == 1 {
                output_txt.push_str(&format!("    {prop_identifier}: {claim_value}"));
            } else {
                output_txt.push_str(&format!("        {claim_value}"));
            }
            output_txt.push_str("\n");
        }
        if claim_group.claims_values.len() > 1 {
            output_txt.push_str(&format!("    )\n"));
        }
    }
    output_txt.push_str("}\n");

    return output_txt;
}


