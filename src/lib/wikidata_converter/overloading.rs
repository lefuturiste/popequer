/// Overloading
/// module to load overloading definitions from files
/// overloading definitions are supplemental data created by the popequer project to more easily
/// integrate wikidata data into popequer.
/// They include data that you will not find in wikidata
/// This is :
/// - for wikidata properties (PXXX): a alphanumerical camelCase alias for each wikidata property
/// that allow to refer to that property with a name
/// - the same for commons wikidata items that we need to refer to often (eg. refer Q5 as "human")
/// 
/// Dont: ~~idealy, we would like to bake the definitions are compile time~~
/// We don't bake the defs at compile time because we would like to change them dynamically

use std::collections::HashMap;
use serde::Deserialize;
use std::fs;
use std::path::Path;
use fully_pub::fully_pub;

#[derive(Debug, PartialEq, Deserialize)]
pub enum LoadOverloadingErr {
    Generic,
    ParseErr
}

fn a_default() -> Vec<String> {
    vec![]
}

#[fully_pub]
#[derive(Debug, PartialEq, Deserialize)]
struct OverloadingItem {
    #[serde(default)]
    priority: u64,

    #[serde(alias = "a", default = "a_default")]
    aliases: Vec<String>,

    #[serde(alias = "type")]
    expected_type: Option<String>,

    notes: Option<Vec<String>>,

    examples: Option<Vec<String>>,
}

#[fully_pub]
#[derive(Debug, PartialEq, Deserialize)]
struct OverloadingDefinition {
    items: HashMap<String, OverloadingItem>
}


pub enum SpecifiedOverloadingDefinition {
    Props,
    Items
}

pub type OverloadingResult = Result<OverloadingDefinition, LoadOverloadingErr>;

pub fn load_overloading(def_file_path: &Path) -> OverloadingResult {
    let inp_def_yaml = fs::read_to_string(def_file_path)
        .expect("Should have been able to read the wikidata properties overloading file");

    let mut overloading_def: OverloadingDefinition = match serde_yaml::from_str(&inp_def_yaml) {
        Ok(res) => res,
        Err(err) => {
            dbg!(err);
            dbg!(inp_def_yaml);
            return Err(LoadOverloadingErr::ParseErr);
        }
    };
    // set priority from position
    let item_keys: Vec<String> = overloading_def.items.keys().cloned().collect();
    for (i, item_key) in item_keys.iter().enumerate() {
        overloading_def.items.get_mut(item_key).unwrap().priority = i as u64;
    }
    
    return Ok(overloading_def);
}

