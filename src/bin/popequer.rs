#![allow(dead_code)]
#![allow(unused_import_braces)]
// #![allow(unused)]

use std::process;
use clap::{arg, Parser, Subcommand};
use popequer::cli::{self, CliErr, SystemFeedback};
use popequer::fs_notebook::{NotebookContext, check_notebook_directory, get_notebook_path};


#[derive(Parser)]
#[command(author, version, about, long_about = None)]
struct PopequerCli {
    /// Turn debugging information on
    #[arg(short = 'v', long = "verbose", action = clap::ArgAction::Count)]
    verbose: u8,
    
    #[command(subcommand)]
    command: Option<Commands>,
}


#[derive(Subcommand)]
enum Commands {
    /// Get current status of notebook (indexed? updated?)
    Status {},
    /// Run the index routine on the notebook
    Index  {
        #[arg(short = 'w', long = "watch", action = clap::ArgAction::SetTrue)]
        watch: bool
    },
    /// List summaries of entries in the notebook
    ListEntries {},
    /// Get a specific Entry
    GetEntry {
        /// the ID repr of the ID to get
        entry_id: String
    },
    /// Query entries by label
    #[command(arg_required_else_help = true)]
    QueryByLabel {
        label_query: String
    },
    /// Filter entries by claims value
    FilterByClaims {
        /// Claims to matches for in PDEL syntax eg. `{ is: [Tower] }`
        claims_query: String
    },
    /// List upcoming events
    CalendarView {}
}

fn main() {
    let cli = PopequerCli::parse();
    // have a POPEQUER_DIR env var that refer to the path of the notebook directory that contain a
    // notebook.json file
    // look for notebook.json file
    
    let notebook_path = match get_notebook_path() {
        Ok(v) => v,
        Err(err) => {
            eprintln!("Could not get the notebook path: {:?}", err);
            process::exit(2);
        }
    };
    eprintln!("{:?}", notebook_path);

    if let Err(err) = check_notebook_directory(&notebook_path) {
        eprintln!("The directory {:?} is not a notebook: {:?}", notebook_path, err);
        process::exit(2);
    }
    
    // internal path is derived from the notebook dir
    let notebook_context = NotebookContext {
        base_path: notebook_path
    };

    let output_channel = SystemFeedback {};
    let res: Result<(), CliErr> = match &cli.command {
        Some(Commands::Status {}) => {
            cli::status::get_notebook_status(output_channel, &notebook_context)
        },
        Some(Commands::Index { watch }) => {
            if *watch {
                todo!();
            }
            cli::index::index_notebook_once(output_channel, &notebook_context)
        },
        Some(Commands::ListEntries {}) => {
            if cli.verbose > 1 {
                cli::list::get_list_debug(output_channel, &notebook_context)
            } else {
                cli::list::get_list(output_channel, &notebook_context)
            }
        },
        Some(Commands::GetEntry { entry_id }) => {
            cli::list::get_one_debug(output_channel, &notebook_context, entry_id)
        },
        Some(Commands::QueryByLabel { label_query }) => {
             cli::query::query_by_label(output_channel, &notebook_context, label_query)
        },
        Some(Commands::FilterByClaims { claims_query }) => {
            cli::query::query_by_claims_filter(output_channel, &notebook_context, &claims_query)
        },
        Some(Commands::CalendarView {}) => {
             cli::views::calendar_view(output_channel, &notebook_context)
        },
        None => {
            Err(CliErr{
                message: "Unsupported sub command".to_string(),
                exit_code: 2
            })
        }
        // TODO: Export vcard sub cmd
        //     cli::export::export_vcard(&notebook_context, "".to_string())
    };

    if let Err(cli_err) = res {
        eprintln!("Failed to run ppq cmd");
        eprintln!("{}", cli_err.message);
        process::exit(cli_err.exit_code);
    }
}

