#![allow(dead_code)]
#![allow(unused_import_braces)]
#![allow(unused)]

use std::fs;
use std::path::Path;

use popequer::pdel_parser;


fn main()
{
    // dbg!(fetch_item(627220));
    convert_ids();

    // parse_wikidata_overloading();
    
    // parse_pdel_demo();

    // wikidata_editor_demo();
    
    // deserializer_wikidata_json_ld_demo();
}

fn convert_ids()
{
    let id = popequer::database::ids::Id::from_repr(&"ABCDEFGHIK").unwrap();
    dbg!(&id);
    dbg!(id.to_repr());
}

fn parse_pdel_demo()
{
    let inp_pdel = fs::read_to_string("./examples/pdel/full1.pdel").expect("should being able to read the example pdel file");
    let res = pdel_parser::parse_wrapper(crate::pdel_parser::parse_entry, &inp_pdel);
    dbg!(res);
}
